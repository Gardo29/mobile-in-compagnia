-- *********************************************
-- * SQL MySQL generation                      
-- *--------------------------------------------
-- * DB-MAIN version: 11.0.1              
-- * Generator date: Dec  4 2018              
-- * Generation date: Fri Apr 24 23:09:17 2020 
-- * LUN file: C:\Users\Lorenzo Gardini\Desktop\Local\Progetti\Progetto Mobile\Mobile.lun 
-- * Schema: Logic/1 
-- ********************************************* 


-- Database Section
-- ________________ 

-- create database Logic;
-- use Logic;


-- Tables Section
-- _____________ 

create table LISTS (
     list_id int not null AUTO_INCREMENT,
     display_home tinyint not null,
     ts int not null,
     deleted tinyint not null,
     enabled_notifications tinyint  not null,
     name varchar(40) not null,
     description varchar(400),
     creation_date DATETIME not null,
     user_id int not null,
     constraint IDLIST primary key (list_id));

create table NOTIFICATIONS (
     task_id int not null,
     time DATETIME not null,
     unit varchar(10),
     ts int not null,
     deleted tinyint not null,
     constraint IDNOTIFICATION primary key (task_id,time));

create table TASKS (
     task_id int not null AUTO_INCREMENT,
     ts int not null,
     creation_date DATETIME not null,
     title varchar(50) not null,
     color int not null,
     start_date DATETIME,
     end_date DATETIME,
     repeat_time int,
     repeat_unit varchar(10),
     deleted DATETIME,
     completed DATETIME,
     description varchar(400),
     list_id int not null,
     permanently_deleted tinyint  not null, 
     constraint IDTASK primary key (task_id));

create table USERS (
     user_id int not null AUTO_INCREMENT,
     deleted tinyint  not null,
     email varchar(50) not null UNIQUE,
     complete_name varchar(40) not null,
     encrypted_password varchar(128) not null,
     salt varchar(128) not null,
     constraint IDUSER primary key (user_id));


-- Constraints Section
-- ___________________ 

alter table LISTS add constraint FKmember
     foreign key (user_id)
     references USERS (user_id)
     ON DELETE CASCADE
     ON UPDATE CASCADE;

alter table NOTIFICATIONS add constraint FKreminder
     foreign key (task_id)
     references TASKS (task_id)
     ON DELETE CASCADE
     ON UPDATE CASCADE;

alter table TASKS add constraint FKcontainment
     foreign key (list_id)
     references LISTS (list_id)
     ON DELETE CASCADE
     ON UPDATE CASCADE;
-- Index Section
-- _____________ 

