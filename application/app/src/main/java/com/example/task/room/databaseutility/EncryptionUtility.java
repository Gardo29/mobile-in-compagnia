package com.example.task.room.databaseutility;

import androidx.annotation.NonNull;

import com.google.common.hash.Hashing;

import java.nio.charset.StandardCharsets;
import java.security.SecureRandom;
import java.util.Random;

public class EncryptionUtility {
    private static final int SALT_SIZE = 128;
    private static Random random = new SecureRandom();

    public static String getRandomSalt(){
        byte[] salt  = new byte[SALT_SIZE];
        random.nextBytes(salt);
        return Hashing.sha512().hashBytes(salt).toString();
    }
    public static String encrypt(@NonNull final String nonEncrypted,@NonNull final String salt){
        return digest(digest(nonEncrypted)+salt);
    }
    public static boolean equals(@NonNull final String nonEncrypted,@NonNull final String salt,@NonNull final String encrypted){
        return encrypt(nonEncrypted,salt).equals(encrypted);
    }
    public static String digest(@NonNull final String inputValue){
        return Hashing.sha512().hashString(inputValue, StandardCharsets.UTF_8).toString();
    }
}
