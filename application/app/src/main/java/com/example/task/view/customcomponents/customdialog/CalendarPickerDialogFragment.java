package com.example.task.view.customcomponents.customdialog;

import android.app.Dialog;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CalendarView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import com.example.task.R;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;

import org.threeten.bp.Instant;
import org.threeten.bp.LocalDate;
import org.threeten.bp.LocalDateTime;
import org.threeten.bp.ZoneId;
import org.threeten.bp.format.DateTimeFormatter;

import java.util.Calendar;

public class CalendarPickerDialogFragment extends DialogFragment {

    private final OnDateSelected listener;
    private long dateSelected;

    public CalendarPickerDialogFragment(@NonNull final OnDateSelected listener, final long dateSelected) {
        this.listener = listener;
        this.dateSelected = dateSelected;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        final View view = LayoutInflater.from(requireContext()).inflate(R.layout.fragment_dialog_calendar, null);
        final CalendarView calendarView = view.findViewById(R.id.calendarView);
        calendarView.setDate(dateSelected);
        dateSelected = LocalDateTime
                .ofInstant(Instant.ofEpochMilli(calendarView.getDate()), ZoneId.systemDefault())
                .toLocalDate()
                .atStartOfDay(ZoneId.systemDefault())
                .toInstant()
                .toEpochMilli();
        calendarView.setOnDateChangeListener((calendarViewDialog, year, month, dayOfMonth) -> {
            final long pickedDate = LocalDate.of(year,month+1,dayOfMonth)
                    .atStartOfDay(ZoneId.systemDefault())
                    .toInstant()
                    .toEpochMilli();

            if (pickedDate != dateSelected) {
                this.listener.onDateSelected(pickedDate);
                this.dismiss();
            }
        });
        return new MaterialAlertDialogBuilder(this.requireContext())
                .setView(view).create();
    }

    public interface OnDateSelected {
        void onDateSelected(long dateSelected);
    }


}
