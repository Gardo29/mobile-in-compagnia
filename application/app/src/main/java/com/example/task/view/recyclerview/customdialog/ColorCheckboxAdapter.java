package com.example.task.view.recyclerview.customdialog;

import android.content.res.ColorStateList;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.core.widget.CompoundButtonCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.example.task.R;

import java.util.List;

public class ColorCheckboxAdapter extends RecyclerView.Adapter<ColorCheckboxViewHolder> {

    private final List<ColorItem> colors;
    private final OnColorItemClick listener;

    public ColorCheckboxAdapter(@NonNull final List<ColorItem> colors,@NonNull final OnColorItemClick listener) {
        this.colors = colors;
        this.listener = listener;
    }

    @NonNull
    @Override
    public ColorCheckboxViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        final View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_checkbox,parent,false);
        return new ColorCheckboxViewHolder(view,(isChecked, position) -> {
            if(this.colors.get(position).isChecked() != isChecked){
                this.colors.get(position).setChecked(isChecked);
                this.listener.onColorItemClick(this.colors);
            }
        });
    }

    @Override
    public void onBindViewHolder(@NonNull ColorCheckboxViewHolder holder, int position) {
        CompoundButtonCompat.setButtonTintList(holder.checkBox, ColorStateList.valueOf(this.colors.get(position).getColor()));
        holder.checkBox.setChecked(this.colors.get(position).isChecked());
        holder.checkBox.setText(this.colors.get(position).getName());
    }

    @Override
    public int getItemCount() { return this.colors.size(); }

    public interface OnColorItemClick{ public void onColorItemClick(List<ColorItem> colors);}
}
