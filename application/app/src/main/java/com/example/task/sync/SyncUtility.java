package com.example.task.sync;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;

import androidx.annotation.NonNull;
import com.example.task.R;
import com.example.task.repetition.RepeatReceiver;

import org.threeten.bp.Duration;
import org.threeten.bp.LocalDateTime;
import org.threeten.bp.ZoneId;
import org.threeten.bp.ZonedDateTime;
import org.threeten.bp.temporal.ChronoUnit;

public class SyncUtility {
    private final static int ID = 1;
    private final static int MINUTES_AMOUNT = 1;
    private final static Duration duration = Duration.of(MINUTES_AMOUNT, ChronoUnit.MINUTES);

    public static void sync(Context context) {
        final AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        final ZonedDateTime dateTime = LocalDateTime.now()
                .plus(duration)
                .atZone(ZoneId.systemDefault());
        final long startTime = dateTime.toInstant().toEpochMilli();
        final PendingIntent pendingIntent = createPendingIntent(context);
        if (alarmManager != null) {
            if (Build.VERSION.SDK_INT < 23) {
                if (System.currentTimeMillis() < startTime) {
                    alarmManager.setExact(AlarmManager.RTC_WAKEUP, startTime, pendingIntent);
                }
            } else {
                if (System.currentTimeMillis() < startTime) {
                    alarmManager.setExactAndAllowWhileIdle(AlarmManager.RTC_WAKEUP, startTime, pendingIntent);
                }
            }
        }
    }
    private static PendingIntent createPendingIntent(@NonNull final Context context) {
        final Intent intent = new Intent(context, RepeatReceiver.class);
        intent.setAction(context.getString(R.string.action_sync))
                .setType("S" + ID);
        return PendingIntent.getBroadcast(context, ID, intent, PendingIntent.FLAG_UPDATE_CURRENT);
    }
}
