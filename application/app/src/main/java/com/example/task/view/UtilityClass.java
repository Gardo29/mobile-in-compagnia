package com.example.task.view;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.work.Constraints;
import androidx.work.ExistingPeriodicWorkPolicy;
import androidx.work.ExistingWorkPolicy;
import androidx.work.NetworkType;
import androidx.work.OneTimeWorkRequest;
import androidx.work.PeriodicWorkRequest;
import androidx.work.WorkManager;

import com.example.task.R;
import com.example.task.notification.NotificationSchedulerWorker;
import com.example.task.repetition.RepetitionSchedulerWorker;
import com.example.task.repetition.RepetitionUtility;
import com.example.task.room.entities.Task;
import com.example.task.room.repositories.TaskRepository;
import com.example.task.sync.SyncSchedulerWorker;

import org.threeten.bp.LocalDateTime;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

import java8.util.stream.StreamSupport;


public class UtilityClass {

    public static LocalDateTime nextNotificationTime = null;

    public static void setNextNotificationTime(final LocalDateTime newTime) {
        Log.e("YU", "next notification at " + newTime);
        nextNotificationTime = newTime;
    }

    public static LocalDateTime getNextNotificationTime() {
        return nextNotificationTime;
    }

    static public void addFragment(int layout, AppCompatActivity activity, Fragment fragment, String tag) {
        activity.getSupportFragmentManager()
                .beginTransaction()
                .addToBackStack(null)
                .add(layout, fragment, tag)
                .commit();
    }

    public static void replaceFragment(@NonNull final FragmentActivity activity, final int layoutId, @NonNull final Fragment fragment,@NonNull final String tag){
        activity.getSupportFragmentManager().beginTransaction()
                .addToBackStack(null)
                .replace(layoutId,fragment,tag).commit();
    }

    public static void replaceFragmentBundle(@NonNull final FragmentActivity activity, final int layoutId, @NonNull final Fragment fragment,
                                              @NonNull final String tag, @NonNull final Serializable data, @NonNull final String dataKey){
        final Bundle bundle = new Bundle();
        bundle.putSerializable(dataKey,data);
        fragment.setArguments(bundle);
        activity.getSupportFragmentManager().beginTransaction()
                .addToBackStack(null)
                .replace(layoutId,fragment,tag).commit();
    }

    public static void replaceFragmentNoBack(@NonNull final FragmentActivity activity, final int layoutId, @NonNull final Fragment fragment,
                                             @NonNull final String tag){
        activity.getSupportFragmentManager().beginTransaction()
                .replace(layoutId,fragment,tag).commit();
    }

    public static void replaceFragmentAnimation(@NonNull final FragmentActivity activity, final int layoutId, @NonNull final Fragment fragment,@NonNull final String tag){
        activity.getSupportFragmentManager().beginTransaction()
                .setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right)
                .addToBackStack(null)
                .replace(layoutId,fragment,tag).commit();
    }

    public static void replaceFragmentAnimationNoBackLeft(@NonNull final FragmentActivity activity, final int layoutId, @NonNull final Fragment fragment,@NonNull final String tag){
        activity.getSupportFragmentManager().beginTransaction()
                .setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right)
                .replace(layoutId,fragment,tag).commit();
    }

    public static void replaceFragmentAnimationNoBackRight(@NonNull final FragmentActivity activity, final int layoutId, @NonNull final Fragment fragment,@NonNull final String tag){
        activity.getSupportFragmentManager().beginTransaction()
                .setCustomAnimations(R.anim.enter_from_left, R.anim.exit_to_right, R.anim.enter_from_right, R.anim.exit_to_left)
                .replace(layoutId,fragment,tag).commit();
    }

    static public void closeFragment(FragmentActivity activity, Fragment fragment) {
        activity.getSupportFragmentManager()
                .beginTransaction()
                .remove(fragment)
                .commit();
    }

    public static void startActivityNoBack(@NonNull final Activity activity, @NonNull final Class<?> newActivityClass){
        activity.startActivity(new Intent(activity, newActivityClass));
        activity.finish();
    }

    public static void startActivity(@NonNull final Activity activity, @NonNull final Class<?> newActivityClass){
        activity.startActivity(new Intent(activity, newActivityClass));
    }

    public static void startActivityBundleForResult(@NonNull final Activity activity, @NonNull final Class<?> newActivityClass, @NonNull final Bundle bundleData, int requestCode){
        final Intent intent = new Intent(activity, newActivityClass);
        intent.putExtras(bundleData);
        activity.startActivityForResult(intent, requestCode);
    }

    public static void startActivityBundle(@NonNull final Activity activity, @NonNull final Class<?> newActivityClass, String keyBundle, Serializable data){
        final Intent intent = new Intent(activity, newActivityClass);
        final Bundle bundle = new Bundle();
        bundle.putSerializable(keyBundle, data);
        intent.putExtras(bundle);
        activity.startActivity(intent);
    }

    public static void focusToBackground(@NonNull Activity activity, final boolean hasFocus){
        if(hasFocus){
           closeKeyboard(activity);
        }
    }

    public static void showToast(@NonNull final Context context, @NonNull final String message){
        Toast.makeText(context, message,Toast.LENGTH_SHORT).show();
    }

    public static void noInternetConnectionToast(@NonNull final Context context){
        Toast.makeText(context,context.getString(R.string.no_internet_message),Toast.LENGTH_SHORT).show();
    }
    public static void serverErrorToast(@NonNull final Context context){
        Toast.makeText(context,"Impossible to contact the server. Try again",Toast.LENGTH_SHORT).show();
    }
    public static void serverResponseProblem(@NonNull final Context context){
        Toast.makeText(context,"Server response error",Toast.LENGTH_SHORT).show();
    }

    public static void submitServerProblem(@NonNull final Context context){
        Toast.makeText(context,"Impossible to send data to the server. Try again",Toast.LENGTH_SHORT).show();
    }

    public static void setupToolbar(@NonNull final AppCompatActivity activity, @NonNull final View view, final int toolbarId, @NonNull final String title){
        final Toolbar toolbar = view.findViewById(toolbarId);
        toolbar.setTitle(title);
        activity.setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(navigationView ->{
            activity.onBackPressed();
            closeKeyboard(activity);
        });
        Objects.requireNonNull(activity.getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
    }

    public static void setupToolbar(@NonNull final AppCompatActivity activity, @NonNull final View view, final int toolbarId){
        final Toolbar toolbar = view.findViewById(toolbarId);
        activity.setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(navigationView ->{
            activity.onBackPressed();
            closeKeyboard(activity);
        });
        Objects.requireNonNull(activity.getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
    }

    public static void setupToolbar(@NonNull final AppCompatActivity activity,final int toolbarId){
        final Toolbar toolbar = activity.findViewById(toolbarId);
        activity.setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(navigationView -> {
            activity.onBackPressed();
            closeKeyboard(activity);
        });
        Objects.requireNonNull(activity.getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
    }


    public static void openKeyboard(@NonNull final Activity activity) {
        activity.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
    }

    public static void closeKeyboard(@NonNull final Activity activity){
        InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        if(inputMethodManager != null && inputMethodManager.isAcceptingText()){
            inputMethodManager.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);
        }
    }

    /*Schedule the first (or more if at the same time) of the next 24 hours. Once the notification is fired by the alarm receiver,
    * this calls this function to schedule the next notification and so on.
    * When we add a new task, we call this function: if the new task has a notification sooner than the current one(s), the sooner
    * one will be scheduled; the older one(s) won't be deleted, but their intent will simply be overwritten once scheduled again
    * since their taskId is used as a unique ID.
    * When we update a task, we will call this function for the same reason as before*/
    public static void scheduleNextNotifications() {
        final WorkManager manager = WorkManager.getInstance();
        final OneTimeWorkRequest notificationRequest = new OneTimeWorkRequest.Builder(NotificationSchedulerWorker.class).build();
        manager.enqueueUniqueWork("Notification scheduler", ExistingWorkPolicy.KEEP, notificationRequest);
    }

    public static void scheduleRepetitions() {
        final WorkManager manager = WorkManager.getInstance();
        final OneTimeWorkRequest notificationRequest = new OneTimeWorkRequest.Builder(RepetitionSchedulerWorker.class).build();
        manager.enqueueUniqueWork("Repetition scheduler", ExistingWorkPolicy.KEEP, notificationRequest);
    }

    public static void scheduleSync() {
        final long REPEAT_SYNC_TIME = 15L;
        final WorkManager manager = WorkManager.getInstance();
        final Constraints constraints = new Constraints.Builder()
                .setRequiredNetworkType(NetworkType.CONNECTED)
                .build();
        final PeriodicWorkRequest syncRequest = new PeriodicWorkRequest.Builder(SyncSchedulerWorker.class,REPEAT_SYNC_TIME, TimeUnit.MINUTES)
                .setConstraints(constraints)
                .build();
        manager.enqueueUniquePeriodicWork("SyncWorker", ExistingPeriodicWorkPolicy.KEEP, syncRequest);
    }

    public static void repeatAllTasks(@NonNull final Context context) {
        final TaskRepository taskRepository = new TaskRepository(context);
        List<Task> repeatableTasks = taskRepository.getTaskDAO().getRepeatableTasks();
        if(repeatableTasks != null && repeatableTasks.size()>0){
            StreamSupport.stream(repeatableTasks).forEach(t->{
                if(t.getRepeat() != null && t.getRepeatUnit() != null){
                    final LocalDateTime now = LocalDateTime.now();
                    while(RepetitionUtility.startTime(t).plus(RepetitionUtility.getRepeat(t)).isBefore(now)){
                        t.repeat();
                    }
                    taskRepository.updateTask(t);
                }
            });
        }
    }
}
