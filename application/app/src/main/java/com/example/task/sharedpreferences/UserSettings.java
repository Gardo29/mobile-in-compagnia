package com.example.task.sharedpreferences;

import android.content.Context;
import android.content.SharedPreferences;
import androidx.annotation.NonNull;
import androidx.preference.PreferenceManager;
import com.example.task.view.homeactivity.TasksColors;
import org.threeten.bp.DayOfWeek;
import org.threeten.bp.Duration;
import java.util.Arrays;
import java.util.Map;
import java8.util.stream.Collectors;
import java8.util.stream.StreamSupport;
import static com.example.task.sharedpreferences.SettingKey.APP_VERSION;
import static com.example.task.sharedpreferences.SettingKey.DEFAULT_COLOR;
import static com.example.task.sharedpreferences.SettingKey.DEFAULT_LIST_SUMMARY;
import static com.example.task.sharedpreferences.SettingKey.DEFAULT_LIST_VALUE;
import static com.example.task.sharedpreferences.SettingKey.DEFAULT_SERVER_LIST;
import static com.example.task.sharedpreferences.SettingKey.ENCRYPTED_PASSWORD;
import static com.example.task.sharedpreferences.SettingKey.MUTE_ALL_NOTIFICATIONS;
import static com.example.task.sharedpreferences.SettingKey.SETTINGS_AVAILABLE;
import static com.example.task.sharedpreferences.SettingKey.USER_EMAIL;
import static com.example.task.sharedpreferences.SettingKey.DEFAULT_LIST;

public class UserSettings {
    private static final int DEFAULT_INT = -1;
    private static final String DEFAULT_STRING = "";
    private static final boolean DEFAULT_BOOL = false;

    private static final boolean DEFAULT_MUTE_ALL_NOTIFICATIONS = false;
    private static final String CURRENT_APP_VERSION = "1.0.0";

    private final SharedPreferences preferences;
    private final TasksColors tasksColors;

    public UserSettings(@NonNull final Context context){
        this.preferences = PreferenceManager.getDefaultSharedPreferences(context);
        this.tasksColors = new TasksColors(context);
        if(!this.areSettingsAvailable()){
            this.writeDefaultSettings();
        }
    }
    public Map<String,?> getAll(){
        return this.preferences.getAll();
    }

    public UserSettings putInt(@NonNull final SettingKey key, final Integer value){
        final SharedPreferences.Editor editor = this.preferences.edit();
        if (value != null) {
            editor.putInt(key.toString(),value);
            editor.apply();
        }
        return this;
    }
    public UserSettings putLong(@NonNull final SettingKey key, final long value){
        final SharedPreferences.Editor editor = this.preferences.edit();
        editor.putLong(key.toString(),value);
        editor.apply();
        return this;
    }

    public UserSettings putString(@NonNull final SettingKey key,@NonNull final String value){
        final SharedPreferences.Editor editor = this.preferences.edit();
        editor.putString(key.toString(),value);
        editor.apply();
        return this;
    }

    public int getInt(@NonNull final SettingKey key){
        return this.preferences.getInt(key.toString(), DEFAULT_INT);
    }

    public boolean getBoolean(@NonNull final SettingKey key){
        return this.preferences.getBoolean(key.toString(),DEFAULT_BOOL);
    }
    public String getString(@NonNull final SettingKey key){
        return this.preferences.getString(key.toString(),DEFAULT_STRING);
    }

    public void writeDefaultSettings(){
        final SharedPreferences.Editor editor = this.preferences.edit();
        editor.putBoolean(SETTINGS_AVAILABLE.toString(),true);
        editor.putInt(DEFAULT_COLOR.toString(),this.tasksColors.BLUE);
        editor.putBoolean(MUTE_ALL_NOTIFICATIONS.toString(),DEFAULT_MUTE_ALL_NOTIFICATIONS);
        editor.putString(APP_VERSION.toString(),CURRENT_APP_VERSION);
        editor.putString(DEFAULT_LIST_SUMMARY.toString(),DEFAULT_STRING);
        editor.putInt(DEFAULT_LIST_VALUE.toString(),DEFAULT_INT);
        editor.putInt(DEFAULT_SERVER_LIST.toString(), DEFAULT_INT);
        editor.putBoolean(DEFAULT_LIST.toString(),DEFAULT_BOOL);
        editor.apply();
    }

    public boolean isSet(@NonNull final SettingKey key){
        return this.preferences.contains(key.toString());
    }

    public void unset(@NonNull final SettingKey... keys){
        final SharedPreferences.Editor editor = this.preferences.edit();
        StreamSupport.stream(Arrays.asList(keys)).forEach(key->{
            editor.remove(key.toString());
        });
        editor.apply();
    }

    private boolean areSettingsAvailable(){ return this.getBoolean(SETTINGS_AVAILABLE); }

    @NonNull
    @Override
    public String toString(){
        return StreamSupport.stream(this.getAll().entrySet())
                .map(e->e.getKey()+": "+e.getValue().toString())
                .collect(Collectors.joining("\n"));
    }
}
