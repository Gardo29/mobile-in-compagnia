package com.example.task.view.recyclerview.interfaces;

public interface OnHolderClick {
    public void onHolderClick(int position);
}
