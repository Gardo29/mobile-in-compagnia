package com.example.task.view.homeactivity;

import android.content.Context;
import android.graphics.drawable.Drawable;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.core.graphics.drawable.DrawableCompat;

import com.example.task.R;

import java.util.Arrays;
import java.util.List;

public class TasksColors {
    public final int BROWN;
    public final int RED;
    public final int PURPLE;
    public final int YELLOW;
    public final int GREEN;
    public final int ORANGE;
    public final int BLUE;

    public TasksColors(@NonNull Context context){
        BROWN = ContextCompat.getColor(context,R.color.brown_task);
        RED = ContextCompat.getColor(context,R.color.red_task);
        PURPLE = ContextCompat.getColor(context,R.color.purple_task);
        YELLOW = ContextCompat.getColor(context,R.color.yellow_task);
        GREEN = ContextCompat.getColor(context,R.color.green_task);
        ORANGE = ContextCompat.getColor(context,R.color.orange_task);
        BLUE = ContextCompat.getColor(context,R.color.blue_task);
    }
    public String toColorName(final int color){
        if(color == RED){
            return "Red";
        }if(color == BLUE){
            return "Blue";
        }if(color == BROWN){
            return "Brown";
        }if(color == YELLOW){
            return "Yellow";
        }if(color == PURPLE){
            return "Purple";
        }if(color == GREEN){
            return "Green";
        }if(color == ORANGE) {
            return "Orange";
        }
        throw new IllegalArgumentException();
    }
    public List<Integer> colorsList(){
        return Arrays.asList(YELLOW, ORANGE,RED,GREEN,BLUE,PURPLE,BROWN);
    }
    public static Drawable colorDrawable(@NonNull final Drawable drawable,final int tint){
        final Drawable wrappedDrawable = DrawableCompat.wrap(drawable);
        DrawableCompat.setTint(wrappedDrawable, tint);
        return wrappedDrawable;
    }
}
