package com.example.task.view.homeactivity.login;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.task.R;
import com.example.task.room.databaseutility.DatabaseResult;
import com.example.task.sharedpreferences.SettingKey;
import com.example.task.sharedpreferences.UserSettings;
import com.example.task.view.UtilityTags;

import com.example.task.volley.DataBuilder;

import org.json.JSONException;


public class LoadingFragment extends AbstractLogin {

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        final View view = inflater.inflate(R.layout.fragment_loading, container, false);

        try {
            this.checkIfAlreadyLoggedIn();
        } catch (JSONException e) {
            this.jsonException();
        }
        return view;
    }

    private void checkIfAlreadyLoggedIn() throws JSONException {
        final UserSettings userSettings = new UserSettings(this.context);
        if (userSettings.isSet(SettingKey.USER_EMAIL) && userSettings.isSet(SettingKey.ENCRYPTED_PASSWORD)) {
            final String email = userSettings.getString(SettingKey.USER_EMAIL);
            final String encryptedPassword = userSettings.getString(SettingKey.ENCRYPTED_PASSWORD);
            final DataBuilder dataBuilder = new DataBuilder(this.activity.getApplication());
            if(!this.connectServer(dataBuilder.createUserLoginSubmit(email, encryptedPassword), success -> {
                this.tryCatch(() -> {
                    if (DatabaseResult.isResultCorrect(success)) {
                        this.startHomeActivity();
                    } else {
                        this.openLoginFragment();
                    }
                });
            }, failure -> this.connectionError())){
                this.startHomeActivity();
            }
        } else {
            this.openLoginFragment();
        }
    }

    private void openLoginFragment() {
        this.replaceFragmentAnimationLeft(new LoginFragment(), UtilityTags.FRAGMENT_LOGIN_TAG);
    }

    @Override
    public void jsonException() {
        this.openLoginFragment();
    }

    @Override
    public void connectionError() {
        this.openLoginFragment();
    }
}
