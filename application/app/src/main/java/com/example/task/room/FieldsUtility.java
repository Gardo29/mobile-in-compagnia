package com.example.task.room;

public class FieldsUtility {
    public static <T> T checkField(final T value, final boolean test){
        if(!test){
            throw new IllegalArgumentException();
        }
        else{
            return value;
        }
    }
}
