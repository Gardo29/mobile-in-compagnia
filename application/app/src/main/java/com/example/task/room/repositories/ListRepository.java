package com.example.task.room.repositories;

import android.app.Application;
import android.content.Context;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;


import com.example.task.room.TaskDatabase;
import com.example.task.room.entities.List;
import com.example.task.viewmodel.DataBuffers;

import java.nio.file.attribute.DosFileAttributes;

public class ListRepository extends AbstractRepository{

    private final DataBuffers dataBuffers;
    public ListRepository(@NonNull final Context context){
        super(context);
        dataBuffers = DataBuffers.getInstance();
    }

    public LiveData<java.util.List<List>> getLists(){
        return RepositoryUtility.getFuture(TaskDatabase.executor.submit(this.getListDAO()::getLists));
    }

    public List getListById(final int listId){
        List list = this.dataBuffers.getRoomList(listId);
        if(list == null){
            list = RepositoryUtility.getFuture(TaskDatabase.executor.submit(()-> this.getListDAO().getListById(listId)));
            if(list != null) {
                this.dataBuffers.putRoomList(listId, list);
            }
        }
        return list;
    }

    public List getListByServerId(final int serverListId){
        List list = this.dataBuffers.getServerList(serverListId);
        if(list == null){
            list = RepositoryUtility.getFuture(TaskDatabase.executor.submit(()-> this.getListDAO().getListByServerId(serverListId)));
            if(list != null) {
                this.dataBuffers.putServerList(serverListId, list);
            }
        }
        return list;
    }

    public java.util.List<List> getDirtyLists(){
        return RepositoryUtility.getFuture(TaskDatabase.executor.submit(()-> this.getListDAO().getDirtyLists()));
    }

    public String getListNameById(final int listId) {
        return RepositoryUtility.getFuture(TaskDatabase.executor.submit(() -> this.getListDAO().getListNameById(listId)));
    }
    public void clearDeletedLists(){
        TaskDatabase.executor.execute(()->this.getListDAO().clearDeletedLists());
    }

    public void addList(@NonNull final List list){
        TaskDatabase.executor.execute(()->this.getListDAO().insertList(list));
    }
    public long rawAddList(@NonNull final List list){
        return this.getListDAO().insertList(list);
    }
    public void updateList(@NonNull final List list){
        if(list.getServerId() != null){
            dataBuffers.putServerList(list.getServerId(),list);
        }
        dataBuffers.putRoomList(list.getListId(),list);
        TaskDatabase.executor.execute(()->this.getListDAO().updateList(list));
    }
    public void removeList(@NonNull final List list){
        TaskDatabase.executor.execute(()->this.getListDAO().deleteList(list));
    }
    public void deleteAllList(){
        TaskDatabase.executor.execute(()->this.getListDAO().deleteAllLists());
    }
}
