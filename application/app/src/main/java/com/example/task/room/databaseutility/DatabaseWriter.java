package com.example.task.room.databaseutility;

import android.content.Context;
import android.database.sqlite.SQLiteConstraintException;
import android.os.AsyncTask;
import android.telephony.AccessNetworkConstants;
import android.util.Log;

import androidx.annotation.NonNull;

import com.example.task.room.TaskDatabase;
import com.example.task.room.entities.List;
import com.example.task.room.entities.Notification;
import com.example.task.room.entities.Task;
import com.example.task.room.repositories.ListRepository;
import com.example.task.room.repositories.NotificationRepository;
import com.example.task.room.repositories.TaskRepository;
import com.example.task.sharedpreferences.RoomTs;
import com.example.task.view.UtilityClass;
import org.json.JSONArray;
import org.json.JSONException;
import org.threeten.bp.LocalDateTime;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import java8.util.stream.StreamSupport;
import static com.example.task.sharedpreferences.RoomKey.LISTS_TS;
import static com.example.task.sharedpreferences.RoomKey.NOTIFICATIONS_TS;
import static com.example.task.sharedpreferences.RoomKey.TASKS_TS;

public class DatabaseWriter {
    public static final int ERROR = -1;
    private static final boolean DIRTY = true;

    private final ListRepository listRepository;
    private final TaskRepository taskRepository;
    private final NotificationRepository notificationRepository;
    private final RoomTs ts;
    private final Context context;

    public DatabaseWriter(@NonNull final Context context){
        this.context = context;
        this.ts = new RoomTs(context);
        this.listRepository = new ListRepository(context);
        this.taskRepository = new TaskRepository(context);
        this.notificationRepository = new NotificationRepository(context);
    }
    public void updateDatabase(@NonNull final JSONArray data) throws JSONException {
        final JSONParser parser = new JSONParser(data);
        new MassiveUpdate(parser,this.context).execute();
    }
    /*       CREATION METHODS OFFLINE      */
    public void insertTask(@NonNull final Task task){
        task.setDirty(DIRTY);
        this.taskRepository.addTask(task);
    }
    public void insertList(@NonNull final List list){
        list.setDirty(DIRTY);
        UtilityClass.scheduleRepetitions();
        this.listRepository.addList(list);
    }
    public void insertNotification(@NonNull final Notification notification){
        notification.setDirty(DIRTY);
        this.notificationRepository.addNotification(notification);
        UtilityClass.scheduleNextNotifications();
    }
    /*      UPDATE METHODS ONLINE     */
    public int updateTask(@NonNull final Task task,@NonNull final JSONArray result) throws JSONException {
        if(DatabaseResult.isResultCorrect(result)) {
            final Task updatedTask = DatabaseResult.updateTask(task,result);
            if(updatedTask.isPermanentlyDelete()){
                this.taskRepository.removeTask(updatedTask);
            }else{
                this.taskRepository.updateTask(updatedTask);
            }
            this.ts.setTs(TASKS_TS,updatedTask.getTs());
            return updatedTask.getServerId();
        }
        return ERROR;
    }
    public int updateList(@NonNull final List list,@NonNull final JSONArray result) throws JSONException {
        if(DatabaseResult.isResultCorrect(result)) {
            final List updatedList = DatabaseResult.updateList(list, result);
            if (updatedList.isDeleted()) {
                this.listRepository.removeList(updatedList);
            } else {
                this.listRepository.updateList(updatedList);
            }
            this.ts.setTs(LISTS_TS, updatedList.getTs());
            return updatedList.getServerId();
        }
        return ERROR;
    }
    public int updateNotification(@NonNull final Notification notification,@NonNull final JSONArray result) throws JSONException {
        if(DatabaseResult.isResultCorrect(result)){
            final Notification updatedNotification = DatabaseResult.updateNotification(notification,result);
            if(updatedNotification.isDeleted()){
                this.notificationRepository.removeNotification(notification);
            }else{
                this.notificationRepository.removeNotification(notification);
                this.notificationRepository.addNotification(updatedNotification);
            }
            this.ts.setTs(NOTIFICATIONS_TS,updatedNotification.getTs());
            return updatedNotification.getTaskServerId();
        }
        return ERROR;
    }

    /*      UPDATE METHODS OFFLINE     */
    public void updateTask(@NonNull final Task task){
        task.setDirty(DIRTY);
        this.taskRepository.updateTask(task);
        UtilityClass.scheduleRepetitions();
    }
    public void updateList(@NonNull final List list){
        list.setDirty(DIRTY);
        this.listRepository.updateList(list);
    }

    public void updateChangeTimeNotification(@NonNull final Notification notification,@NonNull final LocalDateTime newTime,@NonNull final String unit){
        notification.setDeleted(true);
        final Notification newNotification = new Notification(newTime,notification.getTaskId());
        newNotification.setTaskServerId(notification.getTaskServerId());
        newNotification.setUnit(unit);

        this.updateNotification(notification);
        Log.e("YU", "update " + notification.getTime());
        this.insertNotification(newNotification);
        Log.e("YU", "insert " + newNotification.getTime());
    }
    public void updateNotification(@NonNull final Notification notification){
        notification.setDirty(DIRTY);
        this.notificationRepository.updateNotification(notification);
    }

    public void clearDeletedEntities(){
        this.taskRepository.clearDeletedTasks();
        this.listRepository.clearDeletedLists();
        this.notificationRepository.clearDeletedNotifications();
    }

    private class MassiveUpdate extends AsyncTask<Void,Void,Void>{
        private final Map<Integer,List> serverLists;
        private final Map<Integer,Task> serverTask;
        private final JSONParser parser;
        private final Context context;

        public MassiveUpdate (JSONParser parser,Context context){
            this.serverLists = new HashMap<>();
            this.serverTask = new HashMap<>();
            this.parser = parser;
            this.context = context;
        }
        @Override
        protected Void doInBackground(Void... voids) {
            this.manageList(this.parser);
            UtilityClass.repeatAllTasks(this.context);
            return null;
        }
        private void manageTask(@NonNull final JSONParser parser){
            final java.util.List<Task> tasks = parser.getTasks();
            if(tasks.size() == 0){
                this.manageNotifications(parser);
            }
            final AtomicInteger count = new AtomicInteger(tasks.size());
            StreamSupport.stream(tasks).forEach(task ->
                    this.manageSingleTask(task,()->{
                        if(count.decrementAndGet() == 0){
                            this.manageNotifications(parser);
                        }
                    }));
        }

        private void manageSingleTask(@NonNull final Task task,InsertDataCallback callback){
            final Task dbTask = taskRepository.getTaskByServerId(task.getServerId());
            ts.setTs(TASKS_TS,task.getTs());
            if(dbTask == null){
                final List list =
                        this.serverLists.containsKey(task.getServerListId()) ?
                                this.serverLists.get(task.getServerListId()) :
                                listRepository.getListByServerId(task.getServerListId());
                task.setListId(list.getListId());
                int taskId = (int) taskRepository.rawAddTask(task);
                task.setTaskId(taskId);
            }else{
                task.setTaskId(dbTask.getTaskId());
                task.setListId(dbTask.getListId());
                if(task.isPermanentlyDelete()){
                    taskRepository.removeTask(task);
                }else {
                    taskRepository.updateTask(task);
                }
            }
            this.serverTask.put(task.getServerId(),task);
            callback.onDataInserted();
        }
        private void manageList(@NonNull final JSONParser parser){
            final java.util.List<List> lists = parser.getLists();
            if(lists.size() == 0){
                this.manageTask(parser);
            }
            final AtomicInteger count = new AtomicInteger(lists.size());
            StreamSupport.stream(lists).forEach(list ->
                    this.manageSingleList(list,()->{
                        if(count.decrementAndGet() == 0){
                            this.manageTask(parser);
                        }
                    }));
        }
        private void manageSingleList(@NonNull final List list,@NonNull final InsertDataCallback callback){
            final List dbList = listRepository.getListByServerId(list.getServerId());
            ts.setTs(LISTS_TS,list.getTs());
            if(dbList == null){
                final int listId = (int) listRepository.rawAddList(list);
                list.setListId(listId);
            }else{
                list.setListId(dbList.getListId());
                if(list.isDeleted()){
                    listRepository.removeList(list);
                }else {
                    listRepository.updateList(list);
                }
            }
            this.serverLists.put(list.getServerId(),list);
            callback.onDataInserted();
        }
        private void manageNotifications(@NonNull final  JSONParser parser){
            final java.util.List<Notification> notifications = parser.getNotifications();
            StreamSupport.stream(notifications).forEach(notification -> {
                final Notification dbNotification = notificationRepository.getNotificationByServerTaskId(notification.getTaskServerId(),notification.getTime());
                ts.setTs(NOTIFICATIONS_TS,notification.getTs());
                if(dbNotification == null){
                    final Task task = this.serverTask.containsKey(notification.getTaskServerId()) ?
                            this.serverTask.get(notification.getTaskServerId()) :
                            taskRepository.getTaskByServerId(notification.getTaskServerId());
                    notification.setTaskId(task.getTaskId());
                    notificationRepository.addNotification(notification);
                }else{
                    if(notification.isDeleted()){
                        notificationRepository.removeNotification(notification);
                    }
                }
            });
        }
    }

    private interface InsertDataCallback{ void onDataInserted();}
}
