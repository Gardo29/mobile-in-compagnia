package com.example.task.view.customcomponents.customdialog;

import android.app.Dialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.recyclerview.widget.RecyclerView;

import com.example.task.R;
import com.example.task.view.homeactivity.TasksColors;
import com.example.task.view.recyclerview.customdialog.ColorCheckboxAdapter;
import com.example.task.view.recyclerview.customdialog.ColorItem;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;

import java.util.List;

import java8.util.stream.Collectors;
import java8.util.stream.StreamSupport;

public class ColorCheckboxDialogFragment extends DialogFragment implements ColorCheckboxAdapter.OnColorItemClick {

    private static final boolean DEFAULT_COLOR_STATE = true;
    private final OnColorsSelected listener;
    private List<ColorItem> colors;

    public ColorCheckboxDialogFragment(@NonNull final List<ColorItem> colors,@NonNull final OnColorsSelected listener) {
        this.listener = listener;
        this.colors = colors;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        final View view = LayoutInflater.from(requireContext()).inflate(R.layout.fragment_dialog_list,null);
        final TasksColors tasksColors = new TasksColors(requireContext());
        final RecyclerView recyclerView = view.findViewById(R.id.dialogRecyclerView);
        recyclerView.setAdapter(new ColorCheckboxAdapter(this.colors,this));
        return new MaterialAlertDialogBuilder(requireContext())
                .setView(view)
                .setTitle("Select tasks colors filter")
                .setNegativeButton("Cancel",((dialog, which) -> {this.dismiss();}))
                .setPositiveButton("OK",(dialog, which) -> { this.listener.onColorsSelected(this.colors); })
                .create();
    }

    @Override
    public void onColorItemClick(@NonNull final List<ColorItem> colors) { this.colors = colors; }

    public interface OnColorsSelected { public void onColorsSelected(List<ColorItem> colors);}
}
