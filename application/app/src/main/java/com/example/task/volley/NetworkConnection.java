package com.example.task.volley;

import android.app.Activity;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.ConnectivityManager.NetworkCallback;
import android.net.Network;
import android.net.NetworkCapabilities;
import android.net.NetworkRequest;
import android.os.Build;
import android.util.Log;

import androidx.annotation.NonNull;

public class NetworkConnection {

    private final Activity activity;
    private final NetworkCallback networkCallback;
    private boolean isNetworkConnected;
    private boolean isListenerAlreadySet = false;

    public NetworkConnection(@NonNull final Activity activity) {
        this.activity = activity;
        this.isNetworkConnected = false;
        this.networkCallback = new NetworkCallback(){
            @Override
            public void onAvailable(@NonNull final Network network) {
                super.onAvailable(network);
                isNetworkConnected = true;
            }
            @Override
            public void onLost(@NonNull final Network network) {
                super.onLost(network);
                isNetworkConnected = false; }
        };
    }

    public void registerNetworkCallback(){
        if(!this.isListenerAlreadySet){
            ConnectivityManager connectivityManager = (ConnectivityManager) this.activity.getSystemService(Context.CONNECTIVITY_SERVICE);
            if (connectivityManager != null) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    connectivityManager.registerDefaultNetworkCallback(this.networkCallback);
                } else {
                    NetworkRequest networkRequest = new NetworkRequest.Builder()
                            .addTransportType(NetworkCapabilities.TRANSPORT_CELLULAR)
                            .addTransportType(NetworkCapabilities.TRANSPORT_WIFI)
                            .build();
                    connectivityManager.registerNetworkCallback(networkRequest, networkCallback);
                }
            } else {
                this.isNetworkConnected = false;
            }
            this.isListenerAlreadySet = true;
        }
    }
    public void unregisterCallback(){
        if(isListenerAlreadySet){
            ConnectivityManager connectivityManager = (ConnectivityManager) this.activity.getSystemService(Context.CONNECTIVITY_SERVICE);
            if (connectivityManager != null) {
                connectivityManager.unregisterNetworkCallback(this.networkCallback);
            }
            this.isListenerAlreadySet = false;
        }
    }

    public boolean isNetworkConnected() {
        return this.isNetworkConnected;
    }
}
