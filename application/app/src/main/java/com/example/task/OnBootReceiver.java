package com.example.task;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.example.task.view.UtilityClass;
import java.util.Objects;

public class OnBootReceiver extends BroadcastReceiver {

    final static String ON_BOOT = "android.intent.action.BOOT_COMPLETED";

    @Override
    public void onReceive(Context context, Intent intent) {
        if (Objects.equals(intent.getAction(), ON_BOOT)) {
            UtilityClass.scheduleSync();
            UtilityClass.repeatAllTasks(context);
            UtilityClass.scheduleNextNotifications();
            UtilityClass.scheduleRepetitions();
        }
    }

}
