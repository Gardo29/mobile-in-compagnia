package com.example.task.view.recyclerview.interfaces;

import com.example.task.room.entities.Task;

public interface OnTaskClick {

    public void onTaskClick(Task task);
}
