package com.example.task.view.recyclerview;

import android.graphics.Paint;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.example.task.R;
import com.example.task.view.customcomponents.RoundCheckBox;
import com.example.task.view.recyclerview.interfaces.OnCheckBoxHolderClick;
import com.example.task.view.recyclerview.interfaces.OnHolderClick;

class ListTaskViewHolder extends RecyclerView.ViewHolder {

    final TextView taskTitle;
    final TextView date;
    final RoundCheckBox checkBox;
    final CardView cardView;

    public ListTaskViewHolder(@NonNull View itemView, @NonNull OnCheckBoxHolderClick checkBoxListener, @NonNull OnHolderClick cardViewListener, @NonNull Boolean done) {
        super(itemView);
        this.taskTitle = itemView.findViewById(R.id.taskName);
        this.date = itemView.findViewById(R.id.taskDate);
        this.checkBox = itemView.findViewById(R.id.roundCheckBox);
        this.cardView = itemView.findViewById(R.id.cardView);

        this.checkBox.setDone(done);
        if(done){
            this.taskTitle.setPaintFlags(taskTitle.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        }

        this.checkBox.setOnClickListener(l -> {
            checkBoxListener.onCheckBoxHolderClick(getAdapterPosition(),this.checkBox.isDone());
        });

        this.cardView.setOnClickListener(l->
                cardViewListener.onHolderClick(getAdapterPosition()));
    }
}
