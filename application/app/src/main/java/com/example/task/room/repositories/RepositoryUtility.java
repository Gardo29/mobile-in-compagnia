package com.example.task.room.repositories;

import androidx.annotation.NonNull;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

public class RepositoryUtility {
    public static <T> T getFuture(@NonNull Future<T> future){
        T result = null;
        try {
            result = future.get();
        }catch (ExecutionException |InterruptedException e){
            e.printStackTrace(); //TODO: catch of exception
        }
        return result;
    }
}
