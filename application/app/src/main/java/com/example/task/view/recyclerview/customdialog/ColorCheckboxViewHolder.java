package com.example.task.view.recyclerview.customdialog;

import android.view.View;
import android.widget.CheckBox;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.task.R;

public class ColorCheckboxViewHolder extends RecyclerView.ViewHolder {
    final CheckBox checkBox;
    public ColorCheckboxViewHolder(@NonNull View itemView,@NonNull final OnColorCheckChange listener) {
        super(itemView);
        this.checkBox = itemView.findViewById(R.id.colorCheckbox);
        this.checkBox.setOnCheckedChangeListener(((buttonView, isChecked) -> {
            listener.onColorCheckChange(isChecked,getAdapterPosition());
        }));
    }

    public interface OnColorCheckChange{ public void onColorCheckChange(boolean isChecked,int position);}
}
