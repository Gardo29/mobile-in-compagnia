package com.example.task.view.customcomponents.customdialog;

import android.app.Dialog;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.DialogFragment;

import com.example.task.R;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.google.android.material.textfield.TextInputLayout;

import org.threeten.bp.temporal.ChronoUnit;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java8.util.stream.StreamSupport;


public class TimeSelectionDialogFragment extends DialogFragment {

    private static final int NUMBER_MAX_SIZE = 3;
    private static final String PLURAL = "s";
    private static final List<ChronoUnit> times = Arrays.asList(ChronoUnit.MINUTES,ChronoUnit.HOURS,ChronoUnit.DAYS,ChronoUnit.WEEKS,ChronoUnit.MONTHS,ChronoUnit.YEARS);
    private final OnTimeSelected selected;
    private final OnSelectionCanceled canceled;
    private final String title;
    private final Mode mode;
    private EditText valueEditText;
    private List<RadioButton> radioButtons;
    private View view;
    private Button positiveButton;

    public TimeSelectionDialogFragment(@NonNull final String dialogTitle,@NonNull final OnTimeSelected selected,
                                       @NonNull final OnSelectionCanceled canceled, @NonNull final Mode mode){
        this.title = dialogTitle;
        this.selected = selected;
        this.canceled = canceled;
        this.mode = mode;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        this.view = LayoutInflater.from(requireContext()).inflate(R.layout.fragment_dialog_time_selection,null);
        this.valueEditText = ((TextInputLayout)view.findViewById(R.id.timeSelectorTextInput)).getEditText();
        this.radioButtons = this.getRadioButtonsList();
        this.valueEditText.addTextChangedListener(this.createEditTextListener());

        ((TextView)view.findViewById(R.id.timeSelectionTitle)).setText(this.title);

        final MaterialAlertDialogBuilder builder = new MaterialAlertDialogBuilder(requireActivity());
        builder.setView(this.view)
        .setPositiveButton("OK",((dialog, which) -> {
            this.selected.onTimeSelected(Integer.parseInt(this.valueEditText.getText().toString()),this.checkboxToChronoUnit());
        })).setNegativeButton("Cancel",((dialog, which) -> this.canceled.onSelectionCanceled()));

        final AlertDialog alertDialog = builder.create();
        alertDialog.setOnShowListener(l->{
            this.positiveButton = alertDialog.getButton(AlertDialog.BUTTON_POSITIVE);
            this.positiveButton.setEnabled(false);
            this.positiveButton.setTextColor(Color.GRAY);
        });
        return alertDialog;
    }

    private TextWatcher createEditTextListener(){
        return new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) { }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String value = valueEditText.getText().toString();
                int num = Integer.MAX_VALUE;
                if(!value.isEmpty()){
                    num = Integer.parseInt(value);
                }
                final boolean disableCondition = value.isEmpty() || num == 0;
                positiveButton.setTextColor(disableCondition  ? Color.GRAY : Color.BLUE);
                positiveButton.setEnabled(!disableCondition);

                if(value.length() > NUMBER_MAX_SIZE){
                    value = value.substring(0,NUMBER_MAX_SIZE);
                    valueEditText.setText(value);
                }
                final boolean isPlural = !value.isEmpty() && Integer.parseInt(value) > 1;
                StreamSupport.stream(radioButtons).forEach(e->e.setText(applyFormatterIfPlural(e.getText().toString(),isPlural)));
            }
            @Override
            public void afterTextChanged(Editable s) { }
        };
    }

    private List<RadioButton> getRadioButtonsList(){
        final RadioGroup radioGroup = this.view.findViewById(R.id.timeRadioGroup);
        if(this.mode == Mode.REDUCED){
            radioGroup.removeViewAt(0);
            radioGroup.removeViewAt(0);
        }
        ((RadioButton)radioGroup.getChildAt(0)).setChecked(true);
        final List<RadioButton> radioButtons = new ArrayList<>();
        for(int i = 0; i< radioGroup.getChildCount(); i++){
            radioButtons.add((RadioButton)radioGroup.getChildAt(i));
        }
        return radioButtons;
    }

    private String applyFormatterIfPlural(@NonNull final String string,final boolean plural){
        if(!string.isEmpty()){
            if(plural && !string.endsWith(PLURAL)){
                return string + PLURAL;
            }
            // string is singular
            if(!plural && string.endsWith(PLURAL)){
                return string.substring(0, string.length() - 1);
            }
        }
        return string;
    }

    private ChronoUnit checkboxToChronoUnit(){
        for(int i = 0; i < this.radioButtons.size() && i < times.size();i++){
            final RadioButton current = this.radioButtons.get(i);
            if(current.isChecked()){
                return this.mode == Mode.FULL ? times.get(i) : times.get(i+2);
            }
        }
        // default value
        return ChronoUnit.MINUTES;
    }

    public interface OnTimeSelected{ void onTimeSelected(int quantity, ChronoUnit timeUnit);}
    public enum Mode { FULL,REDUCED }
}
