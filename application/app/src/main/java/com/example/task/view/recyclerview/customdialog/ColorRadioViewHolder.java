package com.example.task.view.recyclerview.customdialog;

import android.view.View;
import android.widget.RadioButton;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.task.R;

public class ColorRadioViewHolder extends RecyclerView.ViewHolder {

    final RadioButton radioButton;

    public ColorRadioViewHolder(@NonNull final View itemView, @NonNull final ColorRadioAdapter.OnItemClick listener) {
        super(itemView);
        this.radioButton = itemView.findViewById(R.id.colorRadiobutton);
        this.radioButton.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if(isChecked){
                listener.onItemClick(getAdapterPosition());
            }
        });
    }
}
