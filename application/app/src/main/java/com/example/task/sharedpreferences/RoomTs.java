package com.example.task.sharedpreferences;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;

import androidx.annotation.NonNull;

import java.util.HashMap;
import java.util.Map;

import java8.util.stream.StreamSupport;


public class RoomTs {

    public static final String SHARED_FILE_ID = "ROOM_TS";
    public static final int DEFAULT_TS = -1;
    private final SharedPreferences roomPreferences;
    private final Map<RoomKey,Integer> accumulate;

    public RoomTs(@NonNull final Context context){
        this.roomPreferences = context.getSharedPreferences(SHARED_FILE_ID, Context.MODE_PRIVATE);
        this.accumulate = new HashMap<>();
    }
    public RoomTs setTs(@NonNull final RoomKey key,final int value){
        final SharedPreferences.Editor editor = this.roomPreferences.edit();
        if(value > this.roomPreferences.getInt(key.toString(),DEFAULT_TS)){
            editor.putInt(key.toString(),value);
            editor.apply();
        }
        return this;
    }
    public int getTs(@NonNull final RoomKey key){
        return this.roomPreferences.getInt(key.toString(),DEFAULT_TS);
    }

    public void accumulate(@NonNull final RoomKey key,final int value){
        if(!this.accumulate.containsKey(key)){
            this.accumulate.put(key,value);
        }else if(this.accumulate.get(key) < value){
            this.accumulate.put(key,value);
        }
    }

    public void apply(){
        StreamSupport.stream(this.accumulate.entrySet()).forEach(e-> this.setTs(e.getKey(),e.getValue()));
        this.accumulate.clear();
    }
}
