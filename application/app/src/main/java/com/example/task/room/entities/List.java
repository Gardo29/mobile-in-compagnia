package com.example.task.room.entities;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;


import com.example.task.room.FieldsUtility;

import org.threeten.bp.LocalDateTime;

import java.io.Serializable;

@Entity(tableName = "lists")
public class List implements Serializable {
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "list_id")
    private int listId;
    @ColumnInfo(name = "server_id")
    private Integer serverId;
    private String name;
    private String description;
    @ColumnInfo(name = "creation_date")
    private LocalDateTime creationDate;
    @ColumnInfo(name = "display_home")
    private boolean displayHome;
    private boolean dirty;
    @ColumnInfo(name = "enabled_notifications")
    private boolean enabledNotifications;
    private boolean deleted;
    private int ts;

    public List(@NonNull String name){
        this.name =  FieldsUtility.checkField(name,!name.isEmpty());
    }

    public boolean isEnabledNotifications() { return this.enabledNotifications; }

    public Integer getServerId() { return this.serverId; }

    public int getListId() {
        return this.listId;
    }

    public String getName() {
        return this.name;
    }

    public String getDescription() {
        return this.description;
    }

    public LocalDateTime getCreationDate() {
        return this.creationDate;
    }

    public int getTs() { return this.ts; }

    public boolean isDisplayHome() { return this.displayHome; }

    public boolean isDirty() { return this.dirty; }

    public boolean isDeleted(){return this.deleted;}

    public void setDirty(final boolean dirty) { this.dirty = dirty; }

    public void setName(@NonNull final String name) { this.name = name; }

    public void setTs(int ts) { this.ts = ts; }

    public void setServerId(final Integer serverId) { this.serverId = serverId; }

    public void setDeleted(boolean deleted) { this.deleted = deleted; }

    public void setDisplayHome(final boolean displayHome) { this.displayHome = displayHome; }

    public void setListId(final int id) {
        this.listId = id;
    }

    public void setEnabledNotifications(boolean enabledNotifications) { this.enabledNotifications = enabledNotifications; }

    public void setDescription(final String description){ this.description  = description; }

    public void setCreationDate(@NonNull final LocalDateTime creationDate) { this.creationDate = creationDate; }
}
