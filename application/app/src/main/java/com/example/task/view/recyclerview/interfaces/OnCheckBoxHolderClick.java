package com.example.task.view.recyclerview.interfaces;

public interface OnCheckBoxHolderClick {
    public void onCheckBoxHolderClick(int position,boolean isChecked);
}
