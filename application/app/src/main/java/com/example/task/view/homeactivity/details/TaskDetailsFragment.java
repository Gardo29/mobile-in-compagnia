package com.example.task.view.homeactivity.details;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import com.example.task.R;
import com.example.task.notification.NotificationUtility;
import com.example.task.repetition.RepetitionUtility;
import com.example.task.room.databaseutility.DatabaseWriter;
import com.example.task.room.entities.Notification;
import com.example.task.room.entities.Task;
import com.example.task.view.UtilityClass;
import com.example.task.view.UtilityTags;
import com.example.task.view.creationactivity.CreationActivity;
import com.example.task.view.creationactivity.UpdateTaskFragment;
import com.example.task.view.homeactivity.TasksColors;
import com.example.task.viewmodel.GetListsViewModel;
import com.example.task.viewmodel.GetTasksViewModel;

import org.threeten.bp.Duration;
import org.threeten.bp.LocalDateTime;
import org.threeten.bp.format.DateTimeFormatter;
import org.threeten.bp.temporal.ChronoUnit;

import java.io.Serializable;
import java.util.List;

import java8.util.stream.Collectors;
import java8.util.stream.StreamSupport;

public class TaskDetailsFragment extends Fragment {

    private static final DateTimeFormatter formatterDate = DateTimeFormatter.ofPattern("EEEE, MMM d uuu");
    private static final DateTimeFormatter formatterDateTime = DateTimeFormatter.ofPattern("EEEE, MMM d '-' HH:mm");
    private Task task;
    private View view;
    private LocalDateTime completionTime;
    private LocalDateTime deletionTime;
    private AppCompatActivity activity;
    private GetTasksViewModel tasksViewModel;
    private List<Notification> notifications;
    private TextView taskName;
    private TextView taskDate;
    private TextView taskDescription;
    private DatabaseWriter databaseWriter;
    private TextView notificationsTaskView;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        this.view = inflater.inflate(R.layout.fragment_task_details, container, false);
        this.task = (Task) requireArguments().getSerializable(UtilityTags.BUNDLE_TASK);
        this.tasksViewModel = new ViewModelProvider(this).get(GetTasksViewModel.class);
        if (this.task != null) {
            this.taskName = view.findViewById(R.id.taskName);
            this.taskDescription = view.findViewById(R.id.taskDescriptionDetails);
            this.taskName.setText(this.task.getTitle());
            this.taskDate = view.findViewById(R.id.taskCreationDate);
            this.taskDate.setText(getResources().getString(R.string.task_date, this.task.getCreationDate().format(formatterDate)));
            this.completionTime = this.task.getCompletionTime();
            this.deletionTime = this.task.getDeletionTime();
        }
        this.activity = (AppCompatActivity) requireActivity();
        this.databaseWriter = new DatabaseWriter(activity);
        this.setHasOptionsMenu(true);
        this.setTimeDate();
        this.setNotifications();
        this.setRepeatTime();
        this.setCompletedOrUncompleted();
        this.setDeleteOrRestore();
        this.printColor();
        this.setDescription();
        UtilityClass.setupToolbar(this.activity, this.view, R.id.detailsToolbar);
        return view;
    }

    private void setRepeatTime() {
        final TextView repeatTime = this.view.findViewById(R.id.repetitionTimeTaskDetails);

        if (this.task.getRepeat() == null || this.task.getRepeat() == 0) {
            repeatTime.setVisibility(View.GONE);
            this.view.findViewById(R.id.view6).setVisibility(View.GONE);
            RepetitionUtility.cancelRepetition(requireContext(), this.task);
        } else {
            repeatTime.setVisibility(View.VISIBLE);
            this.view.findViewById(R.id.view6).setVisibility(View.VISIBLE);
            repeatTime.setText("Every " + this.task.getRepeat().toString() + " " + this.task.getRepeatUnit().toLowerCase());
        }
    }

    private void setNotifications() {
        this.notificationsTaskView = this.view.findViewById(R.id.notificationsTaskDetails);
        this.notifications = StreamSupport.stream(this.tasksViewModel.getAllNotifications())
                .filter(reminder -> reminder.getTask().getTaskId() == this.task.getTaskId())
                .findFirst().get().getNotifications();
        this.setNotificationTextView();
    }

    private void setNotificationTextView() {
        if (notifications.isEmpty()) {
            notificationsTaskView.setVisibility(View.GONE);
            this.view.findViewById(R.id.view5).setVisibility(View.GONE);
            Log.e("YU", "empty notif");
        } else {
            notificationsTaskView.setVisibility(View.VISIBLE);
            this.view.findViewById(R.id.view5).setVisibility(View.VISIBLE);
            notificationsTaskView.setText(StreamSupport.stream(notifications)
                    .sorted((a, b) -> - a.getTime().compareTo(b.getTime()))
                    .filter(notification -> !notification.isDeleted())
                    .map(notification -> {
                        Log.e("YU", notification.getTime() + " " + notification.isDeleted());
                        if (ChronoUnit.valueOf(notification.getUnit()).between(notification.getTime(), this.task.getStart()) > 0) {
                            return (ChronoUnit.valueOf(notification.getUnit()).between(notification.getTime(), this.task.getStart())
                                    + " " + notification.getUnit().toLowerCase() + " before");
                        } else {
                            LocalDateTime nexTime = this.task.getStart().plus(Duration.of(this.task.getRepeat(), ChronoUnit.valueOf(this.task.getRepeatUnit())));
                            return (ChronoUnit.valueOf(notification.getUnit()).between(notification.getTime(), nexTime)
                                    + " " + notification.getUnit().toLowerCase() + " before");
                        }
                    })
                    .collect(Collectors.joining("\n")));
            Log.e("YU", "count " + StreamSupport.stream(notifications)
                    .sorted((a, b) -> - a.getTime().compareTo(b.getTime()))
                    .filter(notification -> !notification.isDeleted())
                    .count());
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        if (!this.compareDateTimes(this.task.getCompletionTime(), this.completionTime)) {
            this.task.setCompletionTime(this.completionTime);
        }
        if (!this.compareDateTimes(this.task.getDeletionTime(), this.deletionTime)) {
            this.task.setDeletionTime(this.deletionTime);
        }
        databaseWriter.updateTask(this.task);
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.details_toolbar, menu);
        final MenuItem edit = menu.findItem(R.id.detailsModify);
        final GetListsViewModel listsViewModel = new ViewModelProvider(requireActivity()).get(GetListsViewModel.class);
        final com.example.task.room.entities.List list = listsViewModel.getListById(this.task.getListId());
        edit.setOnMenuItemClickListener(l -> {
            final Bundle bundle = new Bundle();
            bundle.putSerializable(UtilityTags.CREATION_FRAGMENT, UpdateTaskFragment.class);
            bundle.putSerializable(UtilityTags.BUNDLE_TASK, this.task);
            bundle.putSerializable(UtilityTags.BUNDLE_NOTIFICATION, (Serializable) this.notifications);
            bundle.putSerializable(UtilityTags.BUNDLE_LIST, list);
            final Intent intent = new Intent(activity, CreationActivity.class);
            intent.putExtras(bundle);
            startActivityForResult(intent, UtilityTags.ACTIVITY_UPDATE_TASK);
            return true;
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == UtilityTags.ACTIVITY_UPDATE_TASK && resultCode == Activity.RESULT_OK) {
            assert data != null;
            this.task = (Task) data.getSerializableExtra(UtilityTags.BUNDLE_TASK);
            assert this.task != null;
            this.taskName.setText(this.task.getTitle());
            this.taskDate.setText(getResources().getString(R.string.task_date, this.task.getCreationDate().format(formatterDate)));
            this.completionTime = this.task.getCompletionTime();
            this.deletionTime = this.task.getDeletionTime();
            this.setTimeDate();
            this.setRepeatTime();
            this.printColor();
            this.setDescription();

            //TODO this is the stupid method c: -YU
            NotificationUtility.cancelAlarm(requireContext(), this.task.getTaskId());
            final List<Notification> newNotification = (List<Notification>) data.getSerializableExtra(UtilityTags.BUNDLE_NOTIFICATION);

            assert newNotification != null;
            if (!(this.notifications.isEmpty() && newNotification.isEmpty())) {

                StreamSupport.stream(this.notifications).forEach(notification -> {
                    notification.setDeleted(true);
                    this.databaseWriter.updateNotification(notification);
                });

                this.notifications.clear();

                StreamSupport.stream(newNotification).forEach(notification -> {
                    notification.setTaskId(this.task.getTaskId());
                    notification.setTaskServerId(this.task.getServerId());
                    this.databaseWriter.insertNotification(notification);
                    this.notifications.add(notification);
                });
            }
            this.setNotificationTextView();
        }
    }

    private void setTimeDate() {
        final TextView timeDate = this.view.findViewById(R.id.taskTimeDetails);
        timeDate.setText(this.task.getStart() == null ? "Daily" : this.task.getStart().format(formatterDateTime) + "\n" + this.task.getEnd().format(formatterDateTime));
    }

    private void setCompletedOrUncompleted() {
        final TextView completedOrUncompleted = this.view.findViewById(R.id.completedOrUncompleted);
        if (this.task.getCompletionTime() != null) {
            this.completedTaskTextView(completedOrUncompleted);
        } else {
            this.uncompletedTaskTextView(completedOrUncompleted);
        }
        completedOrUncompleted.setOnClickListener(l -> {
            if (this.completionTime == null) {
                this.completionTime = LocalDateTime.now();
                this.completedTaskTextView(completedOrUncompleted);
            } else {
                this.completionTime = null;
                this.uncompletedTaskTextView(completedOrUncompleted);
            }
        });
    }

    private void setDeleteOrRestore() {
        final TextView deleteOrRestore = this.view.findViewById(R.id.deletedOrRestoreDetails);
        if (this.task.getDeletionTime() != null) {
            this.setRestored(deleteOrRestore);
        } else {
            this.setDeleted(deleteOrRestore);
        }
        deleteOrRestore.setOnClickListener(l -> {
            if (this.deletionTime == null) {
                this.deletionTime = LocalDateTime.now();
                if (!this.notifications.isEmpty()) {
                    NotificationUtility.cancelAlarm(requireContext(), this.task.getTaskId());
                }
                this.setRestored(deleteOrRestore);
            } else {
                this.deletionTime = null;
                if (!this.notifications.isEmpty()) {
                    UtilityClass.scheduleNextNotifications();
                }
                this.setDeleted(deleteOrRestore);
            }
        });
    }

    private void setDeleted(@NonNull final TextView restored) {
        final Drawable icon = getResources().getDrawable(R.drawable.bin_red);
        restored.setText("Delete");
        restored.setTextColor(Color.RED);
        this.setIcon(restored, icon);
    }

    private void setRestored(@NonNull final TextView deleted) {
        final Drawable icon = getResources().getDrawable(R.drawable.restore_blue);
        deleted.setText("Restore");
        deleted.setTextColor(Color.BLUE);
        this.setIcon(deleted, icon);
    }

    private void completedTaskTextView(@NonNull final TextView completed) {
        final Drawable uncompletedIcon = getResources().getDrawable(R.drawable.barred_check);
        this.setIcon(completed, uncompletedIcon);
        completed.setText("Mark as uncompleted");
    }

    private void uncompletedTaskTextView(@NonNull final TextView uncompleted) {
        final Drawable uncompletedIcon = getResources().getDrawable(R.drawable.check);
        this.setIcon(uncompleted, uncompletedIcon);
        uncompleted.setText("Mark as completed");
    }

    private void printColor() {
        final TextView color = this.view.findViewById(R.id.taskColorDetails);
        final TasksColors tasksColors = new TasksColors(this.requireContext());
        final Drawable colorCircle = getResources().getDrawable(R.drawable.black_circle);
        final int tint = this.task.getColor();
        this.setIcon(color, TasksColors.colorDrawable(colorCircle, tint));
        color.setText(tasksColors.toColorName(this.task.getColor()));
    }

    private void setIcon(@NonNull final TextView textView, @NonNull final Drawable icon) {
        textView.setCompoundDrawablesWithIntrinsicBounds(icon, null, null, null);
    }

    private void setDescription() {
        if (this.task.getDescription() != null) {
            this.taskDescription.setVisibility(View.VISIBLE);
            this.taskDescription.setText(this.task.getDescription());
        } else {
            this.taskDescription.setVisibility(View.GONE);
            this.view.findViewById(R.id.view2).setVisibility(View.GONE);
        }
    }

    private boolean compareDateTimes(final LocalDateTime time1, final LocalDateTime time2) {
        if (time1 == time2) {
            return true;
        }
        if (time1 != null && time2 != null) {
            return time1.equals(time2);
        }
        return false;
    }
}
