package com.example.task.sync;
import android.content.Context;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.work.Worker;
import androidx.work.WorkerParameters;

import com.example.task.room.databaseutility.DatabaseResult;
import com.example.task.sharedpreferences.SettingKey;
import com.example.task.sharedpreferences.UserSettings;
import com.example.task.view.UtilityClass;
import com.example.task.view.homeactivity.login.AbstractLogin;
import com.example.task.volley.DataBuilder;
import com.example.task.volley.DatabaseUpdater;
import com.example.task.volley.RequestManager;

import org.json.JSONException;

import java.util.concurrent.atomic.AtomicReference;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.stream.StreamSupport;

public class SyncSchedulerWorker extends Worker {
    private final DatabaseUpdater databaseUpdater;
    private final DataBuilder dataBuilder;
    private final UserSettings userSettings;
    private final RequestManager requestManager;

    public SyncSchedulerWorker(@NonNull Context context, @NonNull WorkerParameters workerParams) {
        super(context, workerParams);
        this.dataBuilder = new DataBuilder(context);
        this.userSettings = new UserSettings(context);
        this.databaseUpdater = new DatabaseUpdater(context);
        this.requestManager = new RequestManager(context);
    }

    @NonNull
    @Override
    public Result doWork() {
        if(this.userSettings.isSet(SettingKey.USER_EMAIL) && this.userSettings.isSet(SettingKey.ENCRYPTED_PASSWORD)){
            final String email = this.userSettings.getString(SettingKey.USER_EMAIL);
            final String encryptedPassword = this.userSettings.getString(SettingKey.ENCRYPTED_PASSWORD);
            this.tryCatch(()->{
                this.requestManager.sendData(this.dataBuilder.createUserLoginSubmit(email, encryptedPassword), success -> {
                    this.tryCatch(()-> {
                        if (DatabaseResult.isResultCorrect(success)) {
                            this.databaseUpdater.syncData(
                                    requestFailure->Log.e("log","requestFailure"),
                                    submitFailure->Log.e("log","submitFailure"),
                                    jsonError-> Log.e("log","jsonError"));
                            UtilityClass.scheduleNextNotifications();
                            UtilityClass.scheduleRepetitions();
                        }else{
                            this.userSettings.unset(SettingKey.USER_EMAIL,SettingKey.ENCRYPTED_PASSWORD);
                        }
                    });
                }, failure -> {});
            });
        }
        return Result.retry();
    }

    public void tryCatch(AbstractLogin.VoidFunction givenFunction){
        try {
            givenFunction.executeAction();
        } catch (JSONException e) {
        }
    }
}
