package com.example.task.view.homeactivity.search;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.core.util.Pair;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.RecyclerView;

import com.example.task.R;
import com.example.task.room.entities.Task;
import com.example.task.view.UtilityClass;
import com.example.task.view.UtilityTags;
import com.example.task.view.customcomponents.customdialog.ColorCheckboxDialogFragment;
import com.example.task.view.homeactivity.details.TaskDetailsFragment;
import com.example.task.view.homeactivity.TasksColors;
import com.example.task.view.recyclerview.SearchAdapter;
import com.example.task.view.recyclerview.customdialog.ColorItem;
import com.example.task.view.recyclerview.interfaces.OnCheckBoxClick;
import com.example.task.view.recyclerview.interfaces.OnTaskClick;
import com.example.task.viewmodel.GetListsViewModel;
import com.example.task.viewmodel.GetTasksViewModel;

import java.util.List;

import java8.util.stream.Collectors;
import java8.util.stream.StreamSupport;

public class SearchFragment extends Fragment implements ColorCheckboxDialogFragment.OnColorsSelected, OnCheckBoxClick,OnTaskClick{
    private View view;
    private AppCompatActivity activity;
    private List<ColorItem> colors;
    private List<ImageView> imageColors;
    private GetTasksViewModel getTasksViewModel;
    private GetListsViewModel getListsViewModel;
    private SearchAdapter searchAdapter;
    private CheckBox deleted;
    private CheckBox completed;
    private SearchView searchView;
    private SearchView.OnQueryTextListener queryTextListener;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        this.view = inflater.inflate(R.layout.fragment_search,container,false);
        this.activity = (AppCompatActivity) this.requireActivity();
        this.getTasksViewModel = new ViewModelProvider(this).get(GetTasksViewModel.class);
        this.getListsViewModel = new ViewModelProvider(this).get(GetListsViewModel.class);
        this.completed = this.view.findViewById(R.id.completedSearchCheckbox);
        this.deleted = this.view.findViewById(R.id.deletedSearchCheckbox);
        this.setupRecyclerView();
        UtilityClass.setupToolbar(this.activity,this.view,R.id.searchToolbar);
        setHasOptionsMenu(true);
        this.setupColors();
        return this.view;
    }

    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        inflater.inflate(R.menu.search_menu, menu);
        final MenuItem searchItem = menu.findItem(R.id.action_search);
        this.searchView = (SearchView) searchItem.getActionView();
        searchItem.expandActionView();
        searchItem.setOnActionExpandListener(this.exitListener());
        this.queryTextListener = this.searchListenerBuilder();
        this.searchView.setOnQueryTextListener(this.queryTextListener);
        this.searchView.setQueryHint("Search");
        this.setupCheckboxes();
    }

    private void setupCheckboxes() {
        final CheckBox.OnCheckedChangeListener checkboxListener = (buttonView, isChecked) -> this.triggerSearch();
        this.deleted.setOnCheckedChangeListener(checkboxListener);
        this.completed.setOnCheckedChangeListener(checkboxListener);
    }

    private List<ImageView> buildColorImages(){
        return StreamSupport.stream(this.colors).map(c->{
            final ImageView color = new ImageView(this.requireContext());
            color.setImageResource(R.drawable.black_circle);
            TasksColors.colorDrawable(color.getDrawable(),c.getColor());
            return color;
        }).collect(Collectors.toList());
    }

    @Override
    public void onColorsSelected(List<ColorItem> colors) {
        this.colors = colors;
        for(int i=0;i<this.colors.size();i++){
            this.imageColors.get(i).setVisibility(!this.colors.get(i).isChecked() ? View.GONE : View.VISIBLE);
        }
        this.triggerSearch();
    }

    private MenuItem.OnActionExpandListener exitListener(){
        return new MenuItem.OnActionExpandListener() {
            @Override
            public boolean onMenuItemActionExpand(MenuItem item) { return true; }
            @Override
            public boolean onMenuItemActionCollapse(MenuItem item) {
                activity.finish();
                return false; // OR FALSE IF YOU DIDN'T WANT IT TO CLOSE!
            }
        };
    }
    private SearchView.OnQueryTextListener searchListenerBuilder(){
        return new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) { return false; }

            @Override
            public boolean onQueryTextChange(String newText) {
                final List<Integer> selectedColors = StreamSupport.stream(colors)
                        .filter(ColorItem::isChecked)
                        .map(ColorItem::getColor)
                        .collect(Collectors.toList());
                getTasksViewModel.searchTasks(newText,selectedColors,completed.isChecked(),deleted.isChecked())
                        .observe(activity,tasks -> {
                            final List<Pair<Task,String>> search = StreamSupport.stream(tasks)
                                    .map(task -> new Pair<>(task,getListsViewModel.getListById(task.getListId()).getName()))
                                    .collect(Collectors.toList());
                            searchAdapter.setData(search);
                        });
                return true;
            }
        };
    }

    private void triggerSearch(){
        this.queryTextListener.onQueryTextChange(this.searchView.getQuery().toString());
    }

    private void setupRecyclerView(){
        final RecyclerView recyclerView = this.view.findViewById(R.id.searchResultRecyclerView);
        this.searchAdapter = new SearchAdapter(this,this);
        recyclerView.setAdapter(this.searchAdapter);
    }

    private void setupColors(){
        final TasksColors tasksColors = new TasksColors(this.requireContext());
        this.colors = StreamSupport.stream(tasksColors.colorsList()).map(c->new ColorItem(tasksColors.toColorName(c),c,true)).collect(Collectors.toList());
        final LinearLayout colorWrapper = this.view.findViewById(R.id.colorsLinearLayout);
        this.imageColors = this.buildColorImages();
        StreamSupport.stream(this.imageColors).forEach(colorWrapper::addView);
        colorWrapper.setOnClickListener(l->{
            new ColorCheckboxDialogFragment(this.colors,this).show(this.activity.getSupportFragmentManager(), UtilityTags.FRAGMENT_COLOR_CHECKBOX_DIALOG);
        });
    }


    @Override
    public void onCheckBoxClick(Task task, boolean isChecked, int position) {

    }

    @Override
    public void onTaskClick(Task task) {
        UtilityClass.replaceFragmentBundle(this.activity,R.id.fragmentContainer, new TaskDetailsFragment(),
                UtilityTags.FRAGMENT_TASK_DETAILS,task,UtilityTags.BUNDLE_TASK);
    }
}
