package com.example.task.view.creationactivity;

import android.animation.LayoutTransition;
import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.task.R;
import com.example.task.room.databaseutility.DatabaseWriter;
import com.example.task.room.entities.List;
import com.example.task.room.entities.Notification;
import com.example.task.room.entities.Task;
import com.example.task.sharedpreferences.SettingKey;
import com.example.task.sharedpreferences.UserSettings;
import com.example.task.view.UtilityClass;
import com.example.task.view.UtilityTags;
import com.example.task.view.customcomponents.customdialog.CalendarPickerDialogFragment;
import com.example.task.view.customcomponents.customdialog.ColorRadioDialogFragment;
import com.example.task.view.customcomponents.customdialog.ListDialogFragment;
import com.example.task.view.customcomponents.customdialog.TimePickerDialogFragment;
import com.example.task.view.customcomponents.customdialog.TimeSelectionDialogFragment;
import com.example.task.view.homeactivity.TasksColors;
import com.example.task.view.recyclerview.NotificationAdapter;

import org.threeten.bp.Duration;
import org.threeten.bp.Instant;
import org.threeten.bp.LocalDate;
import org.threeten.bp.LocalDateTime;
import org.threeten.bp.LocalTime;
import org.threeten.bp.ZoneId;
import org.threeten.bp.ZoneOffset;
import org.threeten.bp.format.DateTimeFormatter;
import org.threeten.bp.temporal.ChronoUnit;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Comparator;

import java8.util.stream.Collectors;
import java8.util.stream.StreamSupport;

import static android.app.Activity.RESULT_OK;

public abstract class AbstractCreateTask extends Fragment implements NotificationAdapter.OnNotificationClick, NotificationAdapter.OnDeleteNotificationClick {

    /*view variables*/
    private Button save;
    private ImageButton close;
    protected EditText taskName;
    protected EditText description;
    protected TextView listDialog;
    protected Switch timelessSwitch;
    protected TextView startDateTextView;
    protected TextView startTimeTextView;
    protected TextView endDateTextView;
    protected TextView endTimeTextView;
    protected TextView addRepetition;
    protected ImageView removeRepetition;
    protected RecyclerView recyclerView;
    protected NotificationAdapter notificationAdapter;
    protected TextView chooseColor;
    protected Drawable colorIcon;
    protected View separator;

    /*creation variables*/
    protected UserSettings userSettings;
    protected java.util.List<String> notifications = new ArrayList<>();
    protected java.util.List<Pair<Integer, ChronoUnit>> timeNotification = new ArrayList<>();
    protected Pair<Integer, ChronoUnit> repetition = null;
    protected int taskColor;
    protected int listId;
    protected Integer serverListId;//TODO: change for better implementation
    protected long startDate;
    protected long endDate;
    protected int startHour;
    protected int startMinute;
    protected int endHour;
    protected int endMinute;
    protected Task task = null;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_create_task, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        final FragmentActivity activity = getActivity();

        if (activity != null) {
            this.initViewComponents(activity);
            this.enableAnimation(activity);

            this.taskName.requestFocus();
            UtilityClass.openKeyboard(activity);

            this.close.setOnClickListener(l -> {
                UtilityClass.closeKeyboard(this.requireActivity());
                activity.finish();
            });

            this.save.setOnClickListener(l -> {
                if (checkTask()) {
                    this.writeTask(activity);
                }
            });

            this.listDialog.setOnClickListener(l -> {
                final DialogFragment listDialog = new ListDialogFragment((listId, serverListId, listName) -> {
                    this.listDialog.setText(listName);
                    this.listId = listId;
                    this.serverListId = serverListId;
                }, this.listId);
                listDialog.show(this.requireActivity().getSupportFragmentManager(), UtilityTags.FRAGMENT_LIST_DIALOG);
            });

            this.startDateTextView.setOnClickListener(l -> showCalendarDialog((dateSelected -> {
                this.startDate = dateSelected;
                this.startDateTextView.setText(this.fromLongToDateStringFormat(dateSelected));
            }), this.startDate));

            this.startTimeTextView.setOnClickListener(l -> showTimePickerDialog((hour, minutes) -> {
                this.startHour = hour;
                this.startMinute = minutes;
                this.startTimeTextView.setText(this.fromIntToTimeStringFormat(hour, minutes));
            }, this.startHour, this.startMinute));

            this.endDateTextView.setOnClickListener(l -> showCalendarDialog((dateSelected -> {
                this.endDate = dateSelected;
                this.endDateTextView.setText(this.fromLongToDateStringFormat(dateSelected));
            }), this.endDate));

            this.endTimeTextView.setOnClickListener(l -> showTimePickerDialog((hour, minutes) -> {
                this.endHour = hour;
                this.endMinute = minutes;
                this.endTimeTextView.setText(this.fromIntToTimeStringFormat(hour, minutes));
            }, this.endHour, this.endMinute));

            this.timelessSwitch.setOnCheckedChangeListener((buttonView, isChecked) -> {
                this.showAndHideTime();
            });

            this.chooseColor.setOnClickListener(l -> {
                final DialogFragment colorDialog = new ColorRadioDialogFragment(this.taskColor, (color, name) -> {
                    this.setColor(color);
                    this.chooseColor.setText(name);
                    this.taskColor = color;
                });
                colorDialog.show(this.requireActivity().getSupportFragmentManager(), UtilityTags.FRAGMENT_COLOR_RADIO_DIALOG);
            });

            this.addRepetition.setOnClickListener(l -> {
                final TimeSelectionDialogFragment.Mode timeMode = this.timelessSwitch.isChecked() ? TimeSelectionDialogFragment.Mode.REDUCED : TimeSelectionDialogFragment.Mode.FULL;
                final TimeSelectionDialogFragment dialogFragment = new TimeSelectionDialogFragment("Repeat every...", (quantity, timeUnit) -> {
                    final String summary = "Repeat every " + quantity + " " + timeUnit.toString().toLowerCase();
                    this.addRepetition.setText(summary);
                    this.repetition = new Pair<>(quantity, timeUnit);
                    this.removeRepetition.setVisibility(View.VISIBLE);
                }, () -> {
                }, timeMode);
                dialogFragment.show(getParentFragmentManager(), UtilityTags.FRAGMENT_TIME_DIALOG);
            });

            this.removeRepetition.setOnClickListener(l -> {
                this.removeRepetition.setVisibility(View.GONE);
                this.repetition = null;
                this.addRepetition.setText(getResources().getText(R.string.add_repetition));
            });
        }
    }

    protected void initViewComponents(FragmentActivity activity) {
        activity.setContentView(R.layout.fragment_create_task);
        this.save = activity.findViewById(R.id.saveButton);
        this.close = activity.findViewById(R.id.closeFormButton);
        this.taskName = activity.findViewById(R.id.titleEdit);
        this.description = activity.findViewById(R.id.addDescriptionView);
        this.listDialog = activity.findViewById(R.id.listView);
        this.timelessSwitch = activity.findViewById(R.id.timelessTaskSwitch);
        this.startDateTextView = activity.findViewById(R.id.taskStartingDateView);
        this.startTimeTextView = activity.findViewById(R.id.taskStartingTimeView);
        this.endDateTextView = activity.findViewById(R.id.taskEndingDateView);
        this.endTimeTextView = activity.findViewById(R.id.taskEndingTimeView);
        this.addRepetition = activity.findViewById(R.id.repeatTaskView);
        this.removeRepetition = activity.findViewById(R.id.deleteRepetition);
        this.removeRepetition.setVisibility(View.GONE);
        this.chooseColor = activity.findViewById(R.id.taskColorView);
        this.userSettings = new UserSettings(activity);
        this.colorIcon = getResources().getDrawable(R.drawable.black_circle);
        this.taskColor = userSettings.getInt(SettingKey.DEFAULT_COLOR);
        this.separator = activity.findViewById(R.id.view2);
        this.setRecyclerView(activity);

        this.startDate = LocalDate.now().atStartOfDay(ZoneId.systemDefault()).toInstant().toEpochMilli();
        this.startDateTextView.setText(this.fromLongToDateStringFormat(this.startDate));
        this.endDate = startDate;
        this.endDateTextView.setText(this.fromLongToDateStringFormat(this.endDate));
        this.startHour = LocalTime.now().getHour();
        this.startMinute = LocalTime.now().getMinute();
        this.startTimeTextView.setText(this.fromIntToTimeStringFormat(this.startHour, this.startMinute));
        this.endHour = LocalTime.now().plusMinutes(1).getHour();
        this.endMinute = LocalTime.now().plusMinutes(1).getMinute();
        this.endTimeTextView.setText(this.fromIntToTimeStringFormat(this.endHour, this.endMinute));

        final Bundle b = activity.getIntent().getExtras();
        if (b != null && b.getSerializable(UtilityTags.BUNDLE_LIST) != null) {
            final List list = (List) b.getSerializable(UtilityTags.BUNDLE_LIST);
            assert list != null;
            this.listId = list.getListId();
            this.listDialog.setText(list.getName());
            this.serverListId = list.getServerId();
        } else if (this.userSettings.getInt(SettingKey.DEFAULT_LIST_VALUE) != -1) {
            this.listId = this.userSettings.getInt(SettingKey.DEFAULT_LIST_VALUE);
            this.serverListId = this.userSettings.getInt(SettingKey.DEFAULT_SERVER_LIST);
            this.listDialog.setText(getResources().getText(R.string.default_list));
        } else {
            this.listId = -1;
            this.listDialog.setText(getResources().getText(R.string.select_list));
        }
    }

    abstract void writeTask(@NonNull final Activity activity);

    private void setRecyclerView(FragmentActivity activity) {
        this.recyclerView = activity.findViewById(R.id.addNotificationView);
        this.recyclerView.setNestedScrollingEnabled(false);
        this.recyclerView.setLayoutManager(new LinearLayoutManager(activity));

        this.notificationAdapter = new NotificationAdapter(this, this);
        this.recyclerView.setAdapter(notificationAdapter);
        this.notifications.add("Add notification");
        this.notificationAdapter.setData(this.notifications);
        this.recyclerView.setClickable(true);
    }

    private boolean checkTask() {
        if (this.taskName.getText().toString().isEmpty()) {
            UtilityClass.showToast(requireContext(), "Please insert a title to your task!");
            return false;
        }
        if (this.listId == -1) {
            UtilityClass.showToast(requireContext(), "Please choose a list!");
            return false;
        }
        if (!this.timelessSwitch.isChecked()) {
            if (this.startDate > this.endDate) {
                UtilityClass.showToast(requireContext(), "The ending date must be equal or later than starting date!");
                return false;
            }
            if (this.endDate == this.startDate) {
                if (this.startHour == this.endHour && this.endMinute <= this.startMinute) {
                    UtilityClass.showToast(requireContext(), "The ending time must be later than starting time");
                    return false;
                }
                if (this.endHour < this.startHour) {
                    UtilityClass.showToast(requireContext(), "The ending time must be later than starting time");
                    return false;
                }
            }
        }
        return true;
    }

    protected void setColor(int color) {
        this.chooseColor.setCompoundDrawablesWithIntrinsicBounds(TasksColors.colorDrawable(this.colorIcon, color), null, null, null);
    }

    private void showCalendarDialog(final @NonNull CalendarPickerDialogFragment.OnDateSelected onDateSelectedListener, long date) {
        final DialogFragment calendarDialog = new CalendarPickerDialogFragment(onDateSelectedListener, date);
        calendarDialog.show(this.requireActivity().getSupportFragmentManager(), UtilityTags.FRAGMENT_DATE_PICKER_DIALOG);
    }

    private void showTimePickerDialog(final @NonNull TimePickerDialogFragment.OnTimeSelected onTimeSelectedListener, final int hour, final int minute) {
        final DialogFragment timeDialog = new TimePickerDialogFragment(onTimeSelectedListener, hour, minute);
        timeDialog.show(this.requireActivity().getSupportFragmentManager(), UtilityTags.FRAGMENT_TIME_PICKER_DIALOG);
    }

    private void toggleVisibility(View view) {
        view.setVisibility(view.getVisibility() == View.VISIBLE ? View.GONE : View.VISIBLE);
    }

    private void enableAnimation(FragmentActivity activity) {
        ViewGroup taskSettings = activity.findViewById(R.id.taskSettings);
        taskSettings.getLayoutTransition().enableTransitionType(LayoutTransition.CHANGING);
    }

    @Override
    public void onNotificationClick(int position) {
        if (this.recyclerView.isClickable()) {
            final TimeSelectionDialogFragment dialogFragment = new TimeSelectionDialogFragment("Notify me...", (quantity, timeUnit) -> {
                final String summary = quantity + " " + timeUnit.toString().toLowerCase() + " before";
                if (position != this.notifications.size() - 1) {
                    this.notifications.set(position, summary);
                    this.timeNotification.set(position, new Pair<>(quantity, timeUnit));
                    this.notificationAdapter.setData(this.notifications);
                } else {
                    this.notifications.add(this.notifications.size() - 1, summary);
                    this.timeNotification.add(new Pair<>(quantity, timeUnit));
                    this.notificationAdapter.setData(this.notifications);
                }
            }, () -> {
            }, TimeSelectionDialogFragment.Mode.FULL);
            dialogFragment.show(getParentFragmentManager(), UtilityTags.FRAGMENT_TIME_DIALOG);
        }
    }

    @Override
    public void onDeleteNotificationClick(int position) {
        this.timeNotification.remove(position);
        this.notifications.remove(position);
        this.notificationAdapter.setData(this.notifications);
    }

    protected void setTimeAndRepetition() {
        if (!this.timelessSwitch.isChecked()) {
            final LocalDate eDate = LocalDateTime.ofInstant(Instant.ofEpochMilli(this.endDate), ZoneId.systemDefault()).toLocalDate();
            final LocalTime eTime = LocalTime.of(this.endHour, this.endMinute);
            this.task.setEnd(LocalDateTime.of(eDate, eTime));
            final LocalDate sDate = LocalDateTime.ofInstant(Instant.ofEpochMilli(this.startDate), ZoneId.systemDefault()).toLocalDate();
            final LocalTime sTime = LocalTime.of(this.startHour, this.startMinute);
            this.task.setStart(LocalDateTime.of(sDate, sTime));
        } else {
            final LocalDate eDate = LocalDateTime.ofInstant(Instant.ofEpochMilli(this.endDate), ZoneId.systemDefault()).toLocalDate();
            this.task.setEnd(LocalDateTime.of(eDate, LocalTime.of(23, 59)));
            this.task.setStart(null);
        }
        if (this.repetition != null) {
            this.task.setRepeat(this.repetition.first);
            this.task.setRepeatUnit(this.repetition.second.toString().toUpperCase());
        } else {
            this.task.setRepeat(null);
            this.task.setRepeatUnit(null);
        }
    }

    protected java.util.List<Notification> getNotifications(@NonNull final Task task) {
        final LocalDateTime time = task.getStart();
        return StreamSupport.stream(this.timeNotification)
                .map(t -> {
                    final Notification notification = new Notification(time.minus(Duration.of(t.first, t.second)), -1);
                    notification.setUnit(t.second.toString().toUpperCase());
                    return notification;
                })
                .sorted((a,b)-> a.getTime().getMinute() - b.getTime().getMinute())
                .collect(Collectors.toList());
    }

    protected String fromLongToDateStringFormat(final long date) {
        return Instant.ofEpochMilli(date)
                .atZone(ZoneId.systemDefault())
                .format(DateTimeFormatter.ofPattern("EEEE, d MMM uuu"));
    }

    protected String fromIntToTimeStringFormat(final int hour, final int minute) {
        String str = hour < 10 ? ("0" + hour) : String.valueOf(hour);
        str += minute < 10 ? ":0" + minute : ":" + minute;
        return str;
    }

    protected String fromLocalDateTimeToStringFormat(@NonNull final LocalDateTime time) {
        return time.format(DateTimeFormatter.ofPattern("EEEE, d MMM uuu"));
    }

    protected long fromLocalDateToLong(@NonNull final LocalDateTime time) {
        return time.truncatedTo(ChronoUnit.DAYS).toInstant(ZoneOffset.UTC).toEpochMilli();
    }

    protected void showAndHideTime() {
        toggleVisibility(this.startDateTextView);
        toggleVisibility(this.startTimeTextView);
        toggleVisibility(this.endTimeTextView);
        toggleVisibility(this.separator);
        toggleVisibility(this.recyclerView);
    }
}
