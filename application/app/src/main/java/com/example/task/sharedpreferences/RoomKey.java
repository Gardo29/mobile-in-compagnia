package com.example.task.sharedpreferences;

import androidx.annotation.NonNull;

public enum RoomKey {
    TASKS_TS("tasks_ts"),
    LISTS_TS("lists_ts"),
    NOTIFICATIONS_TS("notifications_ts");

    private final String string;

    private RoomKey(@NonNull final String string){ this.string = string; }

    @NonNull
    @Override
    public String toString() { return this.string; }
}
