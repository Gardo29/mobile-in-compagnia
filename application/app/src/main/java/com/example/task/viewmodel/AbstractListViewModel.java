package com.example.task.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;

import com.example.task.room.repositories.ListRepository;

public abstract class AbstractListViewModel extends AndroidViewModel {

    private final ListRepository listRepository;

    AbstractListViewModel(@NonNull final Application application) {
        super(application);
        this.listRepository = new ListRepository(application);
    }
    protected ListRepository getListRepository(){ return this.listRepository; }
}
