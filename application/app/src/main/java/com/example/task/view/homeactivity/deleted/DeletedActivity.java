package com.example.task.view.homeactivity.deleted;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.task.R;
import com.example.task.view.UtilityClass;
import com.example.task.view.UtilityTags;

public class DeletedActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.activity_for_fragments);
        UtilityClass.replaceFragmentNoBack(this,R.id.fragmentContainer,new DeletedFragment(), UtilityTags.FRAGMENT_DELETED);
    }
}
