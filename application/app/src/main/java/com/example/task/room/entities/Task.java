package com.example.task.room.entities;

import android.graphics.Color;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.Ignore;
import androidx.room.Index;
import androidx.room.PrimaryKey;
import androidx.work.PeriodicWorkRequest;


import com.example.task.repetition.RepetitionUtility;
import com.example.task.room.FieldsUtility;

import org.threeten.bp.Duration;
import org.threeten.bp.LocalDateTime;
import org.threeten.bp.temporal.ChronoUnit;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.util.DoubleSummaryStatistics;

import static androidx.room.ForeignKey.CASCADE;

@Entity(tableName = "tasks",
        foreignKeys = {@ForeignKey(onDelete = CASCADE,entity = List.class,parentColumns = "list_id",childColumns = "list_id")},
        indices = {@Index("list_id")})
public class Task implements Serializable,Comparable{
    @Ignore
    public final static int EXPIRED_TASK_COLOR = Color.RED;
    @Ignore
    private final static int DAYS_FOR_TASK_COMPLETION = 1;
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "task_id")
    private int taskId;
    /* foreign key for list */
    @ColumnInfo(name = "list_id")
    private int listId;
    @ColumnInfo(name = "server_id")
    private Integer serverId;
    @ColumnInfo(name = "server_list_id")
    private Integer serverListId;
    @ColumnInfo(name = "creation_date")
    private LocalDateTime creationDate;
    private String title;
    private int color;
    @ColumnInfo(name = "start_date")
    private LocalDateTime start;
    @ColumnInfo(name = "end_date")
    private LocalDateTime end;
    @ColumnInfo(name = "repeat_time")
    private Integer repeat;
    @ColumnInfo(name = "repeat_unit")
    private String repeatUnit;

    @ColumnInfo(name = "deletion_time")
    private LocalDateTime deletionTime;
    @ColumnInfo(name = "completion_time")
    private LocalDateTime completionTime;
    @ColumnInfo(name = "permanently_deleted")
    private boolean permanentlyDelete;
    private String description;
    private boolean dirty;
    private int ts;

    public Task(@NonNull final String title,final int color) {
        this.title = title;
        this.color = color;
    }

    public String getRepeatUnit() { return this.repeatUnit; }

    public Integer getServerListId() { return this.serverListId; }

    public Integer getServerId() { return this.serverId; }

    public int getTaskId() { return this.taskId; }

    public int getListId() { return this.listId; }

    public LocalDateTime getCreationDate() { return this.creationDate; }

    public String getTitle() { return this.title; }

    public int getColor() { return this.color; }

    public boolean isDirty() { return this.dirty; }

    public LocalDateTime getStart() { return this.start; }

    public LocalDateTime getEnd() { return this.end; }

    public Integer getRepeat() { return this.repeat; }

    public LocalDateTime getDeletionTime () { return this.deletionTime; }

    public LocalDateTime getCompletionTime() { return this.completionTime; }

    public boolean isPermanentlyDelete() { return this.permanentlyDelete; }

    public String getDescription() { return this.description; }

    public int getTs() { return ts; }

    public void setRepeatUnit(String repeatUnit) { this.repeatUnit = repeatUnit; }

    public void setListId(int listId) { this.listId = listId; }

    public void setServerListId(final Integer serverListId) { this.serverListId = serverListId; }

    public void setDirty(final boolean dirty) { this.dirty = dirty; }

    public void setCreationDate(final LocalDateTime creationDate) { this.creationDate = creationDate; }

    public void setTaskId(final int taskId){ this.taskId = taskId; }

    public void setPermanentlyDelete(final boolean permanentlyDelete) { this.permanentlyDelete = permanentlyDelete; }

    public void setTs(final int ts) { this.ts = ts; }

    public void setTitle(@NonNull final String title) {
        this.title = FieldsUtility.checkField(title,!title.isEmpty());
    }

    public void setServerId(final Integer serverId) { this.serverId = serverId; }

    public void setColor(final int color) {
        this.color = FieldsUtility.checkField(color,Color.BLACK <= color && color <= Color.WHITE);
    }
    public void setStart(final LocalDateTime start){ this.start = start; }

    public void setEnd(final LocalDateTime end){ this.end = end; }

    public void setCompleteTime(@NonNull final LocalDateTime start,@NonNull final LocalDateTime end) {
        this.end = FieldsUtility.checkField(end,end.isAfter(start));
    }

    public boolean isExpired(){
        return this.end != null && this.end.isBefore(LocalDateTime.now()) && this.completionTime == null;
    }
    public void setDailyTask(@NonNull final LocalDateTime start){
        this.end = start.plusDays(DAYS_FOR_TASK_COMPLETION).truncatedTo(ChronoUnit.DAYS);
    }

    public void setRepeat(final Integer repeat) { this.repeat = repeat; }

    public void setDeletionTime(final LocalDateTime deleted) { this.deletionTime = deleted; }

    public void setCompletionTime(final LocalDateTime completed) { this.completionTime = completed; }

    public void setDescription(final String description) { this.description = description; }

    public void repeat(){
        if(this.repeat != null && this.repeatUnit != null){
            this.completionTime = null;
            this.dirty = true;
            this.setEnd(this.getEnd().plus(Duration.of(this.getRepeat(),ChronoUnit.valueOf(this.getRepeatUnit()))));
            if(this.getStart() != null){
                this.setStart(this.getStart().plus(Duration.of(this.getRepeat(),ChronoUnit.valueOf(this.getRepeatUnit()))));
            }
        }
    }

    @Override
    public int compareTo(@NonNull final Object o) {
        final Task b = (Task) o;
        final LocalDateTime startA = RepetitionUtility.startTime(this);
        final LocalDateTime startB = RepetitionUtility.startTime(b);

        if (this.isExpired() && b.isExpired()) {
            return startA.compareTo(startB);
        }
        if (this.isExpired()) {
            return -1;
        }
        if (this.getStart() == null) {
            if (b.isExpired()) {
                return 1;
            } else if (b.getStart() == null) {
                return startA.compareTo(startB);
            } else {
                return -1;
            }
        }
        if (this.getStart() != null && (b.isExpired() || b.start == null)) {
            return 1;
        }
        return this.getStart().compareTo(b.getStart());
    }
}
