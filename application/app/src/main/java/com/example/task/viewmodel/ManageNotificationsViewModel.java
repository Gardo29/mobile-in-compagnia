package com.example.task.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;

import com.example.task.room.entities.Notification;

public class ManageNotificationsViewModel extends AbstractNotificationViewModel {

    public ManageNotificationsViewModel(@NonNull Application application) {
        super(application);
    }
    public void clearDeletedNotifications(){ this.getNotificationRepository().clearDeletedNotifications(); }
    public void removeNotification(@NonNull final Notification notification){ this.getNotificationRepository().removeNotification(notification); }
    public void insertNotification(@NonNull final Notification notification){ this.getNotificationRepository().addNotification(notification); }
    public void updateNotification(@NonNull final Notification notification){ this.getNotificationRepository().updateNotification(notification); }
}
