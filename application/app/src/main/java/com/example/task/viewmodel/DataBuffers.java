package com.example.task.viewmodel;

import android.icu.text.TimeZoneFormat;

import androidx.annotation.NonNull;
import androidx.core.util.Pair;

import com.example.task.room.entities.List;
import com.example.task.room.entities.Notification;
import com.example.task.room.entities.Task;
import com.example.task.room.repositories.ListRepository;
import com.google.common.hash.Hashing;

import org.threeten.bp.LocalDateTime;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class DataBuffers {
    private static final DataBuffers instance = new DataBuffers();
    private final Map<Integer, List> roomLists;
    private final Map<Integer, List> serverLists;
    private final Map<Integer, Task> roomTasks;
    private final Map<Integer, Task> serverTasks;
    private final Map<Pair<Integer, LocalDateTime>,Notification> roomNotifications;
    private final Map<Pair<Integer, LocalDateTime>,Notification> serverNotifications;

    private DataBuffers(){
        this.roomLists = new HashMap<>();
        this.serverLists = new HashMap<>();
        this.roomTasks = new HashMap<>();
        this.serverTasks = new HashMap<>();
        this.roomNotifications = new HashMap<>();
        this.serverNotifications = new HashMap<>();
    }

    public static DataBuffers getInstance() { return  instance ;}

    /* setters */
    public void putServerTask(int serverId, @NonNull final Task task){
        this.serverTasks.put(serverId,task);
    }
    public void putServerList(int serverId, @NonNull final List list){ this.serverLists.put(serverId,list); }
    public void putServerNotification(int serverId,LocalDateTime localDateTime, @NonNull final Notification notification){
        final Pair<Integer,LocalDateTime> key = new Pair<>(serverId,localDateTime);
        this.serverNotifications.put(key,notification);
    }
    public void putRoomTask(int roomId, @NonNull final Task task){ this.roomTasks.put(roomId,task); }
    public void putRoomList(int roomId, @NonNull final List list){ this.roomLists.put(roomId,list); }
    public void putRoomNotification(int roomId,LocalDateTime localDateTime, @NonNull final Notification notification){
        final Pair<Integer,LocalDateTime> key = new Pair<>(roomId,localDateTime);
        this.roomNotifications.put(key,notification);
    }
    /* getters */
    public Task getServerTask(int serverId){ return this.serverTasks.get(serverId); }
    public List getServerList(int serverId){ return this.serverLists.get(serverId); }
    public Notification getServerNotification(int serverId,LocalDateTime localDateTime){
        final Pair<Integer,LocalDateTime> key = new Pair<>(serverId,localDateTime);
        return this.serverNotifications.get(key);
    }
    public Task getRoomTask(int roomId){ return this.roomTasks.get(roomId); }
    public List getRoomList(int roomId){ return this.roomLists.get(roomId); }
    public Notification getRoomNotification(int roomId,LocalDateTime localDateTime){
        final Pair<Integer,LocalDateTime> key = new Pair<>(roomId,localDateTime);
        return this.roomNotifications.get(key);
    }
}
