package com.example.task.view.recyclerview.interfaces;

import com.example.task.room.entities.Task;

public interface OnCheckBoxClick {
    public void onCheckBoxClick(Task task,boolean isChecked,int position);
}
