package com.example.task.view.homeactivity.login;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.task.R;
import com.example.task.view.UtilityClass;
import com.example.task.view.UtilityTags;


public class LoginActivity extends AppCompatActivity {

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_for_fragments);

        UtilityClass.replaceFragmentNoBack(this,R.id.fragmentContainer,new LoadingFragment(), UtilityTags.FRAGMENT_LOADING_TAG);
    }
}
