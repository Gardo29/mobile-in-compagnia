package com.example.task.view.creationactivity;

import android.app.Activity;
import android.content.Intent;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentActivity;

import com.example.task.room.databaseutility.DatabaseWriter;
import com.example.task.room.entities.Task;
import com.example.task.sharedpreferences.SettingKey;
import com.example.task.view.UtilityClass;
import com.example.task.view.UtilityTags;
import org.threeten.bp.LocalDateTime;
import java.io.Serializable;
import static android.app.Activity.RESULT_OK;

public class CreateTaskFragment extends AbstractCreateTask {

    @Override
    protected void initViewComponents(FragmentActivity activity) {
        super.initViewComponents(activity);
        super.setColor(userSettings.getInt(SettingKey.DEFAULT_COLOR));
        super.taskColor = userSettings.getInt(SettingKey.DEFAULT_COLOR);
        this.timelessSwitch.setChecked(true);
        this.showAndHideTime();
    }

    @Override
    void writeTask(@NonNull Activity activity) {
        final DatabaseWriter databaseWriter = new DatabaseWriter(activity);
        super.task = new Task(super.taskName.getText().toString(), super.taskColor);
        super.task.setListId(super.listId);
        super.task.setCreationDate(LocalDateTime.now());
        super.task.setCompletionTime(null);
        String description = super.description.getText().toString();
        super.task.setDescription(description.isEmpty() ? null : description);
        super.setTimeAndRepetition();
        databaseWriter.insertTask(super.task);

        final Intent intent = new Intent();
        intent.putExtra(UtilityTags.BUNDLE_NOTIFICATION, (Serializable) getNotifications(task));
        activity.setResult(RESULT_OK, intent);
        activity.finish();
    }
}
