package com.example.task.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;

import com.example.task.room.entities.List;

public class ManageListsViewModel extends AbstractListViewModel{
    public ManageListsViewModel(@NonNull final Application application) {
        super(application);
    }

    public void updateList(@NonNull final List list){ this.getListRepository().updateList(list); }
    public void removeList(@NonNull final List list){ this.getListRepository().removeList(list); }
    public void insertList(@NonNull final List list){ this.getListRepository().addList(list); }
    public void deleteAllLists() { this.getListRepository().deleteAllList(); }
}
