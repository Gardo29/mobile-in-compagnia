package com.example.task.room.relations;

import androidx.room.Embedded;
import androidx.room.Relation;


import com.example.task.room.entities.Notification;
import com.example.task.room.entities.Task;

import java.util.List;

public class Reminder {
    @Embedded
    private Task task;

    @Relation(parentColumn = "task_id",entityColumn = "task_id")
    private List<Notification> notifications;

    public List<Notification> getNotifications() {
        return this.notifications;
    }

    public Task getTask() {
        return this.task;
    }

    public void setNotifications(final List<Notification> notifications) { this.notifications = notifications;
    }

    public void setTask(final Task task) { this.task = task; }
}
