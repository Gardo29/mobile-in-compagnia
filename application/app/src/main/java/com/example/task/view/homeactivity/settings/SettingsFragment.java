package com.example.task.view.homeactivity.settings;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.drawable.Drawable;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.graphics.drawable.DrawableCompat;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.ViewModelProvider;
import androidx.preference.ListPreference;
import androidx.preference.Preference;
import androidx.preference.PreferenceFragmentCompat;
import androidx.preference.SwitchPreference;
import androidx.preference.SwitchPreferenceCompat;

import com.example.task.R;
import com.example.task.room.entities.Task;
import com.example.task.sharedpreferences.SettingKey;
import com.example.task.sharedpreferences.UserSettings;
import com.example.task.view.UtilityTags;
import com.example.task.view.customcomponents.customdialog.TimeSelectionDialogFragment.Mode;
import com.example.task.view.customcomponents.customdialog.ColorCheckboxDialogFragment;
import com.example.task.view.customcomponents.customdialog.ColorRadioDialogFragment;
import com.example.task.view.customcomponents.customdialog.ListDialogFragment;
import com.example.task.view.customcomponents.customdialog.TimeSelectionDialogFragment;
import com.example.task.view.homeactivity.TasksColors;
import com.example.task.view.recyclerview.customdialog.ColorItem;
import com.example.task.viewmodel.GetListsViewModel;
import com.example.task.viewmodel.GetTasksViewModel;
import com.example.task.viewmodel.ManageListsViewModel;
import com.example.task.viewmodel.ManageTasksViewModel;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;

import java.util.List;
import java.util.Objects;

import java8.util.stream.Collectors;
import java8.util.stream.IntStream;
import java8.util.stream.IntStreams;
import java8.util.stream.StreamSupport;

public class SettingsFragment extends PreferenceFragmentCompat {
    
    private UserSettings userSettings;
    private Context context;
    private FragmentManager fragmentManager;

    @Override
    public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
        setPreferencesFromResource(R.xml.settings_layout,rootKey);
        final FragmentActivity activity = requireActivity();
        this.context = requireContext();
        this.fragmentManager = activity.getSupportFragmentManager();
        final ManageTasksViewModel taskViewModel = new ViewModelProvider(activity).get(ManageTasksViewModel.class);
        final ManageListsViewModel listViewModel = new ViewModelProvider(activity).get(ManageListsViewModel.class);
        this.userSettings = new UserSettings(context);
        final TasksColors tasksColors = new TasksColors(context);

        final SwitchPreferenceCompat defaultList = (SwitchPreferenceCompat) this.preference(SettingKey.DEFAULT_LIST.toString());
        if(this.userSettings.getBoolean(SettingKey.DEFAULT_LIST)){
            defaultList.setSummary(this.userSettings.getString(SettingKey.DEFAULT_LIST_SUMMARY));
        }
        int bo = this.userSettings.getInt(SettingKey.DEFAULT_LIST_VALUE);
        defaultList.setOnPreferenceClickListener(l->{
            if(defaultList.isChecked()){
                defaultList.setChecked(!defaultList.isChecked());
                final GetListsViewModel tasksViewModel = new ViewModelProvider(this).get(GetListsViewModel.class);
                tasksViewModel.getLists().observe(this,lists -> {
                    if(lists.size() > 0){
                        new ListDialogFragment((listId,listServerId,listName) -> {
                            this.userSettings.putString(SettingKey.DEFAULT_LIST_SUMMARY,listName);
                            this.userSettings.putInt(SettingKey.DEFAULT_LIST_VALUE,listId);
                            this.userSettings.putInt(SettingKey.DEFAULT_SERVER_LIST, listServerId);
                            defaultList.setSummary(listName);
                            defaultList.setChecked(true);
                        },this.userSettings.getInt(SettingKey.DEFAULT_LIST_VALUE))
                                .show(this.fragmentManager,UtilityTags.FRAGMENT_LIST_DIALOG);
                    }else{
                        Toast.makeText(this.context,"No lists are present yet, create one before",Toast.LENGTH_SHORT).show();
                    }

                });
            }else{
                this.userSettings.unset(SettingKey.DEFAULT_LIST_VALUE,SettingKey.DEFAULT_SERVER_LIST);
                defaultList.setSummary("");
                defaultList.setChecked(false);
            }

            return true;
        });

        /* default color */
        final Preference defaultColorPreference = this.preference(SettingKey.DEFAULT_COLOR.toString());
        int defaultColor = this.userSettings.getInt(SettingKey.DEFAULT_COLOR);
        TasksColors.colorDrawable(defaultColorPreference.getIcon(),defaultColor);
        defaultColorPreference.setSummary(this.convertColor(this.userSettings.getInt(SettingKey.DEFAULT_COLOR)));
        defaultColorPreference.setOnPreferenceClickListener(l->{
            new ColorRadioDialogFragment(this.userSettings.getInt(SettingKey.DEFAULT_COLOR),(color, name) -> {
               this.userSettings.putInt(SettingKey.DEFAULT_COLOR,color);
               defaultColorPreference.setSummary(tasksColors.toColorName(color));
               TasksColors.colorDrawable(defaultColorPreference.getIcon(),this.userSettings.getInt(SettingKey.DEFAULT_COLOR));
            }).show(this.fragmentManager, UtilityTags.FRAGMENT_COLOR_RADIO_DIALOG);
            return true;
        });

        /* delete all completed tasks */
        final Preference deleteAllLists = Objects.requireNonNull(findPreference("delete_all_lists"));
        deleteAllLists.setOnPreferenceClickListener(l->{
            this.makeDialog("Delete all completed tasks?","Are you sure to delete all completed tasks?",((dialog, which) -> {
                listViewModel.deleteAllLists();
            }));
            return true;
        });

        /* delete all completed tasks */
        final Preference deleteAllCompletedTasks = Objects.requireNonNull(findPreference("delete_all_completed_tasks"));
        deleteAllCompletedTasks.setOnPreferenceClickListener(l->{
            this.makeDialog("Delete all completed tasks?","Are you sure to delete all completed tasks?",((dialog, which) -> {
                taskViewModel.deleteAllCompletedTasks();
            }));
            return true;
        });

        /* delete all tasks*/
        final Preference deleteAllTasks = Objects.requireNonNull(findPreference("delete_all_tasks"));
        deleteAllTasks.setOnPreferenceClickListener(l->{
            this.makeDialog("Delete all tasks?","Are you sure to delete all tasks?",((dialog, which) -> {
                taskViewModel.deleteAllTasks();
            }));
            return true;
        });

        /* app version */
        final Preference appVersion = this.preference(SettingKey.APP_VERSION.toString());
        appVersion.setTitle(appVersion.getTitle() +this.userSettings.getString(SettingKey.APP_VERSION));
        appVersion.setOnPreferenceClickListener(l-> {
            Toast.makeText(this.context,"ZA WARUDO!",Toast.LENGTH_LONG).show();
            final MediaPlayer media = MediaPlayer.create(this.context,R.raw.za_warudo);
            media.start();
            return true;
        });

    }

    private void makeDialog(@NonNull final String title, @NonNull final String message,@NonNull final DialogInterface.OnClickListener yesListener){
        new MaterialAlertDialogBuilder(requireContext())
                .setTitle(title)
                .setMessage(message)
                .setNegativeButton("Cancel",(dialog,witch)->{})
                .setPositiveButton("Yes",yesListener)
                .show();
    }
    @NonNull
    private Preference preference(@NonNull final String name){
        return Objects.requireNonNull(this.findPreference(name));
    }
    private String convertColor(final int color){
        return new TasksColors(this.requireContext()).toColorName(color);
    }
    private Preference.OnPreferenceClickListener getTimeSelectionListener(@NonNull final SwitchPreferenceCompat preference,@NonNull final String title,
                                                                          @NonNull final SettingKey valueKey, @NonNull final SettingKey summaryKey){
        return l->{
            if(preference.isChecked()){
                preference.setChecked(false);
                new TimeSelectionDialogFragment(title,(quantity, timeUnit) -> {
                    final String summary = quantity +" "+ timeUnit.toString();
                    this.userSettings.putString(summaryKey,summary);
                    preference.setSummary(summary);
                    this.userSettings.putLong(valueKey,timeUnit.getDuration().toMillis()*quantity);
                    preference.setChecked(true);
                },()-> preference.setChecked(false), Mode.FULL)
                        .show(this.fragmentManager,UtilityTags.FRAGMENT_TIME_DIALOG);
            }else{
                preference.setSummary("");
                this.userSettings.unset(valueKey);
                this.userSettings.unset(summaryKey);
            }
            return true;

        };
    }
}
