package com.example.task.view.recyclerview.customdialog;

import androidx.annotation.NonNull;

import java.util.stream.StreamSupport;

public class ColorItem {
    private final int color;
    private final String name;
    private boolean isChecked;

    public ColorItem(@NonNull final String name,int color) {
        this(name,color,false);
    }

    public ColorItem(@NonNull final String name,int color,final boolean isChecked) {
        this.isChecked = isChecked;
        this.color = color;
        this.name = name;
    }

    public int getColor() { return this.color; }

    public String getName() { return this.name; }

    public boolean isChecked() { return this.isChecked; }

    public void setChecked(boolean checked) { this.isChecked = checked; }
}
