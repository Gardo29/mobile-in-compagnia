package com.example.task.view.recyclerview.customdialog;


import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.task.R;
import com.example.task.room.entities.List;

import java.util.ArrayList;

public class ListDialogAdapter extends RecyclerView.Adapter<ListDialogViewHolder> {
    private final java.util.List<List> lists = new ArrayList<>();
    private final OnListClickListener listener;
    private int selectedPosition;

    public ListDialogAdapter(@NonNull OnListClickListener listener) {
        this.listener = listener;
    }

    @NonNull
    @Override
    public ListDialogViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        final View holder = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_radio_button,parent,false);
        return new ListDialogViewHolder(holder, position -> listener.onItemClick(this.lists.get(position)));
    }

    @Override
    public void onBindViewHolder(@NonNull ListDialogViewHolder holder, int position) {
        holder.radioButton.setText(this.lists.get(position).getName());
        if(this.selectedPosition != -1 && this.selectedPosition == position){
            holder.radioButton.setOnCheckedChangeListener(null);
        }
        holder.radioButton.setChecked(position == this.selectedPosition);
    }

    public void setData(java.util.List<List> data, int selectedPosition) {
        this.lists.clear();
        this.lists.addAll(data);
        this.selectedPosition = selectedPosition;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return this.lists.size();
    }

    public interface OnListClickListener { void onItemClick(List list); }
}
