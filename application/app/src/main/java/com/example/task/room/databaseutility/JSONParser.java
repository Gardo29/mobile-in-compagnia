package com.example.task.room.databaseutility;

import androidx.annotation.NonNull;

import com.example.task.room.entities.Converters;
import com.example.task.room.entities.List;
import com.example.task.room.entities.Notification;
import com.example.task.room.entities.Task;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.threeten.bp.Duration;
import org.threeten.bp.LocalDateTime;

import java.util.ArrayList;


public class JSONParser {
    /*    dirty    */
    private static final boolean CLEAN = false;
    private static final String LISTS = "lists";
    private static final String TASKS = "tasks";
    private static final String NOTIFICATIONS = "notifications";


    private final java.util.List<List> lists;
    private final java.util.List<Task> tasks;
    private final java.util.List<Notification> notifications;
    private final JSONArray data;

    public JSONParser(@NonNull final JSONArray data) throws JSONException {
        this.data = data;
        this.lists = new ArrayList<>();
        this.notifications = new ArrayList<>();
        this.tasks = new ArrayList<>();
        this.loadData();
    }

    private void loadData() throws JSONException {
        final JSONObject realData = this.data.getJSONObject(0);
        final JSONArray lists = realData.getJSONArray(LISTS);
        final JSONArray tasks = realData.getJSONArray(TASKS);
        final JSONArray notifications = realData.getJSONArray(NOTIFICATIONS);

        for(int l = 0; l < lists.length(); l++) {
            final JSONObject jsonList = lists.getJSONObject(l);
            final List roomList = parseList(jsonList);
            this.lists.add(roomList);
        }

        for(int t = 0; t < tasks.length(); t++) {
            final JSONObject jsonTask = tasks.getJSONObject(t);
            final Task roomTask = parseTask(jsonTask);
            this.tasks.add(roomTask);
        }

        for(int n = 0; n < notifications.length(); n++){
            final JSONObject  jsonNotification = notifications.getJSONObject(n);
            final Notification roomNotification = parseNotification(jsonNotification);
            this.notifications.add(roomNotification);
        }
    }

    public java.util.List<Notification> getNotifications() { return this.notifications; }

    public java.util.List<List> getLists() { return this.lists; }

    public java.util.List<Task> getTasks() { return this.tasks; }

    public static List parseList(@NonNull final JSONObject jsonList) throws JSONException{
        List roomList = new List(jsonList.getString(EntityFields.LIST_NAME.toString()));
        roomList.setDirty(CLEAN);
        roomList.setServerId(jsonList.getInt(EntityFields.LIST_ID.toString()));
        roomList.setDescription(nullableString(jsonList,EntityFields.LIST_DESCRIPTION.toString()));
        roomList.setCreationDate(dateConverter(jsonList.getString(EntityFields.LIST_CREATION_DATE.toString())));
        roomList.setTs(jsonList.getInt(EntityFields.TS.toString()));
        roomList.setDisplayHome(booleanConverter(jsonList.getInt(EntityFields.LIST_DISPLAY_HOME.toString())));
        roomList.setEnabledNotifications(booleanConverter(jsonList.getInt(EntityFields.LIST_ENABLED_NOTIFICATIONS.toString())));
        roomList.setDeleted(booleanConverter(jsonList.getInt(EntityFields.LIST_DELETED.toString())));
        return roomList;
    }
    public static Task parseTask(@NonNull final JSONObject jsonTask) throws JSONException{
        Task roomTask = new Task(jsonTask.getString(EntityFields.TASK_TITLE.toString()), jsonTask.getInt(EntityFields.TASK_COLOR.toString()));
        roomTask.setServerId(jsonTask.getInt(EntityFields.TASK_ID.toString()));
        roomTask.setDirty(CLEAN);
        roomTask.setCreationDate(dateConverter(jsonTask.getString(EntityFields.TASK_CREATION_DATE.toString())));
        roomTask.setEnd(dateConverter(nullableDate(jsonTask,EntityFields.TASK_END_DATE.toString())));
        roomTask.setStart(dateConverter(nullableDate(jsonTask,EntityFields.TASK_START_DATE.toString())));
        roomTask.setDeletionTime(dateConverter(nullableDate(jsonTask,EntityFields.TASK_DELETED.toString())));
        roomTask.setCompletionTime(dateConverter(nullableDate(jsonTask,EntityFields.TASK_COMPLETED.toString())));
        roomTask.setDescription(nullableString(jsonTask,EntityFields.TASK_DESCRIPTION.toString()));
        roomTask.setRepeat(nullableInt(jsonTask,EntityFields.TASK_REPEAT_TIME.toString()));
        roomTask.setRepeatUnit(nullableString(jsonTask,EntityFields.TASK_REPEAT_UNIT.toString()));
        roomTask.setTs(jsonTask.getInt(EntityFields.TS.toString()));
        roomTask.setServerListId(jsonTask.getInt(EntityFields.TASK_LIST_ID.toString()));
        roomTask.setPermanentlyDelete(booleanConverter(jsonTask.getInt(EntityFields.TASK_PERMANENTLY_DELETED.toString())));
        return roomTask;
    }

    public static Notification parseNotification(@NonNull final JSONObject jsonNotification) throws JSONException {
        Notification roomNotification = new Notification(dateConverter(jsonNotification.getString(EntityFields.NOTIFICATION_TIME.toString())), jsonNotification.getInt(EntityFields.NOTIFICATION_TASK_ID.toString()));
        roomNotification.setDeleted(booleanConverter(jsonNotification.getInt(EntityFields.NOTIFICATION_DELETED.toString())));
        roomNotification.setTs(jsonNotification.getInt(EntityFields.TS.toString()));
        roomNotification.setUnit(jsonNotification.getString(EntityFields.NOTIFICATION_UNIT.toString()));
        roomNotification.setTaskServerId(jsonNotification.getInt(EntityFields.NOTIFICATION_TASK_ID.toString()));
        return roomNotification;
    }

    public static LocalDateTime dateConverter(final String date){
        return date == null ? null : LocalDateTime.parse(date.replace(" ","T"));
    }
    public static String dateConverter(final LocalDateTime dateTime ){
        return dateTime == null ? null : dateTime.format(Converters.getTimeFormatter()).replace("T"," ");
    }

    public static Long durationConverter(final Duration duration) {
        return duration == null ? null : duration.toMinutes();
    }
    public static boolean booleanConverter(final int number){
        return number == 1;
    }

    private static String nullableString(@NonNull final JSONObject object,@NonNull final String key) throws JSONException {
        return object.isNull(key) ? null : object.getString(key);
    }

    private static Integer nullableInt(@NonNull final JSONObject object,@NonNull final String key) throws JSONException {
        return object.isNull(key) ? null : object.getInt(key);
    }

    private static String nullableDate(@NonNull final JSONObject object,@NonNull final String key) throws JSONException {
        return object.isNull(key) ? null : object.getString(key);
    }
}
