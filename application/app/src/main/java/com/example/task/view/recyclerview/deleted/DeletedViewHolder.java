package com.example.task.view.recyclerview.deleted;

import android.graphics.Color;
import android.view.View;
import android.widget.CheckBox;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.task.R;
import com.example.task.view.recyclerview.interfaces.OnCheckBoxHolderClick;
import com.example.task.view.recyclerview.interfaces.OnHolderClick;

public class DeletedViewHolder extends RecyclerView.ViewHolder{

    final CheckBox checkBox;
    final TextView deletionDate;
    final TextView title;

    public DeletedViewHolder(@NonNull final View itemView,
                             @NonNull final OnCheckBoxHolderClick changeListener,
                             @NonNull final OnHolderClick clickListener){
        super(itemView);
        this.deletionDate = itemView.findViewById(R.id.deletionDate);
        this.checkBox = itemView.findViewById(R.id.deletedItem);
        this.title = itemView.findViewById(R.id.deletedTitle);


        this.checkBox.setOnCheckedChangeListener((l,checked)->{
            this.title.setTextColor(checked ? Color.RED : Color.BLACK);
            changeListener.onCheckBoxHolderClick(getAdapterPosition(),checked);
        });

        this.title.setOnClickListener(l->clickListener.onHolderClick(getAdapterPosition()));
    }
}
