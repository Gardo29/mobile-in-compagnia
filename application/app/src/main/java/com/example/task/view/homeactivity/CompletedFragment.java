package com.example.task.view.homeactivity;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.util.Pair;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.RecyclerView;

import com.example.task.R;
import com.example.task.room.databaseutility.DatabaseWriter;
import com.example.task.room.entities.Task;
import com.example.task.view.UtilityClass;
import com.example.task.view.UtilityTags;
import com.example.task.view.homeactivity.details.TaskDetailsFragment;
import com.example.task.view.recyclerview.completed.CompletedAdapter;
import com.example.task.view.recyclerview.interfaces.OnCheckBoxClick;
import com.example.task.view.recyclerview.interfaces.OnTaskClick;
import com.example.task.viewmodel.GetListsViewModel;
import com.example.task.viewmodel.GetTasksViewModel;

import java.util.List;

import java8.util.stream.Collectors;
import java8.util.stream.StreamSupport;

public class CompletedFragment extends Fragment implements OnTaskClick, OnCheckBoxClick {
    private View view;
    private CompletedAdapter adapter;
    private DatabaseWriter databaseWriter;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        this.view = inflater.inflate(R.layout.fragment_items_list,container,false);
        this.databaseWriter = new DatabaseWriter((AppCompatActivity)this.requireActivity());
        UtilityClass.setupToolbar((AppCompatActivity)requireActivity(),this.view,R.id.itemsListToolbar,"Completed");
        this.setAdapter();
        return this.view;
    }

    private void setAdapter(){
        final RecyclerView recyclerView = this.view.findViewById(R.id.deleteListDetails);
        this.adapter = new CompletedAdapter(this,this);
        recyclerView.setAdapter(this.adapter);
        recyclerView.setHasFixedSize(true);
        final GetTasksViewModel tasksViewModel = new ViewModelProvider(this).get(GetTasksViewModel.class);
        final GetListsViewModel listModel = new ViewModelProvider(this).get(GetListsViewModel.class);

        tasksViewModel.getCompletedTask().observe(this.requireActivity(), tasks->{
            final List<Pair<Task, String>> list = StreamSupport.stream(tasks)
                    .sorted((a,b)-> - a.getCompletionTime().compareTo(b.getCompletionTime()))
                    .map(t -> new Pair<>(t, listModel.getListNameById(t.getListId())))
                    .collect(Collectors.toList());
            this.adapter.setData(list);
        });
    }

    @Override
    public void onCheckBoxClick(Task task, boolean isChecked, int position) {
        task.setCompletionTime(null);
        this.databaseWriter.updateTask(task);
    }

    @Override
    public void onTaskClick(Task task) {
        UtilityClass.replaceFragmentBundle(this.requireActivity(),R.id.drawerLayout,new TaskDetailsFragment(),
                UtilityTags.FRAGMENT_TASK_DETAILS,task,UtilityTags.BUNDLE_TASK);
    }
}
