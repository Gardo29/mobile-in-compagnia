package com.example.task.volley;

import android.app.Activity;
import android.content.Context;
import android.util.Log;

import androidx.annotation.NonNull;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;

public class RequestManager {

    private static final String TAG_REQUEST = "tag-request";
    private static final String TAG_SEND = "tag-send";
    private static final String SERVER_LINK = "https://sitolorenzogardini.altervista.org/";

    private final RequestQueue queue;

    public RequestManager(@NonNull final Context context){
        this.queue = Volley.newRequestQueue(context);
    }
    public void getData(@NonNull final JSONArray requestData,@NonNull final RequestSuccess requestSuccess,@NonNull final RequestFailure requestFailure){
        final JsonArrayRequest request = new JsonArrayRequest (Request.Method.POST, SERVER_LINK,requestData, requestSuccess::onSuccess, requestFailure::onFailure);
        request.setTag(TAG_REQUEST);
        queue.add(request);
    }
    public void sendData(@NonNull final JSONArray sendList,@NonNull final SubmitSuccess submitSuccess,@NonNull final SubmitFailure submitFailure){
        final JsonArrayRequest request = new JsonArrayRequest (Request.Method.POST, SERVER_LINK,sendList, submitSuccess::onSuccess, submitFailure::onFailure);
        request.setTag(TAG_SEND);
        queue.add(request);
    }
    public void cancelConnection(){
        this.queue.cancelAll(TAG_REQUEST);
        this.queue.cancelAll(TAG_SEND);
    }

    public interface RequestSuccess{ public void onSuccess(JSONArray result);}
    public interface RequestFailure{ public void onFailure(VolleyError error);}
    public interface SubmitSuccess{ public void onSuccess(JSONArray result);}
    public interface SubmitFailure{ public void onFailure(VolleyError error);}
}
