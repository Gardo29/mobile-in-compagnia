package com.example.task.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;

import com.example.task.room.entities.Task;
import com.example.task.room.repositories.TaskRepository;

import java.util.HashMap;

public abstract class AbstractTaskViewModel extends AndroidViewModel {

    private final TaskRepository taskRepository;

    AbstractTaskViewModel(@NonNull final Application application) {
        super(application);
        this.taskRepository = new TaskRepository(application);
    }
    protected TaskRepository getTaskRepository(){ return this.taskRepository; }
}
