package com.example.task.view.recyclerview;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.task.R;

public class NotificationViewHolder extends RecyclerView.ViewHolder {
    final TextView notification;
    final ImageView deleteNotification;

    NotificationViewHolder(@NonNull View itemView, @NonNull final NotificationAdapter.OnNotificationClick listener, @NonNull final NotificationAdapter.OnDeleteNotificationClick delete) {
        super(itemView);
        this.notification = itemView.findViewById(R.id.notificationTextView);
        this.deleteNotification = itemView.findViewById(R.id.deleteNotification);
        this.notification.setOnClickListener(l -> listener.onNotificationClick(getAdapterPosition()));
        this.deleteNotification.setOnClickListener(l -> delete.onDeleteNotificationClick(getAdapterPosition()));
    }
}
