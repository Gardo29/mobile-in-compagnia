package com.example.task.notification;

import android.app.AlarmManager;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.os.Build;
import android.util.Log;
import androidx.annotation.NonNull;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import com.example.task.R;
import com.example.task.view.UtilityTags;
import com.example.task.view.homeactivity.MainActivity;
import org.threeten.bp.LocalDateTime;
import org.threeten.bp.ZoneId;
import org.threeten.bp.ZonedDateTime;

public class NotificationUtility {

    public static void createNotificationChannel(@NonNull final Context context, final boolean showBadge, final String name, final String description, final int importance) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            final NotificationChannel channel = new NotificationChannel(UtilityTags.CHANNEL_ID, name, importance);
            channel.setDescription(description);
            channel.setShowBadge(showBadge);

            final NotificationManager notificationManager = context.getSystemService(NotificationManager.class);
            if (notificationManager != null) {
                notificationManager.createNotificationChannel(channel);
            }
        }
    }

    private static NotificationCompat.Builder taskNotificationBuilder(@NonNull final Context context, @NonNull String taskMessage, int taskId) {
        final Intent intent = new Intent(context, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.putExtra(UtilityTags.BUNDLE_TASK_ID, taskId);
        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, intent, 0);

        return new NotificationCompat.Builder(context, UtilityTags.CHANNEL_ID)
                .setSmallIcon(R.drawable.small_logo)
                .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                .setLargeIcon(BitmapFactory.decodeResource(context.getResources(), R.drawable.logo))
                .setContentTitle(taskMessage)
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                .setContentIntent(pendingIntent)
                .setAutoCancel(true);

    }

    static void createTaskNotification(@NonNull final Context context, @NonNull final String taskMessage, final int taskId) {
        final NotificationManagerCompat manager = NotificationManagerCompat.from(context);
        manager.notify(taskId, taskNotificationBuilder(context, taskMessage, taskId).build());
    }


    private static PendingIntent createPendingIntent(@NonNull final Context context, final int taskId) {
        final Intent intent = new Intent(context, AlarmReceiver.class);
        intent.setAction(context.getString(R.string.action_notify_task))
                .setType("T" + taskId)
                .putExtra(UtilityTags.BUNDLE_TASK_ID, taskId);
        return PendingIntent.getBroadcast(context, taskId, intent, PendingIntent.FLAG_UPDATE_CURRENT);
    }

    public static void cancelAlarm(@NonNull final Context context, final int taskId) {
        final AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        final PendingIntent pendingIntent = createPendingIntent(context, taskId);
        if (alarmManager != null) {
            alarmManager.cancel(pendingIntent);
        }
    }

    public static void scheduleAlarm(@NonNull final Context context, @NonNull final LocalDateTime time, final int taskId) {
        final AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        final ZonedDateTime dateTime = time.atZone(ZoneId.systemDefault());
        final long startTime = dateTime.toInstant().toEpochMilli();
        final PendingIntent pendingIntent = createPendingIntent(context, taskId);
        if (alarmManager != null) {
            if (Build.VERSION.SDK_INT < 23) {
                if (System.currentTimeMillis() < startTime) {
                    alarmManager.setExact(AlarmManager.RTC_WAKEUP, startTime, pendingIntent);
                }
            } else {
                if (System.currentTimeMillis() < startTime) {
                    alarmManager.setExactAndAllowWhileIdle(AlarmManager.RTC_WAKEUP, startTime, pendingIntent);
                }
            }
        }
        Log.e("YU","Schedule notification for " + taskId+ " at " + time);
    }
}
