package com.example.task.repetition;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.example.task.R;
import com.example.task.room.TaskDatabase;
import com.example.task.room.daos.TaskDAO;
import com.example.task.room.entities.Task;
import com.example.task.room.repositories.RepositoryUtility;
import com.example.task.view.UtilityClass;
import com.example.task.view.UtilityTags;


public class RepeatReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        if (context != null && intent != null && intent.getAction() != null) {
            if (intent.getAction().equals(context.getString(R.string.action_repeat_task))) {
                if (intent.getExtras() != null) {
                    final int taskId = (int) intent.getExtras().get(UtilityTags.BUNDLE_TASK_ID);
                    final TaskDAO dao = TaskDatabase.getInstance(context).taskDAO();
                    final Task task = RepositoryUtility.getFuture(TaskDatabase.executor.submit(()->dao.getTaskById(taskId)));
                    if (task != null && task.getRepeat() != null) {
                        task.repeat();
                        TaskDatabase.executor.execute(()->dao.updateTask(task));
                        UtilityClass.scheduleRepetitions();
                    }
                }
            }
        }
    }
}
