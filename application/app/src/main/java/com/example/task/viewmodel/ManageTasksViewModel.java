package com.example.task.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;

import com.example.task.room.entities.Task;

import java.util.List;

public class ManageTasksViewModel extends AbstractTaskViewModel {

    public ManageTasksViewModel(@NonNull final Application application) {
        super(application);
    }
    public void updateTask(@NonNull final Task task){this.getTaskRepository().updateTask(task); }
    public void removeTask(@NonNull final Task task){ this.getTaskRepository().removeTask(task); }
    public void insertTask(@NonNull final Task task){ this.getTaskRepository().addTask(task); }
    public void deleteAllTasks() { this.getTaskRepository().deleteAllTasks(); }
    public void deleteAllCompletedTasks() { this.getTaskRepository().deleteAllCompletedTasks(); }
    public void clearDeletedTasks() { this.getTaskRepository().clearDeletedTasks();}
}
