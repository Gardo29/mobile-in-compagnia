package com.example.task.repetition;
import android.content.Context;

import androidx.annotation.NonNull;
import androidx.lifecycle.LifecycleOwner;
import androidx.work.Worker;
import androidx.work.WorkerParameters;

import com.example.task.room.databaseutility.DatabaseWriter;
import com.example.task.room.entities.Notification;
import com.example.task.room.entities.Task;
import com.example.task.room.repositories.NotificationRepository;
import com.example.task.room.repositories.TaskRepository;
import com.example.task.view.UtilityClass;

import org.threeten.bp.Duration;
import org.threeten.bp.LocalDateTime;
import org.threeten.bp.temporal.ChronoUnit;

import java.util.ArrayList;
import java.util.List;

import java8.util.stream.Collectors;
import java8.util.stream.StreamSupport;

public class RepetitionSchedulerWorker extends Worker {
    private final TaskRepository taskRepository;
    private final NotificationRepository notificationRepository;
    private final DatabaseWriter databaseWriter;
    private final Context context;

    public RepetitionSchedulerWorker(@NonNull Context context, @NonNull WorkerParameters workerParams) {
        super(context, workerParams);
        this.taskRepository = new TaskRepository(context);
        this.notificationRepository = new NotificationRepository(context);
        this.databaseWriter = new DatabaseWriter(context);
        this.context = context;
    }

    @NonNull
    @Override
    public Result doWork() {
        final List<Task> tasks = this.taskRepository.getRepeatableTasks();
        UtilityClass.repeatAllTasks(context);
        if(tasks != null && tasks.size() > 0){
            final List<Task> sortedTasks = StreamSupport.stream(tasks)
                    .sorted((a,b) -> RepetitionUtility.sumRepetition(a).compareTo(RepetitionUtility.sumRepetition(b)))
                    .collect(Collectors.toList());
            final List<Task> firsts = this.getFirstTasks(sortedTasks);
            StreamSupport.stream(firsts).forEach(task -> {
                RepetitionUtility.scheduleRepeat(this.getApplicationContext(),task);
                final List<Notification> notificationList = notificationRepository.getNotificationById(task.getServerId());
                StreamSupport.stream(notificationList).forEach(notification -> {
                    if (!notification.isDeleted() && notification.getTime().isBefore(LocalDateTime.now())) {
                        final LocalDateTime newTime = notification.getTime().plus(Duration.of(task.getRepeat(), ChronoUnit.valueOf(task.getRepeatUnit())));
                        this.databaseWriter.updateChangeTimeNotification(notification,newTime,notification.getUnit());
                    }
                });
            });
        }
        return Result.failure();
    }

    private List<Task> getFirstTasks(List<Task> tasks){
        final List<Task> equalsTime = new ArrayList<>();
        equalsTime.add(tasks.get(0));
        for(int i = 1; i<tasks.size(); i++ ){
            final Task current = tasks.get(i);
            final Task prev = tasks.get(i - 1);
            if(!RepetitionUtility.sumRepetition(current).equals(RepetitionUtility.sumRepetition(prev))){
                break;
            }
            equalsTime.add(current);
        }
        return equalsTime;
    }
}
