package com.example.task.view.recyclerview.completed;

import android.graphics.Paint;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.example.task.R;
import com.example.task.view.customcomponents.RoundCheckBox;
import com.example.task.view.recyclerview.interfaces.OnCheckBoxHolderClick;
import com.example.task.view.recyclerview.interfaces.OnHolderClick;

public class CompletedViewHolder extends RecyclerView.ViewHolder {
    private static final boolean DONE = true;
    final RoundCheckBox roundCheckBox;
    final TextView completionDate;
    final TextView title;
    final TextView list;
    final CardView taskBody;
    final TextView startDate;

    public CompletedViewHolder(@NonNull final View itemView,
                               @NonNull final OnCheckBoxHolderClick onCheckBoxHolderClick,
                               @NonNull final OnHolderClick onHolderClick) {
        super(itemView);
        this.roundCheckBox = itemView.findViewById(R.id.roundCheckBox);
        this.completionDate = itemView.findViewById(R.id.completionTimeTextView);
        this.title = itemView.findViewById(R.id.taskName);
        this.list = itemView.findViewById(R.id.listName);
        this.startDate = itemView.findViewById(R.id.taskDate);
        this.taskBody = itemView.findViewById(R.id.cardView);

        this.roundCheckBox.setDone(DONE);
        this.title.setPaintFlags(this.title.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        this.roundCheckBox.setOnClickListener(l->onCheckBoxHolderClick.onCheckBoxHolderClick(getAdapterPosition(),!DONE));
        this.taskBody.setOnClickListener(l -> onHolderClick.onHolderClick(getAdapterPosition()));
    }
}
