package com.example.task.view.homeactivity.login;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.example.task.R;
import com.example.task.room.databaseutility.DatabaseResult;
import com.example.task.room.databaseutility.EncryptionUtility;
import com.example.task.view.UtilityClass;
import com.example.task.view.UtilityTags;
import com.example.task.volley.DataBuilder;
import com.google.android.material.textfield.TextInputLayout;

import org.json.JSONException;


public class LoginFragment extends AbstractLogin {
    private static final String EMPTY = "";
    private View view;
    private TextInputLayout emailTextInput;
    private TextInputLayout passwordTextInput;
    private TextView invalidCredentialsTextView;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater,container,savedInstanceState);
        this.view = inflater.inflate(R.layout.fragment_login,container,false);

        this.loadTextInput();

        this.view.findViewById(R.id.login_layout).setOnFocusChangeListener((v, hasFocus)-> UtilityClass.focusToBackground(this.activity,hasFocus));

        this.view.findViewById(R.id.sign_up_button).setOnClickListener(l-> this.replaceFragmentAnimationLeft(new AccountCreationFragment(), UtilityTags.FRAGMENT_ACCOUNT_CREATION_TAG));

        this.view.findViewById(R.id.login_next_button).setOnClickListener(l->tryCatch(this::processLogin));
        return this.view;
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
        if(!this.networkConnection.isNetworkConnected()){
            UtilityClass.noInternetConnectionToast(this.context);
        }
    }

    private void processLogin() throws JSONException {
        if(this.checkFields()){
            if(this.networkConnection.isNetworkConnected()){
                final String encryptedPassword = EncryptionUtility.digest(this.getText(passwordTextInput));
                final String email = this.getText(emailTextInput);
                final DataBuilder dataBuilder = new DataBuilder(this.activity.getApplication());
                this.connectServer(dataBuilder.createUserLoginSubmit(email,encryptedPassword),success->{
                    this.tryCatch(() -> {
                        if(DatabaseResult.isResultCorrect(success)){
                            this.writeUserCredentials(email,encryptedPassword);
                            this.startHomeActivity();
                        }else{
                            this.fadeInErrorLine();
                        }
                    });
                },failure-> this.connectionError());
            }else{
                UtilityClass.noInternetConnectionToast(this.context);
            }
        }
    }

    private boolean checkFields(){
        boolean flag = true;
        final String password = this.getText(passwordTextInput);
        final String email = this.getText(emailTextInput);
        this.clearTextInput();

        if(email.isEmpty()){
            emailTextInput.setError("Enter email");
            flag = false;
        }
        if(!email.isEmpty() && !Patterns.EMAIL_ADDRESS.matcher(email).matches()){
            emailTextInput.setError("Enter valid email");
            flag = false;
        }
        if(password.isEmpty()){
            passwordTextInput.setError("Enter password");
            flag = false;
        }
        return flag;
    }

    private void clearTextInput(){
        this.invalidCredentialsTextView.setText(EMPTY);
        this.emailTextInput.setErrorEnabled(false);
        this.passwordTextInput.setErrorEnabled(false);
    }

    private void loadTextInput(){
        this.invalidCredentialsTextView = this.view.findViewById(R.id.invalid_credentials_text_view);
        this.emailTextInput = this.view.findViewById(R.id.login_email_text_input);
        this.passwordTextInput = this.view.findViewById(R.id.login_password_text_input);
    }

    private void fadeInErrorLine(){
        final Animation fadeIn = AnimationUtils.loadAnimation(this.context,R.anim.fade_in);
        this.invalidCredentialsTextView.setText("Invalid username or password");
        this.invalidCredentialsTextView.startAnimation(fadeIn);
    }


    @Override
    public void jsonException() {
        UtilityClass.serverResponseProblem(this.context);
    }

    @Override
    public void connectionError() {
        UtilityClass.serverErrorToast(this.context);
    }
}
