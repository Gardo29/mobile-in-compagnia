package com.example.task.view.homeactivity.deleted;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.view.ActionMode;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.RecyclerView;

import com.example.task.R;
import com.example.task.room.databaseutility.DatabaseWriter;
import com.example.task.room.entities.Task;
import com.example.task.view.UtilityClass;
import com.example.task.view.UtilityTags;
import com.example.task.view.homeactivity.details.TaskDetailsFragment;
import com.example.task.view.recyclerview.deleted.DeletedAdapter;
import com.example.task.viewmodel.GetTasksViewModel;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;

import java.util.LinkedList;
import java.util.List;

import java8.util.function.Consumer;
import java8.util.stream.StreamSupport;

public class DeletedFragment extends Fragment implements DeletedAdapter.OnItemClickListener, DeletedAdapter.OnCheckChangeListener{

    private View view;
    private AppCompatActivity activity;
    private ActionMode actionMode;
    private boolean onActionMode;
    private DeletedAdapter adapter;
    private List<Task> selected;
    private boolean areAllSelected;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        this.view = inflater.inflate(R.layout.fragment_items_list,container,false);
        this.activity = (AppCompatActivity)this.requireActivity();
        this.selected = new LinkedList<>();
        this.onActionMode = false;
        UtilityClass.setupToolbar(this.activity,this.view,R.id.itemsListToolbar,"Deleted");
        setHasOptionsMenu(true);
        this.setAdapter();
        this.areAllSelected = false;
        return this.view;
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        inflater.inflate(R.menu.deleted_toolbar_menu,menu);
        final MenuItem menuCheckbox = menu.findItem(R.id.selectAllDeleteMenu);
        final CheckBox checkBox = (CheckBox)menuCheckbox.getActionView();
        checkBox.setButtonTintList(getResources().getColorStateList(R.color.red_checkbox_menu));
        checkBox.setOnCheckedChangeListener((v,checked)->{
            if(checked){
                this.adapter.checkAll();
                this.areAllSelected = true;
                checkBox.setChecked(false);
            }
        });
    }

    private void setAdapter(){
        final RecyclerView recyclerView = this.view.findViewById(R.id.deleteListDetails);
        this.adapter = new DeletedAdapter(this,this);
        recyclerView.setAdapter(this.adapter);
        recyclerView.setHasFixedSize(true);
        final GetTasksViewModel tasksViewModel = new ViewModelProvider(this).get(GetTasksViewModel.class);
        tasksViewModel.getDeletedTasks().observe(this.activity, this.adapter::setData);
    }


    private ActionMode.Callback getMenuCallback(){
        return new ActionMode.Callback() {
            @Override
            public boolean onCreateActionMode(final ActionMode mode,final Menu menu) {
                mode.getMenuInflater().inflate(R.menu.deleted_action_menu, menu);
                CheckBox checkBox = (CheckBox)menu.findItem(R.id.deleteAllActionMenu).getActionView();
                checkBox.setChecked(areAllSelected);
                checkBox.setButtonTintList(getResources().getColorStateList(R.color.red));
                checkBox.setOnCheckedChangeListener((view,checked)->{
                    if(checked){
                        areAllSelected = true;
                        adapter.checkAll();
                    }else{
                        areAllSelected = false;
                        mode.finish();
                    }
                });
                return true;
            }

            @Override
            public boolean onPrepareActionMode(ActionMode mode, Menu menu) { return false; }

            @Override
            public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.restoreDeleted:
                        showDialog("Restore",(dialog,witch)->performActionButton(mode,task -> task.setDeletionTime(null)));
                        break;
                    case R.id.deletePermanently:
                        showDialog("Delete permanently",(dialog,witch)->performActionButton(mode,task -> task.setPermanentlyDelete(true)));
                        break;
                    default:
                        return false;
                }
                return true;
            }
            @Override
            public void onDestroyActionMode(ActionMode mode) {
                onActionMode = false;
                areAllSelected = false;
                uncheckAll();
            }
        };
    }

    private void performActionButton(@NonNull final ActionMode mode,@NonNull final Consumer<Task> consumer){
        final DatabaseWriter databaseWriter = new DatabaseWriter(this.activity);
        StreamSupport.stream(selected).forEach(task->{
            consumer.accept(task);
            databaseWriter.updateTask(task);
        });
        mode.finish();
    }

    private void uncheckAll(){
        actionMode = null;
        onActionMode = false;
        adapter.uncheckAll();
    }

    @Override
    public void onClick(Task task) {
        if(this.onActionMode){
            this.actionMode.finish();
        }
        UtilityClass.replaceFragmentBundle(this.activity,R.id.fragmentContainer,new TaskDetailsFragment(),
                UtilityTags.FRAGMENT_TASK_DETAILS,task,UtilityTags.BUNDLE_TASK);
    }

    @Override
    public void onCheck(final List<Task> selectedTasks) {
        this.selected = selectedTasks;
        if(!this.selected.isEmpty()){
            if(!this.onActionMode){
                this.actionMode = this.activity.startSupportActionMode(this.getMenuCallback());
                this.onActionMode = true;
            }
        }else{
            if(this.actionMode != null){
                this.onActionMode = false;
                this.actionMode.finish();
            }
        }
        if(this.onActionMode && this.actionMode != null){
            this.actionMode.setTitle(Integer.toString(this.selected.size()));
        }
    }

    private void showDialog(@NonNull final String action, @NonNull final DialogInterface.OnClickListener positive){
        new MaterialAlertDialogBuilder(this.requireContext())
                .setTitle(action+" ?")
                .setMessage("Are you sure to "+action.toLowerCase()+" checked tasks?")
                .setNeutralButton("Cancel",(dialog,witch)->{})
                .setPositiveButton("Yes",positive)
                .show();
    }
}