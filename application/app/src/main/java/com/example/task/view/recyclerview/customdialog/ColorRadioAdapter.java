package com.example.task.view.recyclerview.customdialog;

import android.content.res.ColorStateList;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.core.widget.CompoundButtonCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.example.task.R;
import java.util.List;


public class ColorRadioAdapter extends RecyclerView.Adapter<ColorRadioViewHolder> {
    private final List<ColorItem> colors;
    private final ColorRadioAdapter.OnItemClick listener;
    private final int selectedPosition;

    public ColorRadioAdapter(@NonNull final List<ColorItem> colors, @NonNull final ColorRadioAdapter.OnItemClick listener, final int selectedPosition){
        this.colors = colors;
        this.listener = listener;
        this.selectedPosition = selectedPosition > colors.size() ? 0 : selectedPosition;
    }
    @NonNull
    @Override
    public ColorRadioViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        final View holder = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_radio_button,parent,false);
        return new ColorRadioViewHolder(holder,this.listener);
    }

    @Override
    public void onBindViewHolder(@NonNull ColorRadioViewHolder holder, int position) {
        CompoundButtonCompat.setButtonTintList(holder.radioButton, ColorStateList.valueOf(this.colors.get(position).getColor()));
        holder.radioButton.setText(this.colors.get(position).getName());
        if(this.selectedPosition == position){
            holder.radioButton.setOnCheckedChangeListener(null);
        }
        holder.radioButton.setChecked(position == this.selectedPosition);
    }

    @Override
    public int getItemCount() {
        return this.colors.size();
    }
    public interface OnItemClick{ void onItemClick(int position); }

}
