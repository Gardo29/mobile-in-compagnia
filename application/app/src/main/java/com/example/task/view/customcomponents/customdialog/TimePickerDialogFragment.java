package com.example.task.view.customcomponents.customdialog;

import android.app.Dialog;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TimePicker;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import com.example.task.R;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;

public class TimePickerDialogFragment extends DialogFragment {

    private final OnTimeSelected listener;
    private final int hour;
    private final int minute;
    public TimePickerDialogFragment(@NonNull final OnTimeSelected listener, final int hour, final int minute) {
        this.listener = listener;
        this.hour = hour;
        this.minute = minute;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        final View view = LayoutInflater.from(requireContext()).inflate(R.layout.fragment_dialog_time_picker, null);
        TimePicker timePicker = view.findViewById(R.id.timePicker);
        timePicker.setIs24HourView(true);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            timePicker.setHour(hour);
            timePicker.setMinute(minute);
        } else {
            timePicker.setCurrentHour(hour);
            timePicker.setCurrentMinute(minute);
        }
        timePicker.setOnTimeChangedListener((view1, hourOfDay, minute) -> listener.onTimeSelected(hourOfDay, minute));
        return new MaterialAlertDialogBuilder(this.requireContext())
                .setView(view).create();
    }

    public interface OnTimeSelected {
        void onTimeSelected(int hour, int minutes);
    }
}
