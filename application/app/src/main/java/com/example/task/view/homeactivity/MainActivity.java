package com.example.task.view.homeactivity;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.ViewModelProvider;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.widget.TextView;

import com.example.task.R;
import com.example.task.room.databaseutility.DatabaseWriter;
import com.example.task.room.entities.Notification;
import com.example.task.room.entities.Task;
import com.example.task.view.AbstractToolbarActivity;
import com.example.task.view.UtilityClass;
import com.example.task.view.UtilityTags;

import com.example.task.view.listviewactivity.ListViewActivity;
import com.example.task.viewmodel.GetTasksViewModel;
import com.example.task.volley.DatabaseUpdater;
import com.google.android.material.tabs.TabLayout;

import org.threeten.bp.LocalDate;
import org.threeten.bp.format.DateTimeFormatter;

import java.util.List;

import java8.util.stream.StreamSupport;

public class MainActivity extends AbstractToolbarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        final TextView monthDay = findViewById(R.id.todayMonthDay);
        monthDay.setText(getString(R.string.month_day, LocalDate.now().format(DateTimeFormatter.ofPattern("MMMM dd"))));
        UtilityClass.replaceFragmentNoBack(this, R.id.fragmentContainer, new ToDoTaskFragment(), UtilityTags.FRAGMENT_TODO_TASK);

        //add menu to choose between list and task creation
        findViewById(R.id.addButton).setOnClickListener(l -> UtilityClass.addFragment(R.id.drawerLayout, this, new MenuFragment(), UtilityTags.FRAGMENT_MENU));

        this.addTabListener(this);
    }

    @Override
    protected void onResumeFragments() {
        super.onResumeFragments();
        if (getSupportFragmentManager().findFragmentByTag(UtilityTags.FRAGMENT_MENU) != null) {
            UtilityClass.closeFragment(this, getSupportFragmentManager().findFragmentByTag(UtilityTags.FRAGMENT_MENU));
        }
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_main;
    }

    @Override
    protected DrawerLayout getDrawerLayout() {
        return findViewById(R.id.drawerLayout);
    }

    @Override
    protected Toolbar getToolbar() {
        return findViewById(R.id.toolbar);
    }

    @Override
    protected void selectBottomMenuItem(MenuItem menuItem) {
        super.selectBottomMenuItem(menuItem);
        if (menuItem.getItemId() == R.id.openAllListView) {
            UtilityClass.startActivity(this, ListViewActivity.class);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == UtilityTags.ACTIVITY_CREATE_TASK && resultCode == Activity.RESULT_OK) {
            if (data != null && data.getExtras() != null
                    && data.getSerializableExtra(UtilityTags.BUNDLE_NOTIFICATION) != null) {
                final List<Notification> notifications = (List<Notification>) data.getSerializableExtra(UtilityTags.BUNDLE_NOTIFICATION);
                final GetTasksViewModel model = new ViewModelProvider(this).get(GetTasksViewModel.class);
                final Task task = model.getLastTask();
                if (notifications != null) {
                    StreamSupport.stream(notifications).forEach(t -> {
                        t.setTaskId(task.getTaskId());
                        final DatabaseWriter writer = new DatabaseWriter(this);
                        writer.insertNotification(t);
                    });
                }
            }
        }
    }

    private void addTabListener(FragmentActivity activity) {
        final TabLayout tabs = findViewById(R.id.tabs);
        tabs.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                switch (tab.getPosition()) {
                    case 0:
                        UtilityClass.replaceFragmentAnimationNoBackRight(activity, R.id.fragmentContainer, new ToDoTaskFragment(), UtilityTags.FRAGMENT_TODO_TASK);
                        break;
                    case 1:
                        UtilityClass.replaceFragmentAnimationNoBackLeft(activity, R.id.fragmentContainer, new DoneTaskFragment(), UtilityTags.FRAGMENT_DONE_TASK);
                        break;
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });
    }
}
