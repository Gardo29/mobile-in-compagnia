package com.example.task.view.recyclerview;

import android.text.Layout;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.example.task.R;

public class ListViewHolder extends RecyclerView.ViewHolder{

    final TextView listTitle;

    ListViewHolder(@NonNull View itemView, @NonNull final OnListClick listener) {
        super(itemView);
        this.listTitle = itemView.findViewById(R.id.listTitleView);

        itemView.setOnClickListener(l -> listener.onListClick(getAdapterPosition()));
    }

    public interface OnListClick { void onListClick(int position); }
}
