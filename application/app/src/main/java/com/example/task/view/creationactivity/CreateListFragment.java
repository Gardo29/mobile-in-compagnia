package com.example.task.view.creationactivity;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Switch;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;

import com.example.task.R;
import com.example.task.room.databaseutility.DatabaseWriter;
import com.example.task.room.entities.List;
import com.example.task.view.UtilityClass;

import org.threeten.bp.LocalDateTime;

import static android.app.Activity.RESULT_OK;

public class CreateListFragment extends Fragment {

    private Button save;
    private ImageButton close;
    private EditText listName;
    private Switch enableNotification;
    private Switch displayHome;
    private TextView description;
    private Context context;
    private View view;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        this.view = inflater.inflate(R.layout.fragment_create_list, container, false);
        return this.view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        this.context = requireContext();
        final FragmentActivity activity = getActivity();


        if (activity != null) {
            initViewComponents(activity);

            this.listName.requestFocus();
            UtilityClass.openKeyboard(activity);
            this.enableNotification.setChecked(true);
            this.displayHome.setChecked(true);

            this.close.setOnClickListener(l -> {
                activity.finish();
            });

            this.save.setOnClickListener(l-> {
                if (checkList()) {
                    final DatabaseWriter databaseWriter = new DatabaseWriter((AppCompatActivity)activity);
                    databaseWriter.insertList(createList());
                    activity.setResult(RESULT_OK);
                    activity.finish();
                }
            });
            activity.findViewById(R.id.listCreationLayout)
                    .setOnFocusChangeListener((v, hasFocus)-> UtilityClass.focusToBackground(this.requireActivity(),hasFocus));
        }
    }

    /*------------------- private utility functions -------------------*/
    private void initViewComponents(FragmentActivity activity) {
        this.save = activity.findViewById(R.id.saveButton);
        this.close = activity.findViewById(R.id.closeFormButton);
        this.listName = activity.findViewById(R.id.titleEdit);
        this.enableNotification = activity.findViewById(R.id.enableNotificationSwitch);
        this.displayHome = activity.findViewById(R.id.displayHomeDetails);
        this.description = activity.findViewById(R.id.addDescriptionView);
    }

    private List createList(){
        final List list = new List(listName.getText().toString());
        list.setCreationDate(LocalDateTime.now());
        list.setDisplayHome(this.displayHome.isChecked());
        list.setDeleted(false);
        list.setEnabledNotifications(this.enableNotification.isChecked());
        if (description.getText().length() != 0){
            list.setDescription(description.getText().toString());
        }
        return list;
    }

    private boolean checkList() {
        if (this.listName.getText().toString().isEmpty()) {
            UtilityClass.showToast(requireContext(), "Please insert a title to your list!");
            return false;
        }
        return true;
    }

}
