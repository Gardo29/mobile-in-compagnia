package com.example.task.room;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;


import com.example.task.room.daos.ListDAO;
import com.example.task.room.daos.NotificationDAO;
import com.example.task.room.daos.TaskDAO;
import com.example.task.room.entities.Converters;
import com.example.task.room.entities.List;
import com.example.task.room.entities.Notification;
import com.example.task.room.entities.Task;
import com.example.task.room.entities.TaskFts;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Database(entities = {Task.class, TaskFts.class, List.class,Notification.class}, version = 1,exportSchema = false)
@TypeConverters({Converters.class})
public abstract class TaskDatabase extends RoomDatabase {
    private static final int THREADS = 4;
    private static TaskDatabase instance;
    public static final ExecutorService executor = Executors.newFixedThreadPool(THREADS);

    public static TaskDatabase getInstance(@NonNull final Context context){
        if(instance == null){
            synchronized (TaskDatabase.class){
                if(instance == null){
                    instance = Room.databaseBuilder(context.getApplicationContext(),TaskDatabase.class,"task_database").build();
                }
            }
        }
        return instance;
    }
    public abstract TaskDAO taskDAO();
    public abstract ListDAO listDAO();
    public abstract NotificationDAO notificationDAO();

}
