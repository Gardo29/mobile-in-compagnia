package com.example.task.viewmodel;

import android.app.Application;
import android.app.DirectAction;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.preference.Preference;

import com.example.task.room.entities.Notification;

import org.threeten.bp.LocalDateTime;
import org.threeten.bp.temporal.ChronoUnit;

import java.util.List;

import java8.util.stream.StreamSupport;

public class GetNotificationsViewModel extends AbstractNotificationViewModel{

    public GetNotificationsViewModel(@NonNull Application application) { super(application); }

    public Notification getNotificationByServerTaskId(final int serverTaskId, @NonNull final LocalDateTime time){ return this.getNotificationRepository().getNotificationByServerTaskId(serverTaskId,time); }
    public List<Notification> getDirtyNotifications(){return this.getNotificationRepository().getDirtyNotifications();}
}
