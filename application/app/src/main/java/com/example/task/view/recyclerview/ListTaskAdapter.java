package com.example.task.view.recyclerview;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.task.R;
import com.example.task.room.entities.Task;
import com.example.task.view.recyclerview.interfaces.OnCheckBoxClick;
import com.example.task.view.recyclerview.interfaces.OnTaskClick;

import org.threeten.bp.LocalDate;
import org.threeten.bp.LocalDateTime;
import org.threeten.bp.format.DateTimeFormatter;

import java.util.ArrayList;
import java.util.List;

import java8.util.function.Supplier;

public class ListTaskAdapter extends RecyclerView.Adapter<ListTaskViewHolder> {

    private final OnCheckBoxClick checkBoxListener;
    private final OnTaskClick cardViewListener;
    private final boolean doneState;
    private final List<Task> taskList = new ArrayList<>();

    public ListTaskAdapter(@NonNull OnCheckBoxClick checkBoxListener, @NonNull OnTaskClick cardViewListener, boolean done){
        this.checkBoxListener = checkBoxListener;
        this.cardViewListener = cardViewListener;
        this.doneState = done;
    }

    @NonNull
    @Override
    public ListTaskViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        final View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_task_b,
                parent, false);
        return new ListTaskViewHolder(layoutView, (position,isChecked) -> this.checkBoxListener.onCheckBoxClick(taskList.get(position),isChecked, position),
                position -> this.cardViewListener.onTaskClick(this.taskList.get(position)), this.doneState);
    }

    @Override
    public void onBindViewHolder(@NonNull ListTaskViewHolder holder, int position) {
        final Task currentTask = taskList.get(position);
        holder.setIsRecyclable(false);
        holder.checkBox.setColor(currentTask.getColor());
        holder.taskTitle.setText(currentTask.getTitle());

        this.setDate(() -> currentTask.isExpired() && currentTask.getStart() == null, holder.date, currentTask.getEnd(), "MM.dd");
        this.setDate(() -> currentTask.isExpired() && currentTask.getStart() != null, holder.date, currentTask.getStart(), "MM.dd HH:mm");
        this.setDate(() -> !currentTask.isExpired() && currentTask.getStart() == null && currentTask.getEnd().toLocalDate().isAfter(LocalDate.now()), holder.date, currentTask.getEnd(), "MM.dd");
        this.setDate(() -> !currentTask.isExpired() && currentTask.getStart() != null && currentTask.getStart().toLocalDate().isAfter(LocalDate.now()), holder.date, currentTask.getStart(), "MM.dd");
        this.setDate(() -> !currentTask.isExpired() && currentTask.getStart() != null && currentTask.getStart().toLocalDate().isEqual(LocalDate.now()), holder.date, currentTask.getStart(), "HH:mm");

        if (currentTask.isExpired()) {
            holder.taskTitle.setTextColor(Task.EXPIRED_TASK_COLOR);
            holder.date.setTextColor(Task.EXPIRED_TASK_COLOR);
        }

        holder.checkBox.setDone(currentTask.getCompletionTime() != null);
    }

    public void setData(@NonNull List<Task> data) {
        this.taskList.clear();
        this.taskList.addAll(data);
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return taskList.size();
    }

    private void setDate(@NonNull final Supplier<Boolean> condition, @NonNull final TextView textView, @NonNull LocalDateTime time, String dateFormat) {
        if (condition.get()) {
            textView.setText(time.format(DateTimeFormatter.ofPattern(dateFormat)));
        }
    }
}
