package com.example.task.view.recyclerview.deleted;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView.Adapter;

import com.example.task.R;
import com.example.task.room.entities.Task;

import org.threeten.bp.LocalDate;
import org.threeten.bp.LocalDateTime;
import org.threeten.bp.format.DateTimeFormatter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import java8.util.stream.Collectors;
import java8.util.stream.IntStreams;


public class DeletedAdapter extends Adapter<DeletedViewHolder> {
    private List<Task> items;
    private final OnItemClickListener itemClickListener;
    private final OnCheckChangeListener checkChangeListener;
    private  List<Boolean> holders;

    public DeletedAdapter(@NonNull final OnItemClickListener itemClickListener,
                          @NonNull final OnCheckChangeListener checkChangeListener){
        this.itemClickListener = itemClickListener;
        this.checkChangeListener = checkChangeListener;
        this.items = new ArrayList<>();
    }
    @NonNull
    @Override
    public DeletedViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        final View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_deleted,parent,false);

        return new DeletedViewHolder(view,((position, checked) -> {

                this.holders.set(position,checked);
                final List<Task> checkedTasks = IntStreams.range(0,this.items.size())
                        .filter(f->this.holders.get(f))
                        .mapToObj(i-> this.items.get(i)).collect(Collectors.toList());
                this.checkChangeListener.onCheck(checkedTasks);

        }),(position -> this.itemClickListener.onClick(this.items.get(position))));
    }

    @Override
    public void onBindViewHolder(@NonNull DeletedViewHolder holder, int position) {
        final Task task = this.items.get(position);
        if(task.getDeletionTime() != null){
            final LocalDate deletionDate = task.getDeletionTime().toLocalDate();
            holder.deletionDate.setText(deletionDate.format(DateTimeFormatter.ISO_DATE));
            holder.title.setText(task.getTitle());
            holder.checkBox.setChecked(this.holders.get(position));

            holder.deletionDate.setVisibility(View.VISIBLE);
            if(position > 0){
                final LocalDateTime prevDate = this.items.get(position - 1).getDeletionTime();
                if(prevDate != null && deletionDate.isEqual(prevDate.toLocalDate()))
                holder.deletionDate.setVisibility(View.GONE);
            }
        }
    }

    @Override
    public int getItemCount() { return this.items.size(); }

    public void setData(@NonNull final List<Task> deletedTasks){
        this.items = deletedTasks;
        this.holders = new ArrayList<>(Collections.nCopies(this.items.size(), false));
        notifyDataSetChanged();
    }

    public void checkAll(){
        Collections.fill(this.holders,true);
        notifyDataSetChanged();
    }

    public void uncheckAll(){
        Collections.fill(this.holders,false);
        notifyDataSetChanged();
    }

    public interface OnCheckChangeListener{ void onCheck(List<Task> checkedTasks);}
    public interface OnItemClickListener{ void onClick(Task task); }
}
