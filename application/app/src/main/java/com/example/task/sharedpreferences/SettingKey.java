package com.example.task.sharedpreferences;

import androidx.annotation.NonNull;

public enum SettingKey {
    SETTINGS_AVAILABLE("settings_available"),
    MUTE_ALL_NOTIFICATIONS("mute_all_notifications"),
    DEFAULT_COLOR("default_color"),
    DEFAULT_LIST_SUMMARY("default_list_summary"),
    DEFAULT_LIST_VALUE("default_list_value"),
    DEFAULT_LIST("default_list"),
    DEFAULT_SERVER_LIST("default_server_list"),
    USER_EMAIL("user_email"),
    ENCRYPTED_PASSWORD("encrypted_password"),
    APP_VERSION("app_version");

    private final String string;

    SettingKey(@NonNull final String string){
        this.string = string;
    }

    @NonNull
    @Override
    public String toString(){ return this.string; }

}
