package com.example.task.view.recyclerview;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.task.R;

import java.util.ArrayList;
import java.util.List;

public class NotificationAdapter extends RecyclerView.Adapter<NotificationViewHolder> {

    private final OnNotificationClick listener;
    private final OnDeleteNotificationClick delete;
    private List<String> notifications = new ArrayList<>();

    public NotificationAdapter(final @NonNull OnNotificationClick listener, final @NonNull OnDeleteNotificationClick delete) {
        this.listener = listener;
        this.delete = delete;
    }

    @NonNull
    @Override
    public NotificationViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        final View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_notification,
                parent, false);
        return new NotificationViewHolder(layoutView, listener, delete);
    }

    @Override
    public void onBindViewHolder(@NonNull NotificationViewHolder holder, int position) {
        final String currentNotification = notifications.get(position);
        holder.notification.setText(currentNotification);
        holder.deleteNotification.setVisibility(View.VISIBLE);
        if (position == 0) {
            holder.notification.setCompoundDrawablesWithIntrinsicBounds(R.drawable.notification, 0, 0, 0);
            holder.notification.setPadding(0, 0, 0, 0);
        }
        if (position == this.notifications.size()-1) {
            holder.deleteNotification.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        return this.notifications.size();
    }

    public void setData(List<String> data) {
        this.notifications.clear();
        this.notifications.addAll(data);
        notifyDataSetChanged();
    }

    public interface OnNotificationClick {
        void onNotificationClick(int position);
    }

    public interface OnDeleteNotificationClick {
        void onDeleteNotificationClick(int position);
    }
}


