package com.example.task.view.recyclerview.customdialog;

import android.view.View;
import android.widget.CheckBox;
import android.widget.RadioButton;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.task.R;

public class ListDialogViewHolder extends RecyclerView.ViewHolder {
    final RadioButton radioButton;

    ListDialogViewHolder(@NonNull View itemView, @NonNull OnListClick listener) {
        super(itemView);
        this.radioButton = itemView.findViewById(R.id.colorRadiobutton);
        this.radioButton.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if(isChecked){
                listener.onListClick(getAdapterPosition());
            }
        });
    }

    public interface OnListClick { void onListClick(int position); }
}
