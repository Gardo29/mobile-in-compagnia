package com.example.task.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;

import com.example.task.room.entities.List;

public class GetListsViewModel extends AbstractListViewModel{

    private final LiveData<java.util.List<List>> allList;

    public GetListsViewModel(@NonNull Application application) {
        super(application);
        this.allList = this.getListRepository().getLists();
    }

    public LiveData<java.util.List<List>> getLists() {return this.allList;}
    public List getListById(final int listId){ return this.getListRepository().getListById(listId); }
    public List getListByServerId(final int serverListId){ return this.getListRepository().getListByServerId(serverListId); }
    public String getListNameById(final int listId) { return  this.getListRepository().getListNameById(listId);}
    public java.util.List<List> getDirtyLists(){ return this.getListRepository().getDirtyLists();}
}
