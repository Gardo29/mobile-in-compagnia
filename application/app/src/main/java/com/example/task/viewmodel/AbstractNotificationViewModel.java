package com.example.task.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;

import com.example.task.room.repositories.NotificationRepository;

public abstract class AbstractNotificationViewModel extends AndroidViewModel {

    private final NotificationRepository notificationRepository;

    AbstractNotificationViewModel(@NonNull final Application application) {
        super(application);
        this.notificationRepository = new NotificationRepository(application);
    }
    protected NotificationRepository getNotificationRepository(){ return this.notificationRepository; }
}
