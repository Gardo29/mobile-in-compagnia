package com.example.task.view.creationactivity;
import android.content.Intent;
import android.os.Bundle;
import android.transition.Slide;
import android.view.Gravity;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.task.R;
import com.example.task.view.UtilityClass;
import com.example.task.view.UtilityTags;

public class CreationActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_for_fragments);
        final Intent intent = getIntent();
        final Class<?> fragment = (Class<?>) intent.getSerializableExtra(UtilityTags.CREATION_FRAGMENT);
        if (fragment == CreateListFragment.class) {
            UtilityClass.replaceFragment(this,R.id.fragmentContainer, new CreateListFragment(), UtilityTags.FRAGMENT_CREATE_LIST);
        } else if (fragment == CreateTaskFragment.class) {
            UtilityClass.replaceFragment(this,R.id.fragmentContainer, new CreateTaskFragment(), UtilityTags.FRAGMENT_CREATE_TASK);
        } else if (fragment == UpdateTaskFragment.class) {
            UtilityClass.replaceFragment(this, R.id.fragmentContainer, new UpdateTaskFragment(), UtilityTags.FRAGMENT_UPDATE_TASK);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.finish();
    }
}