package com.example.task.room.repositories;

import android.app.Application;
import android.content.Context;
import android.database.sqlite.SQLiteConstraintException;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;


import com.example.task.room.TaskDatabase;
import com.example.task.room.entities.Notification;
import com.example.task.viewmodel.DataBuffers;

import org.threeten.bp.LocalDateTime;

import java.util.List;

public class NotificationRepository extends AbstractRepository {
    private final DataBuffers dataBuffers;

    public NotificationRepository(@NonNull final Context context) {
        super(context);
        this.dataBuffers = DataBuffers.getInstance();
    }

    public void addNotification(@NonNull final Notification notification){
        TaskDatabase.executor.execute(()-> {
            try{
                this.getNotificationDAO().insertNotification(notification);
            }catch (Exception e){
                int a = 5;
            }
        });
    }

    public void rawAddNotification(@NonNull final Notification notification){
        this.getNotificationDAO().insertNotification(notification);
    }

    public void updateNotification(@NonNull final Notification notification){
        if(notification.getTaskServerId() != null){
            dataBuffers.putServerNotification(notification.getTaskServerId(),notification.getTime(),notification);
        }
        dataBuffers.putRoomNotification(notification.getTaskId(),notification.getTime(),notification);
        TaskDatabase.executor.execute(()-> this.getNotificationDAO().updateNotification(notification));
    }

    public void removeNotification(@NonNull final Notification notification){
        TaskDatabase.executor.execute(()-> this.getNotificationDAO().deleteNotification(notification));
    }

    public void clearDeletedNotifications(){
        TaskDatabase.executor.execute(()-> this.getNotificationDAO().clearDeletedNotifications());
    }

    public List<Notification> getNotificationById(final int serverTaskId){
        return RepositoryUtility.getFuture(TaskDatabase.executor.submit(()-> this.getNotificationDAO().getNotificationById(serverTaskId)));
    }

    public Notification getNotificationById(final int taskId, @NonNull final LocalDateTime time){
        return RepositoryUtility.getFuture(TaskDatabase.executor.submit(()-> this.getNotificationDAO().getNotificationById(taskId,time)));
    }

    public Notification getNotificationByServerTaskId(final int serverTaskId, @NonNull final LocalDateTime time){
        Notification notification = this.dataBuffers.getServerNotification(serverTaskId,time);
        if(notification == null){
            notification = RepositoryUtility.getFuture(TaskDatabase.executor.submit(()-> this.getNotificationDAO().getNotificationByServerTaskId(serverTaskId,time)));
            if(notification != null) {
                this.dataBuffers.putServerNotification(serverTaskId, time, notification);
            }
        }
        return notification;
    }

    public List<Notification> getDirtyNotifications(){
        return RepositoryUtility.getFuture(TaskDatabase.executor.submit(()-> this.getNotificationDAO().getDirtyNotifications()));
    }

    public List<Notification> getNotificationsByTime(@NonNull final LocalDateTime time){
        return RepositoryUtility.getFuture(TaskDatabase.executor.submit(()-> this.getNotificationDAO().getNotificationsByTime(time)));
    }

    public LocalDateTime getTime(){
        return RepositoryUtility.getFuture(TaskDatabase.executor.submit(()->this.getNotificationDAO().getTime(LocalDateTime.now(), LocalDateTime.now().plusDays(1))));
    }

}
