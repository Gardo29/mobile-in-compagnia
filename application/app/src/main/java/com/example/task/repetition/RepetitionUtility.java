package com.example.task.repetition;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.StatusBarManager;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.util.Log;

import androidx.annotation.NonNull;

import com.example.task.R;
import com.example.task.notification.NotificationUtility;
import com.example.task.room.entities.Notification;
import com.example.task.room.entities.Task;
import com.example.task.view.UtilityTags;

import org.threeten.bp.Duration;
import org.threeten.bp.LocalDateTime;
import org.threeten.bp.ZoneId;
import org.threeten.bp.ZonedDateTime;
import org.threeten.bp.temporal.ChronoUnit;
import org.threeten.bp.temporal.TemporalUnit;

public class RepetitionUtility {

    static void scheduleRepeat(@NonNull final Context context, final Task task) {
        final AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        final LocalDateTime startTime = startTime(task);
        final ZonedDateTime dateTime = startTime
                .plus(getRepeat(task))
                .atZone(ZoneId.systemDefault());
        final long repeatMillis = dateTime.toInstant().toEpochMilli();
        final PendingIntent pendingIntent = createPendingIntent(context, task);
        if (alarmManager != null) {
            if (Build.VERSION.SDK_INT < 23) {
                if (System.currentTimeMillis() < repeatMillis) {
                    alarmManager.setExact(AlarmManager.RTC_WAKEUP, repeatMillis, pendingIntent);
                }
            } else {
                if (System.currentTimeMillis() < repeatMillis) {
                    alarmManager.setExactAndAllowWhileIdle(AlarmManager.RTC_WAKEUP, repeatMillis, pendingIntent);
                }
            }
        }
    }

    public static void cancelRepetition(@NonNull final Context context, @NonNull final Task task) {
        final AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        final PendingIntent pendingIntent = createPendingIntent(context, task);
        if (alarmManager != null) {
            alarmManager.cancel(pendingIntent);
        }
    }

    private static PendingIntent createPendingIntent(@NonNull final Context context, final Task task) {
        final Intent intent = new Intent(context, RepeatReceiver.class);
        intent.setAction(context.getString(R.string.action_repeat_task))
                .putExtra(UtilityTags.BUNDLE_TASK_ID,task.getTaskId())
                .setType("R" + task.getTaskId());
        return PendingIntent.getBroadcast(context, task.getTaskId(), intent, PendingIntent.FLAG_UPDATE_CURRENT);
    }

    public static Duration getRepeat(@NonNull final Task task){
        return Duration.of(task.getRepeat(), ChronoUnit.valueOf(task.getRepeatUnit()));
    }

    public static LocalDateTime startTime(@NonNull final Task task){
        return task.getStart() == null ? task.getEnd().truncatedTo(ChronoUnit.DAYS) : task.getStart();
    }

    public static LocalDateTime sumRepetition(@NonNull final Task task){
        return RepetitionUtility.startTime(task).plus(RepetitionUtility.getRepeat(task));
    }
}
