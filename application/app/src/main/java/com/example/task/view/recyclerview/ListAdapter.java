package com.example.task.view.recyclerview;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.task.R;
import com.example.task.room.entities.List;

import java.util.ArrayList;

public class ListAdapter extends RecyclerView.Adapter<ListViewHolder> {

    private final int size;
    private final OnListClickListener listener;
    private final java.util.List<List> list = new ArrayList<>();

    public ListAdapter(@NonNull OnListClickListener listener, int width){
        this.listener = listener;
        this.size = width/3;
    }

    @NonNull
    @Override
    public ListViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        final View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_list,
                parent, false);
        final ViewGroup.LayoutParams params = layoutView.getLayoutParams();
        params.height = size;
        return new ListViewHolder(layoutView, position -> listener.onItemClick(this.list.get(position), position));
    }

    @Override
    public void onBindViewHolder(@NonNull ListViewHolder holder, int position) {
        final List currentList = list.get(position);
        holder.listTitle.setText(currentList.getName());
    }

    @Override
    public int getItemCount() {
        return this.list.size();
    }

    public void setData(java.util.List<List> data) {
        this.list.clear();
        this.list.addAll(data);
        notifyDataSetChanged();
    }

    public interface OnListClickListener {
        void onItemClick(List list, int position);
    }
}
