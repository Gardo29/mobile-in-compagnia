package com.example.task.room.repositories;

import android.app.Application;
import android.app.DownloadManager;
import android.content.Context;
import android.text.method.MultiTapKeyListener;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.sqlite.db.SimpleSQLiteQuery;
import androidx.sqlite.db.SupportSQLiteQuery;


import com.example.task.room.TaskDatabase;
import com.example.task.room.entities.Converters;
import com.example.task.room.entities.Task;
import com.example.task.room.relations.Reminder;
import com.example.task.viewmodel.DataBuffers;
import com.google.common.collect.ObjectArrays;

import org.threeten.bp.LocalDateTime;

import java.text.CollationElementIterator;
import java.util.List;

import java8.util.stream.Collectors;
import java8.util.stream.StreamSupport;

public class TaskRepository extends AbstractRepository{
    private final DataBuffers dataBuffers;

    public TaskRepository(@NonNull final Context context){
        super(context);
        this.dataBuffers = DataBuffers.getInstance();
    }

    public void addTask(@NonNull final Task task){
        TaskDatabase.executor.submit(()-> this.getTaskDAO().insertTask(task));
    }

    public long rawAddTask(@NonNull final Task task){
        return this.getTaskDAO().insertTask(task);
    }

    public void updateTask(@NonNull final Task task){
        if(task.getServerId() != null){
            dataBuffers.putServerTask(task.getServerId(),task);
        }
        dataBuffers.putRoomTask(task.getTaskId(),task);
        TaskDatabase.executor.execute(()-> this.getTaskDAO().updateTask(task));
    }

    public void removeTask(@NonNull final Task task){
        TaskDatabase.executor.execute(()-> this.getTaskDAO().deleteTask(task));
    }

    public LiveData<List<Task>> getUncompletedTasks(){
        return RepositoryUtility.getFuture(TaskDatabase.executor.submit(()-> this.getTaskDAO().getUncompletedTasks()));
    }

    public LiveData<List<Task>> getCompletedTasks(){
        return RepositoryUtility.getFuture(TaskDatabase.executor.submit(() -> getTaskDAO().getCompletedTasks()));
    }

    public Task getLastTask() {
        return RepositoryUtility.getFuture(TaskDatabase.executor.submit(() -> getTaskDAO().getLastTask()));
    }

    public List<Task> getDirtyTasks(){
        return RepositoryUtility.getFuture(TaskDatabase.executor.submit(()-> this.getTaskDAO().getDirtyTasks()));
    }

    public List<Reminder> getAllNotifications(){
        return RepositoryUtility.getFuture(TaskDatabase.executor.submit(()-> this.getTaskDAO().getAllNotifications()));
    }

    public LiveData<List<Task>> getDeletedTasks(){
        return RepositoryUtility.getFuture(TaskDatabase.executor.submit(()->this.getTaskDAO().getDeletedTasks()));
    }

    public Task getTaskById(final int taskId){
        Task task = this.dataBuffers.getRoomTask(taskId);
        if(task == null){
            task = RepositoryUtility.getFuture(TaskDatabase.executor.submit(()-> this.getTaskDAO().getTaskById(taskId)));
            if(task != null) {
                this.dataBuffers.putRoomTask(taskId, task);
            }
        }
        return task;
    }
    public Task getTaskByServerId(final int taskServerId){
        Task task = this.dataBuffers.getServerTask(taskServerId);
        if(task == null){
            task = RepositoryUtility.getFuture(TaskDatabase.executor.submit(()-> this.getTaskDAO().getTaskByServerId(taskServerId)));
            if(task != null){
                this.dataBuffers.putServerTask(taskServerId,task);
            }
        }
        return task;
    }
    public void deleteAllTasks(){
        TaskDatabase.executor.execute( ()-> this.getTaskDAO().deleteAllTasks(LocalDateTime.now()));
    }

    public void clearDeletedTasks(){
        TaskDatabase.executor.execute( ()-> this.getTaskDAO().clearDeletedTasks());
    }
    public void deleteAllCompletedTasks(){
        TaskDatabase.executor.execute( ()-> this.getTaskDAO().deleteAllCompletedTasks(LocalDateTime.now()));
    }
    public LiveData<List<Task>> searchTasks(@NonNull final String title,@NonNull final List<Integer> colors,
                                            final boolean completed,final boolean deleted){
        return RepositoryUtility.getFuture(TaskDatabase.executor.submit(()-> {
            final StringBuilder query = new StringBuilder();
            query.append("SELECT tasks.* FROM tasks,task_fts WHERE task_id = docid AND task_fts MATCH ? AND permanently_deleted = 0 AND (");
            query.append(StreamSupport.stream(colors).map(c-> "color = ?").collect(Collectors.joining(" OR ")));
            query.append(")");
            query.append(completed ? " AND completion_time IS NOT NULL" : " AND completion_time IS NULL");
            query.append(deleted ? " AND deletion_time IS NOT NULL" : " AND deletion_time IS NULL");
            query.append(" ORDER BY title");
            return this.getTaskDAO().searchTasks(new SimpleSQLiteQuery(query.toString(), ObjectArrays.concat(new Object[] {String.format("*%s*",title)},colors.toArray(),Object.class)));
        }));
    }

    public List<Task> getRepeatableTasks() {
        return RepositoryUtility.getFuture(TaskDatabase.executor.submit(()->this.getTaskDAO().getRepeatableTasks()));
    }

    public List<Task> getTasks() {
        return RepositoryUtility.getFuture(TaskDatabase.executor.submit(()->this.getTaskDAO().getTasks()));
    }

}
