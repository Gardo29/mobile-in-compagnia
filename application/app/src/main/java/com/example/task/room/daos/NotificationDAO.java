package com.example.task.room.daos;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;
import com.example.task.room.entities.Notification;
import org.threeten.bp.LocalDateTime;

import java.util.List;

@Dao
public interface NotificationDAO {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertNotification(Notification notification);

    @Update
    void updateNotification(Notification notification);

    @Delete
    void deleteNotification(Notification notification);

    @Query("SELECT * FROM notifications")
    List<Notification> getNotifications();

    @Query("SELECT * FROM notifications WHERE deleted = 0 AND time = :time")
    List<Notification> getNotificationsByTime(LocalDateTime time);

    @Query("SELECT time FROM notifications WHERE time > :now AND time < :end ORDER BY date(time) ASC LIMIT 1")
    LocalDateTime getTime(LocalDateTime now, LocalDateTime end);

    @Query("SELECT * FROM notifications WHERE dirty = 1")
    List<Notification> getDirtyNotifications();

    @Query("SELECT * FROM notifications WHERE task_server_id = :serverTaskId")
    List<Notification> getNotificationById(int serverTaskId);

    @Query("SELECT * FROM notifications WHERE task_id = :taskId AND time = :time")
    Notification getNotificationById(int taskId, LocalDateTime time);

    @Query("SELECT * FROM notifications WHERE task_server_id = :serverTaskId AND time = :time")
    Notification getNotificationByServerTaskId(int serverTaskId, LocalDateTime time);

    @Query("DELETE FROM notifications WHERE dirty = 0 AND deleted = 1")
    void clearDeletedNotifications();
}
