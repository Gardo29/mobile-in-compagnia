package com.example.task.notification;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import com.example.task.R;
import com.example.task.room.TaskDatabase;
import com.example.task.room.daos.TaskDAO;
import com.example.task.room.entities.Task;
import com.example.task.room.repositories.RepositoryUtility;
import com.example.task.view.UtilityClass;
import com.example.task.view.UtilityTags;

public class AlarmReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        if (context != null && intent != null && intent.getAction() != null) {
            if (intent.getAction().equals(context.getString(R.string.action_notify_task))) {
                if (intent.getExtras() != null) {
                    final int taskId = (int) intent.getExtras().get(UtilityTags.BUNDLE_TASK_ID);
                    final TaskDAO dao = TaskDatabase.getInstance(context).taskDAO();
                    final Task task = RepositoryUtility.getFuture(TaskDatabase.executor.submit(()->dao.getTaskById(taskId)));
                    if (task != null) {
                        Log.e("YU", "Creating notification for " + task.getTitle() + " " + taskId);
                        String taskMessage = task.getRepeat() == null ? task.getTitle() : "Repeating " + task.getTitle() + " notification";
                        NotificationUtility.createTaskNotification(context, taskMessage, taskId);
                        UtilityClass.scheduleNextNotifications();
                    }
                }
            }
        }
    }

}
