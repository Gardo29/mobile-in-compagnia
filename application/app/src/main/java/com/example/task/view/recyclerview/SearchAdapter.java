package com.example.task.view.recyclerview;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Paint;
import android.text.method.MultiTapKeyListener;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.util.Pair;
import androidx.recyclerview.widget.RecyclerView;

import com.example.task.R;
import com.example.task.repetition.RepetitionUtility;
import com.example.task.room.entities.Converters;
import com.example.task.room.entities.Task;
import com.example.task.view.recyclerview.interfaces.OnCheckBoxClick;
import com.example.task.view.recyclerview.interfaces.OnTaskClick;

import org.threeten.bp.LocalDate;
import org.threeten.bp.LocalDateTime;
import org.threeten.bp.format.DateTimeFormatter;

import java.util.ArrayList;
import java.util.List;

import java8.util.function.Supplier;

public class SearchAdapter extends RecyclerView.Adapter<TaskViewHolder> {
    private final OnCheckBoxClick checkBoxListener;
    private final OnTaskClick cardViewListener;
    private List<Pair<Task,String>> resultTasks;

    public SearchAdapter(@NonNull OnCheckBoxClick checkBoxListener, @NonNull OnTaskClick cardViewListener){
        this.resultTasks = new ArrayList<>();
        this.checkBoxListener = checkBoxListener;
        this.cardViewListener = cardViewListener;
    }
    @NonNull
    @Override
    public TaskViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        final View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_task,parent,false);
        return new TaskViewHolder(view, (position,isChecked) -> this.checkBoxListener.onCheckBoxClick(resultTasks.get(position).first,isChecked,position),
                position -> this.cardViewListener.onTaskClick(this.resultTasks.get(position).first),false);
    }

    @Override
    public void onBindViewHolder(@NonNull TaskViewHolder holder, int position) {
        final Task currentTask = this.resultTasks.get(position).first;
        holder.taskTitle.setText(currentTask.getTitle());
        holder.checkBox.setColor(currentTask.getDeletionTime() != null ? Color.LTGRAY : currentTask.getColor());
        holder.checkBox.setDone(currentTask.getCompletionTime() != null);

        this.setDate(() -> currentTask.isExpired() && currentTask.getStart() == null, holder.date, currentTask.getEnd(), "MM.dd");
        this.setDate(() -> currentTask.isExpired() && currentTask.getStart() != null, holder.date, currentTask.getStart(), "MM.dd HH:mm");
        this.setDate(() -> !currentTask.isExpired() && currentTask.getStart() != null && currentTask.getStart().toLocalDate().isEqual(LocalDate.now()), holder.date, currentTask.getStart(), "HH:mm");

        holder.listTitle.setText(this.resultTasks.get(position).second);

        if (currentTask.isExpired()) {
            holder.taskTitle.setTextColor(Task.EXPIRED_TASK_COLOR);
            holder.date.setTextColor(Task.EXPIRED_TASK_COLOR);
        }

        if(currentTask.getCompletionTime() != null){
            holder.taskTitle.setPaintFlags(holder.taskTitle.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        }else {
            holder.taskTitle.setPaintFlags(0);
        }
    }


    @Override
    public int getItemCount() { return this.resultTasks.size();}

    public void setData(@NonNull List<Pair<Task,String>> tasks){
        this.resultTasks = tasks;
        notifyDataSetChanged();
    }

    private void setDate(@NonNull final Supplier<Boolean> condition, @NonNull final TextView textView, @NonNull LocalDateTime time, String dateFormat) {
        if (condition.get()) {
            textView.setText(time.format(DateTimeFormatter.ofPattern(dateFormat)));
        }
    }
}
