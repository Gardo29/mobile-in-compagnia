package com.example.task.view.listviewactivity;

import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.widget.Toolbar;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.task.R;
import com.example.task.repetition.RepeatReceiver;
import com.example.task.repetition.RepetitionUtility;
import com.example.task.room.databaseutility.DatabaseWriter;
import com.example.task.room.entities.List;
import com.example.task.room.entities.Notification;
import com.example.task.room.entities.Task;
import com.example.task.view.AbstractToolbarActivity;
import com.example.task.view.UtilityClass;
import com.example.task.view.UtilityTags;
import com.example.task.view.creationactivity.CreateTaskFragment;
import com.example.task.view.creationactivity.CreationActivity;
import com.example.task.view.homeactivity.details.ListDetailsFragment;
import com.example.task.view.homeactivity.details.TaskDetailsFragment;
import com.example.task.view.recyclerview.ListTaskAdapter;
import com.example.task.view.recyclerview.interfaces.OnCheckBoxClick;
import com.example.task.view.recyclerview.interfaces.OnTaskClick;
import com.example.task.viewmodel.GetListsViewModel;
import com.example.task.viewmodel.GetTasksViewModel;
import com.example.task.viewmodel.ManageTasksViewModel;

import org.threeten.bp.LocalDate;
import org.threeten.bp.LocalDateTime;
import org.threeten.bp.format.DateTimeFormatter;

import java.util.ArrayList;

import java8.util.stream.Collectors;
import java8.util.stream.StreamSupport;

public class SingleListActivity extends AbstractToolbarActivity implements OnCheckBoxClick, OnTaskClick {

    private ListTaskAdapter todayAdapter;
    private ListTaskAdapter laterAdapter;
    private ManageTasksViewModel manageTasksViewModel;
    private DatabaseWriter databaseWriter;
    private List list;
    private java.util.List<Task> todayTasks = new ArrayList<>();
    private java.util.List<Task> laterTasks = new ArrayList<>();
    private TextView todayTextView;
    private TextView monthDayTextView;
    private TextView laterTextView;
    private RecyclerView laterRecyclerView;
    private RecyclerView todayRecyclerView;

    private View separator1;
    private View separator2;
    private View separator3;

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.databaseWriter = new DatabaseWriter(this);
        final Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            list = (List) bundle.getSerializable(UtilityTags.BUNDLE_LIST);
            final GetTasksViewModel model = new ViewModelProvider(this).get(GetTasksViewModel.class);
            this.todayTextView = findViewById(R.id.todayTextView);
            this.monthDayTextView = findViewById(R.id.todayMonthDay);
            this.laterTextView = findViewById(R.id.laterTextView);
            this.monthDayTextView.setText(LocalDate.now().format(DateTimeFormatter.ofPattern("MMMM dd")));
            this.separator1 = findViewById(R.id.view1);
            this.separator2 = findViewById(R.id.view2);
            this.separator3 = findViewById(R.id.view3);
            this.setTodayRecyclerView();
            this.setLaterRecyclerView();
            model.getUncompletedTask().observe(this, listTasks -> {
                java.util.List<Task> listsTasks = StreamSupport.stream(listTasks)
                        .filter(l -> l.getListId() == (list.getListId()))
                        .collect(Collectors.toList());

                this.todayTasks = StreamSupport.stream(listsTasks)
                        .filter(t -> (RepetitionUtility.startTime(t).toLocalDate().isEqual(LocalDate.now())) || t.isExpired())
                        .sorted(Task::compareTo)
                        .collect(Collectors.toList());
                this.laterTasks = StreamSupport.stream(listsTasks)
                        .filter(t -> !t.isExpired() &&  RepetitionUtility.startTime(t).toLocalDate().isAfter(LocalDate.now()))
                        .sorted(Task::compareTo)
                        .collect(Collectors.toList());

                if (todayTasks.isEmpty()) {
                    this.todayTextView.setVisibility(View.GONE);
                    this.monthDayTextView.setVisibility(View.GONE);
                    this.todayRecyclerView.setVisibility(View.GONE);
                    this.separator1.setVisibility(View.GONE);
                } else {
                    this.todayTextView.setVisibility(View.VISIBLE);
                    this.monthDayTextView.setVisibility(View.VISIBLE);
                    this.todayRecyclerView.setVisibility(View.VISIBLE);
                    this.separator1.setVisibility(View.VISIBLE);
                }

                if (laterTasks.isEmpty()) {
                    this.laterTextView.setVisibility(View.GONE);
                    this.laterRecyclerView.setVisibility(View.GONE);
                    this.separator3.setVisibility(View.GONE);
                } else {
                    this.laterTextView.setVisibility(View.VISIBLE);
                    this.laterRecyclerView.setVisibility(View.VISIBLE);
                    this.separator3.setVisibility(View.VISIBLE);
                }

                if (!todayTasks.isEmpty() && !laterTasks.isEmpty()) {
                    this.separator2.setVisibility(View.VISIBLE);
                } else {
                    this.separator2.setVisibility(View.GONE);
                }
                todayAdapter.setData(this.todayTasks);
                laterAdapter.setData(this.laterTasks);
            });
        }

        findViewById(R.id.addButton).setOnClickListener(l -> {
            final Bundle data = new Bundle();
            data.putSerializable(UtilityTags.BUNDLE_LIST, this.list);
            data.putSerializable(UtilityTags.CREATION_FRAGMENT, CreateTaskFragment.class);
            UtilityClass.startActivityBundleForResult(this, CreationActivity.class, data, UtilityTags.ACTIVITY_CREATE_TASK);
        });

        getToolbar().setOnClickListener(l->{
            UtilityClass.replaceFragmentBundle(this,R.id.drawerLayout,new ListDetailsFragment(),UtilityTags.FRAGMENT_LIST_DETAILS,this.list,UtilityTags.BUNDLE_LIST);
        });
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_single_list;
    }

    @Override
    protected DrawerLayout getDrawerLayout() {
        return findViewById(R.id.drawerLayout);
    }

    @Override
    protected Toolbar getToolbar() {
        return findViewById(R.id.toolbar);
    }

    @Override
    protected void selectBottomMenuItem(MenuItem menuItem) {
        super.selectBottomMenuItem(menuItem);
        if (menuItem.getItemId() == R.id.openAllListView) {
            this.onBackPressed();
        }
    }

    private void setTodayRecyclerView() {
        this.todayRecyclerView = findViewById(R.id.todayRecyclerView);
        this.todayRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        this.todayRecyclerView.setHasFixedSize(true);

        this.todayAdapter = new ListTaskAdapter(this, this, false);
        this.todayRecyclerView.setAdapter(todayAdapter);
    }

    private void setLaterRecyclerView() {
        this.laterRecyclerView = findViewById(R.id.laterRecyclerView);
        this.laterRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        this.laterRecyclerView.setHasFixedSize(true);

        this.laterAdapter = new ListTaskAdapter(this, this, false);
        this.laterRecyclerView.setAdapter(laterAdapter);
    }

    @Override
    public void onCheckBoxClick(Task task, boolean isChecked, int position) {
        task.setCompletionTime(LocalDateTime.now());
        this.databaseWriter.updateTask(task);
        todayAdapter.notifyDataSetChanged();
        laterAdapter.notifyDataSetChanged();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == UtilityTags.ACTIVITY_CREATE_TASK && resultCode == Activity.RESULT_OK) {
            if (data != null && data.getExtras() != null
                    && data.getSerializableExtra(UtilityTags.BUNDLE_NOTIFICATION) != null) {
                final java.util.List<Notification> notifications = (java.util.List<Notification>) data.getSerializableExtra(UtilityTags.BUNDLE_NOTIFICATION);
                final GetTasksViewModel model = new ViewModelProvider(this).get(GetTasksViewModel.class);
                final Task task = model.getLastTask();
                if (notifications != null) {
                    StreamSupport.stream(notifications).forEach(t -> {
                        t.setTaskId(task.getTaskId());
                        final DatabaseWriter writer = new DatabaseWriter(this);
                        writer.insertNotification(t);
                    });
                }
                UtilityClass.scheduleNextNotifications();
            }
        }
    }

    @Override
    public void onTaskClick(Task task) {
        UtilityClass.replaceFragmentBundle(this, R.id.drawerLayout, new TaskDetailsFragment(),
                UtilityTags.FRAGMENT_TASK_DETAILS, task, UtilityTags.BUNDLE_TASK);
    }
}
