package com.example.task.volley;

import android.app.Application;
import android.content.Context;
import android.util.Log;

import androidx.annotation.NonNull;

import com.example.task.room.entities.List;
import com.example.task.room.entities.Notification;
import com.example.task.room.entities.Task;
import com.example.task.sharedpreferences.RoomKey;
import com.example.task.sharedpreferences.RoomTs;
import com.example.task.room.databaseutility.EncryptionUtility;
import com.example.task.room.databaseutility.EntityFields;
import com.example.task.room.databaseutility.JSONParser;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.threeten.bp.LocalDateTime;
import org.threeten.bp.temporal.ChronoUnit;

public class DataBuilder {
    private static final String TOKEN = "cats";
    private static final String ENCRYPTED_TOKEN = EncryptionUtility.digest(TOKEN);
    /*       Actions       */
    private static final String REQUEST = "pull";
    private static final String SUBMIT = "push";
    private static final String LOGIN = "login";
    private static final String MODIFY_ACCOUNT = "modify_account";
    private static final String REGISTRATION = "registration";
    /*       Keys        */
    private static final String TOKEN_KEY = "token";
    private static final String USER_KEY = "user_email";
    private static final String ACTION_KEY = "action";
    private static final String ENTITY = "entity";
    private static final String PASSWORD = "password";
    private static final String COMPLETE_NAME = "complete_name";
    /*       Ts       */
    private static final String TASK_TS_KEY = "task_ts";
    private static final String LIST_TS_KEY = "list_ts";
    private static final String NOTIFICATION_TS_KEY = "notification_ts";
    /*       Values     */
    private static final String LIST = "list";
    private static final String NOTIFICATION = "notification";
    private static final String TASK = "task";

    private final Context context;

    public DataBuilder(@NonNull final Context context){
        this.context = context;
    }
    public JSONArray createDataRequest(@NonNull final String userEmail,@NonNull final String password) throws JSONException {
        final JSONObject data  = this.createRequestHead(userEmail,password,REQUEST);
        final RoomTs ts = new RoomTs(this.context);
        int listTs = ts.getTs(RoomKey.LISTS_TS);
        int taskTs = ts.getTs(RoomKey.TASKS_TS);
        int notificationTs = ts.getTs(RoomKey.NOTIFICATIONS_TS);
        data.put(TASK_TS_KEY,taskTs);
        data.put(LIST_TS_KEY,listTs);
        data.put(NOTIFICATION_TS_KEY,notificationTs);
        return new JSONArray().put(data);
    }

    public JSONArray createListSubmit(@NonNull final String userEmail,@NonNull final String password,@NonNull final List list) throws JSONException {
        final JSONObject data = this.createRequestHead(userEmail,password,SUBMIT);
        data.put(ENTITY,LIST);
        if(list.getServerId() != null){
            data.put(EntityFields.LIST_ID.toString(),list.getServerId());
        }
        data.put(EntityFields.LIST_NAME.toString(),list.getName());
        data.put(EntityFields.LIST_DESCRIPTION.toString(),this.nullField(list.getDescription()));
        data.put(EntityFields.LIST_CREATION_DATE.toString(), JSONParser.dateConverter(list.getCreationDate()));
        data.put(EntityFields.LIST_DELETED.toString(),list.isDeleted());
        data.put(EntityFields.LIST_DISPLAY_HOME.toString(),list.isDisplayHome());
        data.put(EntityFields.LIST_ENABLED_NOTIFICATIONS.toString(),list.isEnabledNotifications());
        return new JSONArray().put(data);
    }

    public JSONArray createTaskSubmit(@NonNull final String userEmail,@NonNull final String password,@NonNull final Task task) throws JSONException {
        final JSONObject data = createRequestHead(userEmail,password,SUBMIT);
        data.put(ENTITY,TASK);
        if(task.getServerId() != null){
            data.put(EntityFields.TASK_ID.toString(),task.getServerId());
        }
        data.put(EntityFields.TASK_REPEAT_UNIT.toString(),this.nullField(task.getRepeatUnit()));
        data.put(EntityFields.TASK_TITLE.toString(),task.getTitle());
        data.put(EntityFields.TASK_DESCRIPTION.toString(),this.nullField(task.getDescription()));
        data.put(EntityFields.TASK_CREATION_DATE.toString(), JSONParser.dateConverter(task.getCreationDate()));
        data.put(EntityFields.TASK_DELETED.toString(),this.nullField(JSONParser.dateConverter(task.getDeletionTime())));
        data.put(EntityFields.TASK_COLOR.toString(),task.getColor());
        data.put(EntityFields.TASK_START_DATE.toString(),this.nullField(JSONParser.dateConverter(task.getStart())));
        data.put(EntityFields.TASK_END_DATE.toString(),this.nullField(JSONParser.dateConverter(task.getEnd())));
        data.put(EntityFields.TASK_REPEAT_TIME.toString(),this.nullField(task.getRepeat()));
        data.put(EntityFields.TASK_COMPLETED.toString(),this.nullField(JSONParser.dateConverter(task.getCompletionTime())));
        data.put(EntityFields.TASK_LIST_ID.toString(),task.getServerListId());
        data.put(EntityFields.TASK_PERMANENTLY_DELETED.toString(),task.isPermanentlyDelete());
        return new JSONArray().put(data);
    }

    public JSONArray createNotificationSubmit(@NonNull final String userEmail,@NonNull final String password,@NonNull final Notification notification) throws JSONException {
        final JSONObject data = createRequestHead(userEmail,password,SUBMIT);

        data.put(EntityFields.NOTIFICATION_UNIT.toString(),notification.getUnit());
        data.put(ENTITY,NOTIFICATION);
        data.put(EntityFields.NOTIFICATION_TASK_ID.toString(),notification.getTaskServerId());
        data.put(EntityFields.NOTIFICATION_TIME.toString(),JSONParser.dateConverter(notification.getTime()));
        data.put(EntityFields.NOTIFICATION_DELETED.toString(),notification.isDeleted());
        return new JSONArray().put(data);
    }
    public JSONArray createUserCreationSubmit(@NonNull final String userEmail,@NonNull final String completeName,@NonNull final String password) throws JSONException{
        return new JSONArray().put(this.createRequestHead(userEmail,password,REGISTRATION).put(COMPLETE_NAME,completeName));
    }
    public JSONArray createUserLoginSubmit(@NonNull final String userEmail,@NonNull final String password) throws JSONException{
        return new JSONArray().put(this.createRequestHead(userEmail,password,LOGIN));
    }
    private JSONObject createRequestHead(@NonNull final String userEmail,@NonNull final String password,@NonNull final String action) throws JSONException {
        return new JSONObject().put(TOKEN_KEY,ENCRYPTED_TOKEN).put(ACTION_KEY,action).put(USER_KEY,userEmail).put(PASSWORD,password);
    }
    private Object nullField(final Object field){
        return field == null ? JSONObject.NULL : field;
    }
}
