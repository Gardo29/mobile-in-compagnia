package com.example.task.view.homeactivity.details;

import android.app.Activity;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import com.example.task.R;
import com.example.task.room.databaseutility.DatabaseWriter;
import com.example.task.room.entities.List;
import com.example.task.room.repositories.ListRepository;
import com.example.task.view.UtilityClass;
import com.example.task.view.UtilityTags;
import com.example.task.view.listviewactivity.ListViewActivity;
import com.example.task.viewmodel.GetListsViewModel;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.google.android.material.textfield.TextInputLayout;

import org.checkerframework.checker.nullness.Opt;

import java.util.Objects;

import java8.util.Optional;

public class ListDetailsFragment extends Fragment {

    private List list;
    private AppCompatActivity activity;
    private Optional<String> newName;
    private View view;
    private Optional<Boolean> displayHome;
    private Optional<String> description;
    private DatabaseWriter databaseWriter;
    private Optional<Boolean> enableNotifications;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        this.view = inflater.inflate(R.layout.fragment_list_details,container,false);
        this.activity = (AppCompatActivity)this.requireActivity();
        this.newName = Optional.empty();
        this.displayHome = Optional.empty();
        this.databaseWriter = new DatabaseWriter(this.activity);
        this.description = Optional.empty();
        this.enableNotifications = Optional.empty();
        this.list = Objects.requireNonNull((List)getArguments().get(UtilityTags.BUNDLE_LIST));
        final TextInputLayout descriptionTextInput = this.view.findViewById(R.id.detailsDescriptionLayout);

        if(this.list.getDescription() != null){
            descriptionTextInput.getEditText().setText(this.list.getDescription());
            descriptionTextInput.setHint("");
        }

        UtilityClass.setupToolbar(this.activity,view,R.id.detailsToolbar,list.getName());
        setHasOptionsMenu(true);

        final Switch displayHome = view.findViewById(R.id.displayHomeDetails);
        displayHome.setChecked(this.list.isDisplayHome());
        displayHome.setOnCheckedChangeListener((buttonView, isChecked) ->
            this.displayHome = (isChecked != this.list.isDisplayHome()) ? Optional.of(isChecked) : Optional.empty()
        );

        final Switch enableNotifications = view.findViewById(R.id.enableNotificationSwitch);
        enableNotifications.setChecked(this.list.isEnabledNotifications());
        enableNotifications.setOnCheckedChangeListener((buttonView, isChecked) ->
                this.enableNotifications = (isChecked != this.list.isEnabledNotifications()) ? Optional.of(isChecked) : Optional.empty()
        );
        descriptionTextInput.getEditText().addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) { }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) { description = Optional.of(s.toString()); }

            @Override
            public void afterTextChanged(Editable s) { }
        });

        view.findViewById(R.id.deleteListDetails).setOnClickListener(l->{
            this.list.setDeleted(true);
            this.databaseWriter.updateList(list);
            this.activity.finish();
        });

        return view;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        final View view = LayoutInflater.from(this.requireContext()).inflate(R.layout.fragment_dialog_rename_list,null);
        final EditText newNameEditText = view.findViewById(R.id.newNameEditText);
        newNameEditText.setText(this.newName.isPresent() ? this.newName.get() : this.list.getName());
        new MaterialAlertDialogBuilder(this.requireContext())
                .setView(view)
                .setPositiveButton("OK",(dialog, which) -> {
                    final String newName = newNameEditText.getText().toString();
                    if(!newName.isEmpty() && !newName.equals(this.list.getName())){
                        this.newName = Optional.of(newName);
                        this.activity.getSupportActionBar().setTitle(newName);
                    }
                })
                .setNegativeButton("Cancel",(dialog, which) -> {})
                .show();
        return super.onOptionsItemSelected(item);
    }

    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.details_toolbar,menu);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        UtilityClass.closeKeyboard(this.activity);
    }

    @Override
    public void onStop() {
        super.onStop();
        boolean update = false;
        if(this.newName.isPresent()){
            final TextView title = this.activity.findViewById(R.id.toolbarTitleView);
            title.setText(getString(R.string.list_title, this.newName.get()));
            final String newName = this.newName.get();
            if(!newName.equals(this.list.getName())){
                this.list.setName(this.newName.get());
                update = true;
            }
        }
        if(this.description.isPresent()){
            this.list.setDescription(this.description.get().isEmpty() ? null : this.description.get());
            update = true;
        }
        if(this.enableNotifications.isPresent()){
            this.list.setEnabledNotifications(this.enableNotifications.get());
            update = true;
        }
        if(this.displayHome.isPresent()){
            this.list.setDisplayHome(this.displayHome.get());
            update = true;
        }

        if(update){
            databaseWriter.updateList(this.list);
        }
    }
}
