package com.example.task.room.daos;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;


import com.example.task.room.entities.List;


@Dao
public interface ListDAO {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    long insertList(List list);

    @Update
    void updateList(List list);

    @Delete
    void deleteList(List list);

    @Query("SELECT * FROM lists WHERE deleted = 0 ORDER BY creation_date")
    LiveData<java.util.List<List>> getLists();

    @Query("SELECT name FROM lists WHERE list_id = :id")
    String getListNameById(int id);

    @Query("SELECT * FROM lists WHERE list_id = :id")
    List getListById(int id);

    @Query("SELECT * FROM lists WHERE server_id = :id")
    List getListByServerId(int id);

    @Query("SELECT * FROM lists WHERE dirty = 1")
    java.util.List<List> getDirtyLists();

    @Query("UPDATE lists SET deleted = 1, dirty = 1")
    void deleteAllLists();

    @Query("DELETE FROM lists WHERE deleted = 1 AND dirty = 0")
    void clearDeletedLists();

}
