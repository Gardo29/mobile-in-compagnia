package com.example.task.view.homeactivity;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;

import com.example.task.R;
import com.example.task.view.UtilityClass;
import com.example.task.view.UtilityTags;
import com.example.task.view.creationactivity.CreationActivity;
import com.example.task.view.creationactivity.CreateListFragment;
import com.example.task.view.creationactivity.CreateTaskFragment;

public class MenuFragment extends Fragment {


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        final View view = inflater.inflate(R.layout.fragment_add_menu, container, false);

        final ConstraintLayout layout = view.findViewById(R.id.menu_container);
        final FragmentActivity activity = getActivity();
        final Bundle fragmentClass = new Bundle();
        if (activity != null) {
            layout.setOnClickListener(v -> UtilityClass.closeFragment(activity, this));

            view.findViewById(R.id.addListButton).setOnClickListener(v -> {
                fragmentClass.putSerializable(UtilityTags.CREATION_FRAGMENT, CreateListFragment.class);
                UtilityClass.startActivityBundleForResult(activity, CreationActivity.class, fragmentClass, UtilityTags.ACTIVITY_CREATE_LIST);
            });

            view.findViewById(R.id.addTaskButton).setOnClickListener(v -> {
                fragmentClass.putSerializable(UtilityTags.CREATION_FRAGMENT, CreateTaskFragment.class);
                UtilityClass.startActivityBundleForResult(activity, CreationActivity.class, fragmentClass, UtilityTags.ACTIVITY_CREATE_TASK);
            });
        }
        return view;
    }
}
