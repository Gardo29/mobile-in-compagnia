package com.example.task.room.daos;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.RawQuery;
import androidx.room.Transaction;
import androidx.room.Update;
import androidx.sqlite.db.SupportSQLiteQuery;


import com.example.task.room.entities.Task;
import com.example.task.room.entities.TaskFts;
import com.example.task.room.relations.Reminder;

import org.threeten.bp.LocalDateTime;

import java.util.List;

@Dao
public interface TaskDAO {
    /* insert */
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    long insertTask(Task task);

    @Update
    void updateTask(Task task);

    @Delete
    void deleteTask(final Task task);

    @Query("SELECT * FROM tasks WHERE completion_time IS NULL AND deletion_time IS NULL AND permanently_deleted = 0")
    LiveData<List<Task>> getUncompletedTasks();

    @Query("SELECT * FROM tasks WHERE completion_time IS NOT NULL AND deletion_time IS NULL AND permanently_deleted = 0 ORDER BY completion_time, start_date")
    LiveData<List<Task>> getCompletedTasks();


    @Transaction
    @Query("SELECT * FROM tasks")
    List<Reminder> getAllNotifications();

    @Query("SELECT * FROM tasks WHERE deletion_time IS NOT NULL AND permanently_deleted = 0 ORDER BY deletion_time DESC")
    LiveData<List<Task>> getDeletedTasks();

    @Query("SELECT * FROM tasks ORDER BY task_id DESC LIMIT 1")
    Task getLastTask();

    @Query("SELECT * FROM tasks WHERE task_id = :taskId")
    Task getTaskById(final int taskId);

    @Query("SELECT * FROM tasks WHERE server_id = :serverTaskId")
    Task getTaskByServerId(final int serverTaskId);

    @Query("SELECT * FROM tasks WHERE dirty = 1")
    List<Task> getDirtyTasks();

    @Query("UPDATE tasks SET dirty = 1, deletion_time = :deletionTime WHERE deletion_time IS NULL")
    void deleteAllTasks(LocalDateTime deletionTime);

    @Query("UPDATE tasks SET dirty = 1, deletion_time = :deletionTime WHERE completion_time IS NOT NULL AND deletion_time IS NULL")
    void deleteAllCompletedTasks(LocalDateTime deletionTime);

    @Query("DELETE FROM tasks WHERE dirty = 0 AND permanently_deleted = 1")
    void clearDeletedTasks();

    @RawQuery(observedEntities = {Task.class, TaskFts.class})
    LiveData<List<Task>> searchTasks(SupportSQLiteQuery query);

    @Query("SELECT * FROM tasks WHERE repeat_time IS NOT NULL AND deletion_time IS NULL AND permanently_deleted = 0")
    List<Task> getRepeatableTasks();

    @Query("SELECT * FROM tasks")
    List<Task> getTasks();
}
