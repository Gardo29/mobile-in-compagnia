package com.example.task.view;

import android.content.Intent;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.lifecycle.ViewModelProvider;
import androidx.room.Database;
import androidx.work.Constraints;
import androidx.work.ExistingPeriodicWorkPolicy;
import androidx.work.ExistingWorkPolicy;
import androidx.work.NetworkType;
import androidx.work.OneTimeWorkRequest;
import androidx.work.PeriodicWorkRequest;
import androidx.work.WorkManager;

import com.example.task.R;
import com.example.task.sharedpreferences.SettingKey;
import com.example.task.sharedpreferences.UserSettings;
import com.example.task.sync.SyncSchedulerWorker;
import com.example.task.view.homeactivity.CompletedFragment;
import com.example.task.view.homeactivity.deleted.DeletedActivity;
import com.example.task.view.homeactivity.login.LoginActivity;
import com.example.task.view.homeactivity.settings.SettingsActivity;
import com.example.task.view.homeactivity.search.SearchActivity;
import com.example.task.viewmodel.GetTasksViewModel;
import com.example.task.viewmodel.ManageTasksViewModel;
import com.example.task.volley.DatabaseUpdater;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.navigation.NavigationView;

import org.threeten.bp.LocalDateTime;

import java.util.Objects;
import java.util.concurrent.TimeUnit;

import java8.util.stream.StreamSupport;

public abstract class AbstractToolbarActivity extends AppCompatActivity {

    private DrawerLayout drawerLayout;
    private ActionBarDrawerToggle drawerToggle;
    private DatabaseUpdater databaseUpdater;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayoutId());
        drawerLayout = getDrawerLayout();
        setUpMenuAndToolBar();

        this.databaseUpdater = new DatabaseUpdater(this);
        databaseUpdater.syncData(
                requestFailure -> Log.e("log", "requestFailure"),
                submitFailure -> Log.e("log", "submitFailure"),
                jsonError -> Log.e("log", "jsonError"));
    }

    @Override
    protected void onStop() {
        super.onStop();
        drawerLayout.closeDrawers();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        this.databaseUpdater.syncData(requestFailure -> Log.e("log", "requestFailure"),
                submitFailure -> Log.e("log", "submitFailure"),
                jsonError -> Log.e("log", "jsonError"));
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        // The action bar home/up action should open or close the drawer.
        if (item.getItemId() == android.R.id.home) {
            drawerLayout.openDrawer(GravityCompat.START);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        drawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(@NonNull Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        drawerToggle.onConfigurationChanged(newConfig);
    }

    protected abstract int getLayoutId();

    protected abstract DrawerLayout getDrawerLayout();

    protected abstract Toolbar getToolbar();

    private void setUpMenuAndToolBar() {
        //set toolbar to replace the ActionBar
        final Toolbar toolbar = getToolbar();
        setSupportActionBar(toolbar);

        //setup menu icon
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);

        //setup drawer view
        drawerToggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.drawer_open, R.string.drawer_close);
        drawerLayout.addDrawerListener(drawerToggle);

        final NavigationView navigationView = findViewById(R.id.navigationView);
        setupDrawerContent(navigationView);

        final BottomNavigationView bottomNavigationView = findViewById(R.id.bottomMenuNavigation);
        setUpBottomContent(bottomNavigationView);
    }

    /*bottom menu*/
    private void setUpBottomContent(BottomNavigationView navigationView) {
        navigationView.setOnNavigationItemSelectedListener(menuItem -> {
            selectBottomMenuItem(menuItem);
            return true;
        });
    }

    protected void selectBottomMenuItem(MenuItem menuItem) {

        switch (menuItem.getItemId()) {
            case R.id.openSearchView:
                UtilityClass.startActivity(this, SearchActivity.class);
                break;
            case R.id.openDeletedTaskView:
                UtilityClass.startActivity(this, DeletedActivity.class);
                break;
            case R.id.openCompletedTaskView:
                UtilityClass.replaceFragment(this,R.id.drawerLayout,new CompletedFragment(), UtilityTags.FRAGMENT_COMPLETED);
                break;
        }

    }

    /*drawer menu*/
    private void setupDrawerContent(NavigationView navigationView) {
        navigationView.setNavigationItemSelectedListener(
                menuItem -> {
                    selectDrawerItem(menuItem);
                    return true;
                });
    }

    public void selectDrawerItem(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case R.id.settings:
                UtilityClass.startActivity(this, SettingsActivity.class);
                break;
            case R.id.log_out:
                final UserSettings userSettings = new UserSettings(getBaseContext());
                userSettings.unset(SettingKey.USER_EMAIL, SettingKey.ENCRYPTED_PASSWORD);
                UtilityClass.startActivityNoBack(this, LoginActivity.class);
                break;
            case R.id.help:
                startActivity(new Intent(Intent.ACTION_VIEW,
                        Uri.parse("http://sitolorenzogardini.altervista.org/UserGuide.html")));
                break;
        }
    }
}
