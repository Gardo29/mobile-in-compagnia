package com.example.task;

import android.app.Application;
import android.app.NotificationManager;
import android.os.Build;

import com.example.task.notification.NotificationUtility;
import com.jakewharton.threetenabp.AndroidThreeTen;

public class TaskApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        AndroidThreeTen.init(this);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            NotificationUtility.createNotificationChannel(this, true, "Task app", "Task notification", NotificationManager.IMPORTANCE_HIGH);
        }
    }
}
