package com.example.task.view.customcomponents.customdialog;

import android.app.Dialog;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.RecyclerView;

import com.example.task.R;
import com.example.task.room.entities.List;
import com.example.task.view.recyclerview.customdialog.ListDialogAdapter;
import com.example.task.viewmodel.GetListsViewModel;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;

import java8.util.stream.StreamSupport;

public class ListDialogFragment extends DialogFragment implements ListDialogAdapter.OnListClickListener {

    private final OnListSelected listSelected;
    private final int listId;

    public ListDialogFragment(@NonNull final OnListSelected listSelected, final int id){
        this.listSelected = listSelected;
        this.listId = id;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        final View view = LayoutInflater.from(requireContext()).inflate(R.layout.fragment_dialog_list,null);
        final GetListsViewModel model = new ViewModelProvider(this).get(GetListsViewModel.class);

        final RecyclerView  recyclerView = view.findViewById(R.id.dialogRecyclerView);
        final ListDialogAdapter adapter = new ListDialogAdapter(this);
        recyclerView.setAdapter(adapter);
        model.getLists().observe(requireActivity(), lists ->
                adapter.setData(lists, getIndex(lists))
        );
        return new MaterialAlertDialogBuilder(this.requireContext())
                .setView(view).create();
    }

    @Override
    public void onItemClick(List list) {
        this.listSelected.onListSelected(list.getListId(),list.getServerId(),list.getName());
        this.dismiss();
    }

    private int getIndex(java.util.List<List> list) {
       if (this.listId != -1) {
            return list.indexOf(StreamSupport.stream(list).filter(l -> l.getListId() == listId).findAny().get());
        }
        return -1;
    }

    public interface OnListSelected{ void onListSelected (int listId, Integer serverListId, String listName);}
}
