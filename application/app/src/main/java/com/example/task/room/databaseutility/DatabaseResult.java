package com.example.task.room.databaseutility;

import androidx.annotation.NonNull;

import com.example.task.room.entities.List;
import com.example.task.room.entities.Notification;
import com.example.task.room.entities.Task;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.threeten.bp.LocalDateTime;

public class DatabaseResult {
    private static final boolean CLEAN = false;
    private static final LocalDateTime EMPTY = null;

    public static Task updateTask(@NonNull final Task task, @NonNull final JSONArray array) throws JSONException {
        final JSONObject result = array.getJSONObject(0);
        task.setTs(result.getInt(EntityFields.TS.toString()));
        task.setServerId(result.getInt(EntityFields.TASK_ID.toString()));
        task.setDirty(CLEAN);
        return task;
    }
    public static List updateList(@NonNull final List list, @NonNull final JSONArray array) throws JSONException {
        final JSONObject result = array.getJSONObject(0);
        list.setTs(result.getInt(EntityFields.TS.toString()));
        list.setServerId(result.getInt(EntityFields.LIST_ID.toString()));
        list.setDirty(CLEAN);
        return list;
    }
    public static Notification updateNotification(@NonNull final Notification notification, @NonNull final JSONArray array) throws JSONException {
        final JSONObject result = array.getJSONObject(0);
        notification.setTs(result.getInt(EntityFields.TS.toString()));
        notification.setTime(JSONParser.dateConverter(result.getString(EntityFields.NOTIFICATION_TIME.toString())));
        notification.setTaskServerId(result.getInt(EntityFields.NOTIFICATION_TASK_ID.toString()));
        notification.setDirty(CLEAN);
        return notification;
    }
    public static boolean isResultCorrect(@NonNull final JSONArray array) throws JSONException {
        final JSONObject result = array.getJSONObject(0);
        return result.getBoolean(EntityFields.DB_RESULT.toString());
    }
}