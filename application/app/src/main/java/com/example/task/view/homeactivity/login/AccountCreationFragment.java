package com.example.task.view.homeactivity.login;

import android.os.Bundle;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.example.task.R;
import com.example.task.room.databaseutility.DatabaseResult;
import com.example.task.room.databaseutility.EncryptionUtility;
import com.example.task.view.UtilityClass;
import com.example.task.view.UtilityTags;
import com.example.task.volley.DataBuilder;
import com.google.android.material.textfield.TextInputLayout;
import org.json.JSONException;

public class AccountCreationFragment extends AbstractLogin {
    private static final int MIN_PASSWORD_CHARACTERS = 8;
    private View view;
    private TextInputLayout completeNameTextInput;
    private TextInputLayout emailTextInput;
    private TextInputLayout passwordTextInput;
    private TextInputLayout passwordConfirmTextInput;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater,container,savedInstanceState);
        this.view = inflater.inflate(R.layout.fragment_account_creation,container,false);

        this.loadTextInput();

        this.view.findViewById(R.id.sign_up_button).setOnClickListener(l->this.replaceFragmentAnimationRight(new LoginFragment(), UtilityTags.FRAGMENT_LOGIN_TAG));

        this.view.findViewById(R.id.accountCreationLayout).setOnFocusChangeListener((v,hasFocus)-> UtilityClass.focusToBackground(this.activity,hasFocus));

        this.view.findViewById(R.id.account_creation_next_button).setOnClickListener(l->this.tryCatch(this::processUserCreation));
        return this.view;
    }

    @Override
    public void onResume() {super.onResume();}

    @Override
    public void onPause() { super.onPause(); }

    private void processUserCreation() throws JSONException {
        if(this.checkFields()){
            final DataBuilder dataBuilder = new DataBuilder(this.activity.getApplication());
            final String user = super.getText(this.emailTextInput);
            final String encryptedPassword = EncryptionUtility.digest(super.getText(this.passwordTextInput));
            final String completeName = super.getText(this.completeNameTextInput);

            this.connectServer(dataBuilder.createUserCreationSubmit(user,completeName,encryptedPassword),success->{
                this.tryCatch(()->{
                    if(DatabaseResult.isResultCorrect(success)){
                        this.writeUserCredentials(user,encryptedPassword);
                        Toast.makeText(this.getContext(),"Account successfully created",Toast.LENGTH_SHORT).show();
                        this.startHomeActivity();
                    }else{
                        this.emailTextInput.setError("That email is already taken. Try another");
                    }
                });
            },failure->this.connectionError());
        }
    }
    private boolean checkFields(){
        boolean flag = true;
        this.clearTextInput();
        final String completeName = super.getText(this.completeNameTextInput);
        final String password = super.getText(this.passwordTextInput);
        final String email = super.getText(this.emailTextInput);
        final String passwordConfirm = super.getText(this.passwordConfirmTextInput);

        //TODO: put strings to strings.xml file
        if(completeName.isEmpty()){
            completeNameTextInput.setError("Enter complete name");
            flag = false;
        }
        if(email.isEmpty()){
            emailTextInput.setError("Enter username");
            flag = false;
        }
        if(!email.isEmpty() && !Patterns.EMAIL_ADDRESS.matcher(email).matches()){
            emailTextInput.setError("Enter valid email");
            flag = false;
        }
        if(password.isEmpty()){
            passwordTextInput.setError("Enter password");
            flag = false;
        }
        if(!password.isEmpty() && password.length() < MIN_PASSWORD_CHARACTERS){
            passwordTextInput.setError("Use 8 characters or more for your password");
            flag = false;
        }
        if(!password.isEmpty() && password.length() >= MIN_PASSWORD_CHARACTERS && !passwordConfirm.equals(password)) {
            passwordConfirmTextInput.setError("Those passwords didn't match. Try again.");
            flag = false;
        }
        return flag;
    }

    private void loadTextInput(){
        this.completeNameTextInput = this.view.findViewById(R.id.account_creation_complete_name);
        this.emailTextInput = this.view.findViewById(R.id.account_creation_email);
        this.passwordTextInput = this.view.findViewById(R.id.account_creation_password);
        this.passwordConfirmTextInput = this.view.findViewById(R.id.account_creation_password_confirm);
    }

    private void clearTextInput(){
        this.completeNameTextInput.setErrorEnabled(false);
        this.emailTextInput.setErrorEnabled(false);
        this.passwordTextInput.setErrorEnabled(false);
        this.passwordConfirmTextInput.setErrorEnabled(false);
    }
    @Override
    public void jsonException() { UtilityClass.serverResponseProblem(this.context); }

    @Override
    public void connectionError() { UtilityClass.serverErrorToast(this.context); }
}
