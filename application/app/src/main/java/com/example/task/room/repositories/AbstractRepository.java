package com.example.task.room.repositories;

import android.app.Application;
import android.content.Context;

import androidx.annotation.NonNull;

import com.example.task.room.TaskDatabase;
import com.example.task.room.daos.ListDAO;
import com.example.task.room.daos.NotificationDAO;
import com.example.task.room.daos.TaskDAO;


public abstract class AbstractRepository {

    private final TaskDAO taskDAO;
    private final ListDAO listDAO;
    private final NotificationDAO notificationDAO;

    public AbstractRepository(@NonNull final Context context){
         final TaskDatabase database = TaskDatabase.getInstance(context);
         this.listDAO = database.listDAO();
         this.taskDAO = database.taskDAO();
         this.notificationDAO = database.notificationDAO();
    }

    public TaskDAO getTaskDAO() { return this.taskDAO; }

    public ListDAO getListDAO() { return this.listDAO; }

    public NotificationDAO getNotificationDAO() { return  this.notificationDAO; }
}
