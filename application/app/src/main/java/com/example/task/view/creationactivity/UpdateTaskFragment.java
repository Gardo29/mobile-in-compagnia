package com.example.task.view.creationactivity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Pair;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentActivity;

import com.example.task.R;
import com.example.task.room.databaseutility.DatabaseWriter;
import com.example.task.room.entities.List;
import com.example.task.room.entities.Notification;
import com.example.task.room.entities.Task;
import com.example.task.sharedpreferences.SettingKey;
import com.example.task.view.UtilityTags;
import com.example.task.view.homeactivity.TasksColors;
import com.google.android.material.textfield.TextInputLayout;

import org.threeten.bp.Duration;
import org.threeten.bp.LocalDateTime;
import org.threeten.bp.temporal.ChronoUnit;

import java.io.Serializable;

import java8.util.stream.StreamSupport;

import static android.app.Activity.RESULT_OK;

public class UpdateTaskFragment extends AbstractCreateTask {

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (super.timelessSwitch.isChecked()) {
            super.showAndHideTime();
        }
    }

    @Override
    protected void initViewComponents(FragmentActivity activity) {
        super.initViewComponents(activity);
        final Bundle b = activity.getIntent().getExtras();

        if (b != null) {
            final List list = (List) b.getSerializable(UtilityTags.BUNDLE_LIST);
            super.task = (Task) b.getSerializable(UtilityTags.BUNDLE_TASK);
            this.setTaskView(b);
            assert list != null;
            super.listId = list.getListId();
            super.listDialog.setText(list.getName());
            super.serverListId = list.getServerId();
        }
    }

    @Override
    void writeTask(@NonNull Activity activity) {
        final DatabaseWriter databaseWriter = new DatabaseWriter(activity);
        super.task.setListId(super.listId);
        super.task.setServerListId(super.serverListId);
        super.task.setTitle(super.taskName.getText().toString());
        super.task.setColor(super.taskColor);
        String description = super.description.getText().toString();
        super.task.setDescription(description.isEmpty() ? null : description);
        super.setTimeAndRepetition();
        databaseWriter.updateTask(super.task);
        final Intent intent = new Intent();
        intent.putExtra(UtilityTags.BUNDLE_NOTIFICATION, (Serializable) getNotifications(task));
        intent.putExtra(UtilityTags.BUNDLE_TASK, super.task);
        requireActivity().setResult(RESULT_OK, intent);
        activity.finish();
    }

    private void setTaskView(@NonNull Bundle b) {
        super.taskName.setText(super.task.getTitle());
        if (super.task.getStart() == null) {
            super.timelessSwitch.setChecked(true);
            final LocalDateTime end = this.task.getEnd();
            super.startDateTextView.setText(this.fromLocalDateTimeToStringFormat(end));
            super.endDateTextView.setText(this.fromLocalDateTimeToStringFormat(end));
            super.endDate = this.fromLocalDateToLong(end);
            super.startDate = this.fromLocalDateToLong(end);
        } else {
            super.timelessSwitch.setChecked(false);
            final LocalDateTime start = this.task.getStart();
            final LocalDateTime end = this.task.getEnd();
            super.startDateTextView.setText(this.fromLocalDateTimeToStringFormat(start));
            super.endDateTextView.setText(this.fromLocalDateTimeToStringFormat(end));
            super.startTimeTextView.setText(this.fromIntToTimeStringFormat(start.getHour(), start.getMinute()));
            super.endTimeTextView.setText(this.fromIntToTimeStringFormat(end.getHour(), end.getMinute()));
            super.startDate = this.fromLocalDateToLong(start);
            super.endDate = this.fromLocalDateToLong(end);
            super.startHour = start.getHour();
            super.startMinute = start.getMinute();
            super.endHour = end.getHour();
            super.endMinute = end.getMinute();
        }

        if (this.task.getRepeat() != null) {
            final String str = "Repeat every " + this.task.getRepeat() + " " + this.task.getRepeatUnit().toLowerCase();
            super.addRepetition.setText(str);
            super.repetition = new Pair<>(this.task.getRepeat(), ChronoUnit.valueOf(this.task.getRepeatUnit()));
            super.removeRepetition.setVisibility(View.VISIBLE);
        }

        @SuppressWarnings("unchecked")
        final java.util.List<Notification> list = (java.util.List<Notification>) b.getSerializable(UtilityTags.BUNDLE_NOTIFICATION);
        if (list != null && !list.isEmpty()) {
            super.notifications.clear();
            StreamSupport.stream(list)
                    .filter(notification -> !notification.isDeleted())
                    .forEach(notification -> {
                        long duration;
                        if (ChronoUnit.valueOf(notification.getUnit()).between(notification.getTime(), this.task.getStart()) > 0) {
                            duration = ChronoUnit.valueOf(notification.getUnit()).between(notification.getTime(), this.task.getStart());
                        } else {
                            final LocalDateTime nexTime = this.task.getStart().plus(Duration.of(this.task.getRepeat(), ChronoUnit.valueOf(this.task.getRepeatUnit())));
                            duration = ChronoUnit.valueOf(notification.getUnit()).between(notification.getTime(), nexTime);
                        }
                        super.timeNotification.add(new Pair<>((int) duration, ChronoUnit.valueOf(notification.getUnit())));
                        super.notifications.add(duration + " " + notification.getUnit().toLowerCase() + " before");
                    });
            super.notifications.add("Add notification");
            super.notificationAdapter.setData(this.notifications);
        }

        super.taskColor = this.task.getColor();
        super.setColor(this.taskColor);
        if (userSettings.getInt(SettingKey.DEFAULT_COLOR) != this.taskColor) {
            final TasksColors tasksColors = new TasksColors(requireActivity());
            super.chooseColor.setText(tasksColors.toColorName(this.taskColor));
        }

        if (this.task.getDescription() != null) {
            final TextInputLayout descriptionInputLayout = this.requireActivity().findViewById(R.id.descriptionLayout);
            descriptionInputLayout.setHint("");
            descriptionInputLayout.getEditText().setText(this.task.getDescription());
        }
    }
}
