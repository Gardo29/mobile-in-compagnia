package com.example.task.view.customcomponents.customdialog;

public interface OnSelectionCanceled {
    void onSelectionCanceled();
}
