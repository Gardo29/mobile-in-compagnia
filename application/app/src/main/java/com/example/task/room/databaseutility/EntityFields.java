package com.example.task.room.databaseutility;

import androidx.annotation.NonNull;

public enum EntityFields {
    /*  lists  */
    LIST_ID("list_id"),
    LIST_NAME("name"),
    LIST_DESCRIPTION("description"),
    LIST_CREATION_DATE("creation_date"),
    LIST_DELETED("deleted"),
    LIST_DISPLAY_HOME("display_home"),
    LIST_ENABLED_NOTIFICATIONS("enabled_notifications"),
    /*  tasks   */
    TASK_ID("task_id"),
    TASK_CREATION_DATE("creation_date"),
    TASK_TITLE("title"),
    TASK_COLOR("color"),
    TASK_START_DATE("start_date"),
    TASK_END_DATE("end_date"),
    TASK_REPEAT_TIME("repeat_time"),
    TASK_DELETED("deleted"),
    TASK_COMPLETED("completed"),
    TASK_DESCRIPTION("description"),
    TASK_LIST_ID("list_id"),
    TASK_PERMANENTLY_DELETED("permanently_deleted"),
    TASK_REPEAT_UNIT("repeat_unit"),
    /*   notifications   */
    NOTIFICATION_TASK_ID("task_id"),
    NOTIFICATION_UNIT("unit"),
    NOTIFICATION_TIME("time"),
    NOTIFICATION_NEW_TIME("new_time"),
    NOTIFICATION_DELETED("deleted"),
    /*      all       */
    DB_RESULT("result"),
    TS("ts"),
    DIRTY("dirty");

    private final String name;

    private EntityFields(@NonNull final String name){
        this.name = name;
    }
    @NonNull
    @Override
    public String toString(){
        return this.name;
    }
}
