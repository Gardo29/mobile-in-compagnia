package com.example.task.room.entities;

import androidx.room.TypeConverter;

import org.threeten.bp.Duration;
import org.threeten.bp.LocalDateTime;
import org.threeten.bp.format.DateTimeFormatter;

public class Converters {
    private static final DateTimeFormatter FORMATTER = DateTimeFormatter.ISO_LOCAL_DATE_TIME;
    @TypeConverter
    public static LocalDateTime dateFromTimestamp(final String date) {
        return date == null ? null : LocalDateTime.parse(date);
    }

    @TypeConverter
    public static String dateToTimestamp(final LocalDateTime date) {
        return date == null ? null : date.format(FORMATTER);
    }

    @TypeConverter
    public static Long periodToTimestamp(final Duration period) {
        return period == null ? null : period.toMinutes();
    }

    @TypeConverter
    public static Duration timestampToPeriod(final Long period) {
        return period == null ? null : Duration.ofMinutes(period);
    }

    public static DateTimeFormatter getTimeFormatter() { return FORMATTER; }
}
