package com.example.task.notification;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.work.Worker;
import androidx.work.WorkerParameters;
import com.example.task.room.entities.List;
import com.example.task.room.entities.Task;
import com.example.task.room.repositories.ListRepository;
import com.example.task.room.repositories.NotificationRepository;
import com.example.task.room.repositories.TaskRepository;
import com.example.task.sharedpreferences.SettingKey;
import com.example.task.sharedpreferences.UserSettings;
import com.example.task.view.UtilityClass;
import com.example.task.viewmodel.DataBuffers;
import org.threeten.bp.LocalDateTime;

import java8.util.stream.StreamSupport;

public class NotificationSchedulerWorker extends Worker {

    final private NotificationRepository notificationRepository;
    final private TaskRepository taskDAO;
    final private ListRepository listDAO;
    final private DataBuffers dataBuffers;
    final private UserSettings userSettings;

    public NotificationSchedulerWorker(@NonNull Context context, @NonNull WorkerParameters workerParams) {
        super(context, workerParams);
        this.notificationRepository = new NotificationRepository(context);
        this.taskDAO = new TaskRepository(context);
        this.listDAO = new ListRepository(context);
        this.userSettings = new UserSettings(context);
        this.dataBuffers = DataBuffers.getInstance();
    }

    @NonNull
    @Override
    public Result doWork() {
        if (!this.userSettings.getBoolean(SettingKey.MUTE_ALL_NOTIFICATIONS)) {
            LocalDateTime time = this.notificationRepository.getTime();
            UtilityClass.setNextNotificationTime(time);
            if (time != null) {
                StreamSupport.stream(this.notificationRepository.getNotificationsByTime(time))
                        .forEach(notification -> {
                            final Task task = this.getTask(notification.getTaskId());
                            final List list = this.getList(task.getListId());
                            if (list.isEnabledNotifications()) {
                                NotificationUtility.scheduleAlarm(getApplicationContext(), notification.getTime(), task.getTaskId());
                            }
                        });
                return Result.success();
            }
        }
        return Result.failure();
    }

    private List getList(final int listId) {
        List list = this.dataBuffers.getRoomList(listId);
        if (list == null){
            list = listDAO.getListById(listId);
        }
        return list;
    }

    private Task getTask(final int taskId) {
        Task task = this.dataBuffers.getRoomTask(taskId);
        if (task == null) {
            task = taskDAO.getTaskById(taskId);
        }
        return task;
    }
}
