package com.example.task.view.listviewactivity;

import android.content.Intent;
import android.graphics.Point;
import android.os.Bundle;

import android.view.Display;
import android.view.MenuItem;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.task.R;
import com.example.task.room.entities.List;
import com.example.task.view.AbstractToolbarActivity;
import com.example.task.view.UtilityClass;
import com.example.task.view.UtilityTags;
import com.example.task.view.creationactivity.CreateListFragment;
import com.example.task.view.creationactivity.CreationActivity;
import com.example.task.view.recyclerview.ListAdapter;
import com.example.task.viewmodel.GetListsViewModel;

public class ListViewActivity extends AbstractToolbarActivity implements ListAdapter.OnListClickListener {

    private static final int NUM_COLUMN = 2;
    private ListAdapter adapter;
    private int numList;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRecyclerView();
        final TextView title = findViewById(R.id.toolbarTitleView);
        final GetListsViewModel model = new ViewModelProvider(this).get(GetListsViewModel.class);
        model.getLists().observe(this, allList -> {
            adapter.setData(allList);
            numList = allList.size();
            title.setText(getString(R.string.all_list_title, Integer.toString(numList)));
        });

        findViewById(R.id.addButton).setOnClickListener(l ->{
            final Bundle fragmentClass = new Bundle();
            fragmentClass.putSerializable(UtilityTags.CREATION_FRAGMENT, CreateListFragment.class);
            UtilityClass.startActivityBundleForResult(this, CreationActivity.class, fragmentClass, UtilityTags.ACTIVITY_CREATE_LIST);
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == UtilityTags.ACTIVITY_CREATE_LIST && resultCode == RESULT_OK) {
            adapter.notifyDataSetChanged();
        }
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_all_list;
    }

    @Override
    protected DrawerLayout getDrawerLayout() {
        return findViewById(R.id.drawerLayout);
    }

    @Override
    protected Toolbar getToolbar() {
        return findViewById(R.id.toolbar);
    }

    @Override
    protected void selectBottomMenuItem(MenuItem menuItem) {
        super.selectBottomMenuItem(menuItem);
        if (menuItem.getItemId() == R.id.openHomeView) {
            this.onBackPressed();
        }
    }

    private void setRecyclerView() {
        final RecyclerView recyclerView;
        recyclerView = this.findViewById(R.id.recyclerView);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new GridLayoutManager(this, NUM_COLUMN));

        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);

        this.adapter = new ListAdapter(this, size.x);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void onItemClick(List list, int position) {
        UtilityClass.startActivityBundle(this, SingleListActivity.class, UtilityTags.BUNDLE_LIST, list);
    }
}
