package com.example.task.view.homeactivity;

import android.os.Bundle;
import android.util.Log;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.task.R;
import com.example.task.room.databaseutility.DatabaseWriter;
import com.example.task.room.entities.Task;
import com.example.task.view.UtilityClass;
import com.example.task.view.UtilityTags;
import com.example.task.view.homeactivity.details.TaskDetailsFragment;
import com.example.task.view.recyclerview.TaskAdapter;
import com.example.task.view.recyclerview.interfaces.OnCheckBoxClick;
import com.example.task.view.recyclerview.interfaces.OnTaskClick;
import com.example.task.viewmodel.GetListsViewModel;
import com.example.task.viewmodel.GetTasksViewModel;
import org.threeten.bp.LocalDate;
import org.threeten.bp.LocalDateTime;
import java.util.List;

import java8.util.stream.Collectors;
import java8.util.stream.StreamSupport;

public class ToDoTaskFragment extends Fragment implements OnCheckBoxClick, OnTaskClick {

    private TaskAdapter adapter;
    private RecyclerView recyclerView;
    private List<Pair<Task, String>> list;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.task_layout, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        FragmentActivity activity = getActivity();

        if (activity != null) {
            setRecyclerView(activity);
            final GetTasksViewModel taskModel = new ViewModelProvider(this).get(GetTasksViewModel.class);
            final GetListsViewModel listModel = new ViewModelProvider(this).get(GetListsViewModel.class);
            taskModel.getUncompletedTask().observe(activity, allList -> {
                this.list = StreamSupport.stream(allList)
                        .filter(task -> {
                            if((task.getStart() == null && task.getEnd().toLocalDate().isEqual(LocalDate.now())) || task.isExpired()){
                                return true;
                            } else if (task.getStart() != null) {
                                return task.getStart().toLocalDate().isEqual(LocalDate.now());
                            }
                            return false;
                        })
                        .filter(task ->listModel.getListById(task.getListId()).isDisplayHome())
                        .sorted(Task::compareTo)
                        .map(t -> new Pair<>(t, listModel.getListById(t.getListId()).getName()))
                        .collect(Collectors.toList());
                adapter.setData(list);
            });
        }
    }

    private void setRecyclerView(FragmentActivity activity){
        recyclerView = activity.findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this.getContext()));
        recyclerView.setHasFixedSize(true);

        this.adapter = new TaskAdapter(this, this, false);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void onCheckBoxClick(Task task, boolean isChecked, int position) {
        task.setCompletionTime(LocalDateTime.now());
        final DatabaseWriter databaseWriter = new DatabaseWriter((AppCompatActivity) this.requireActivity());
        databaseWriter.updateTask(task);
    }

    @Override
    public void onTaskClick(Task task) {
        UtilityClass.replaceFragmentBundle(requireActivity(), R.id.drawerLayout, new TaskDetailsFragment(),
                UtilityTags.FRAGMENT_TASK_DETAILS,task,UtilityTags.BUNDLE_TASK);
    }
}
