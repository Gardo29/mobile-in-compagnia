package com.example.task.view;

public class UtilityTags {

    public static final String FRAGMENT_TODO_TASK = "ToDoTaskFragment";
    public static final String FRAGMENT_DONE_TASK = "DoneTaskFragment";
    public static final String FRAGMENT_MENU = "MenuFragment";
    public static final String FRAGMENT_CREATE_LIST = "CreateListFragment";
    public static final String FRAGMENT_CREATE_TASK = "CreateTaskFragment";
    public static final String FRAGMENT_UPDATE_TASK = "UpdateTaskFragment";
    public static final String FRAGMENT_LOGIN_TAG = "LoginFragment";
    public static final String FRAGMENT_ACCOUNT_CREATION_TAG = "AccountCreationFragment";
    public static final String FRAGMENT_LOADING_TAG = "LoadingFragment";
    public static final String FRAGMENT_SETTINGS = "SettingsFragment";
    public static final String FRAGMENT_COLOR_CHECKBOX_DIALOG = "ColorCheckboxDialogFragment";
    public static final String FRAGMENT_COLOR_RADIO_DIALOG = "ColorRadioDialogFragment";
    public static final String FRAGMENT_TIME_DIALOG = "TimeDialogFragment";
    public static final String FRAGMENT_LIST_DIALOG = "ListDialogFragment";
    public static final String FRAGMENT_DATE_PICKER_DIALOG = "DatePickerDialogFragment";
    public static final String FRAGMENT_TIME_PICKER_DIALOG = "TimePickerDialogFragment";
    public static final String FRAGMENT_TASK_DETAILS = "TaskDetailsFragment";
    public static final String FRAGMENT_DELETED = "DeletedFragment";
    public static final String FRAGMENT_SEARCH = "SearchFragment";
    public static final String FRAGMENT_COMPLETED = "CompletedFragment";
    public static final String FRAGMENT_LIST_DETAILS = "ListDetailsFragment";

    public static final String FRAGMENT_DATE_PICKER = "DatePicker";

    public static final String BUNDLE_LIST_ID = "listId";
    public static final String BUNDLE_LIST_NAME = "listName";
    public static final String BUNDLE_LIST_SERVER_ID = "listServerId";
    public static final String BUNDLE_TASK_ID = "taskId";
    public static final String BUNDLE_LIST = "list";
    public static final String BUNDLE_TASK = "task";
    public static final String BUNDLE_NOTIFICATION = "notification";
    public static final String CREATION_FRAGMENT = "fragment";

    public static final String CHANNEL_ID = "Todo App";
    public static final String CHANNEL_DESCRIPTION = "Task notification channel";
    public static int ACTIVITY_CREATE_LIST = 0;
    public static int ACTIVITY_CREATE_TASK = 0;
    public static int ACTIVITY_UPDATE_LIST = 0;
    public static int ACTIVITY_UPDATE_TASK = 0;
}
