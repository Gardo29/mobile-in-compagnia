package com.example.task.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Transformations;

import com.example.task.room.entities.Task;
import com.example.task.room.relations.Reminder;

import org.threeten.bp.LocalDate;
import org.threeten.bp.LocalDateTime;
import org.threeten.bp.temporal.ChronoUnit;

import java.util.List;
import java.util.TreeSet;

import java8.util.stream.Collectors;
import java8.util.stream.StreamSupport;

public class GetTasksViewModel extends AbstractTaskViewModel{

    private final LiveData<List<Task>> deletedTasks;
    private final LiveData<List<Task>> uncompletedTasks;
    private final LiveData<List<Task>> completedTasks;

    public GetTasksViewModel(@NonNull final Application application) {
        super(application);
        this.deletedTasks = this.getTaskRepository().getDeletedTasks();
        this.uncompletedTasks = this.getTaskRepository().getUncompletedTasks();
        this.completedTasks = this.getTaskRepository().getCompletedTasks();
    }

    public Task getTaskByServerId(final int taskServerId) { return this.getTaskRepository().getTaskByServerId(taskServerId); }
    public Task getTaskById(final int taskId){ return this.getTaskRepository().getTaskById(taskId); }
    public Task getLastTask() {return this.getTaskRepository().getLastTask();}
    public List<Task> getDirtyTask() { return this.getTaskRepository().getDirtyTasks(); }
    public LiveData<List<Task>> getDeletedTasks() { return  this.deletedTasks; }
    public LiveData<List<Task>> getUncompletedTask() { return  this.uncompletedTasks; }
    public LiveData<List<Task>> searchTasks(@NonNull final String title,@NonNull final List<Integer> colors,
                                            final boolean completed, final boolean deleted) {
        return this.getTaskRepository().searchTasks(title,colors,completed,deleted);
    }
    public List<Reminder> getAllNotifications() { return this.getTaskRepository().getAllNotifications(); }
    public LiveData<List<Task>> getCompletedTask() { return this.completedTasks; }
}
