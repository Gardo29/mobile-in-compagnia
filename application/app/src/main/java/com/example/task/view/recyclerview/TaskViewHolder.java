package com.example.task.view.recyclerview;

import android.graphics.Paint;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.example.task.R;
import com.example.task.view.customcomponents.RoundCheckBox;
import com.example.task.view.recyclerview.interfaces.OnCheckBoxHolderClick;
import com.example.task.view.recyclerview.interfaces.OnHolderClick;

public class TaskViewHolder extends RecyclerView.ViewHolder {

    public final TextView taskTitle;
    public final TextView listTitle;
    public final TextView date;
    public final RoundCheckBox checkBox;
    public final CardView cardView;
    public final TextView completionDate;

    public TaskViewHolder(@NonNull View itemView, @NonNull OnCheckBoxHolderClick checkBoxListener, @NonNull OnHolderClick cardViewListener, @NonNull Boolean done) {
        super(itemView);
        this.taskTitle = itemView.findViewById(R.id.taskName);
        this.listTitle = itemView.findViewById(R.id.listName);
        this.date = itemView.findViewById(R.id.taskDate);
        this.checkBox = itemView.findViewById(R.id.roundCheckBox);
        this.cardView = itemView.findViewById(R.id.cardView);
        this.completionDate = itemView.findViewById(R.id.completionTimeTextView);

        this.checkBox.setDone(done);
        if (done) {
            this.taskTitle.setPaintFlags(taskTitle.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        }

        this.checkBox.setOnClickListener(l -> {
            //this.checkBox.updateDoneState();
            checkBoxListener.onCheckBoxHolderClick(getAdapterPosition(),this.checkBox.isDone());
        });

        this.cardView.setOnClickListener(l->
            cardViewListener.onHolderClick(getAdapterPosition()));
    }
}
