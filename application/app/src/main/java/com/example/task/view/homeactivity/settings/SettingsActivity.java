package com.example.task.view.homeactivity.settings;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import com.example.task.R;
import com.example.task.view.UtilityClass;
import com.example.task.view.UtilityTags;

public class SettingsActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_settings);

        UtilityClass.setupToolbar(this,R.id.settingsToolbar);

        UtilityClass.replaceFragmentNoBack(this,R.id.settingsLayoutContainer,new SettingsFragment(), UtilityTags.FRAGMENT_SETTINGS);
    }

}
