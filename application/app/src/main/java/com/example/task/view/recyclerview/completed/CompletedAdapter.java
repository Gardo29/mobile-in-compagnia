package com.example.task.view.recyclerview.completed;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.core.util.Pair;
import androidx.recyclerview.widget.RecyclerView;

import com.example.task.R;
import com.example.task.room.entities.Task;
import com.example.task.view.recyclerview.TaskViewHolder;
import com.example.task.view.recyclerview.interfaces.OnCheckBoxClick;
import com.example.task.view.recyclerview.interfaces.OnCheckBoxHolderClick;
import com.example.task.view.recyclerview.interfaces.OnHolderClick;
import com.example.task.view.recyclerview.interfaces.OnTaskClick;

import org.threeten.bp.LocalDate;
import org.threeten.bp.LocalDateTime;
import org.threeten.bp.format.DateTimeFormatter;

import java.util.ArrayList;
import java.util.List;

import java8.util.stream.Collectors;
import java8.util.stream.StreamSupport;

public class CompletedAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final int NO_DATE = 1;
    private static final int DATE = 0;
    private List<Pair<Task,String>> tasks;
    private final OnCheckBoxClick checkBoxClick;
    private final OnTaskClick taskClick;


    public CompletedAdapter(@NonNull final OnCheckBoxClick onCheckBoxClick, @NonNull final OnTaskClick onTaskClick){
        this.checkBoxClick = onCheckBoxClick;
        this.taskClick = onTaskClick;
        this.tasks = new ArrayList<>();
    }
    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        final OnHolderClick onHolderClick = position -> this.taskClick.onTaskClick(this.tasks.get(position).first);
        final OnCheckBoxHolderClick onCheckBoxHolderClick = (position, isChecked) -> this.checkBoxClick.onCheckBoxClick(this.tasks.get(position).first, isChecked,position);
        if(viewType == DATE){
            final View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_task_completed, parent, false);
            return new CompletedViewHolder(layoutView,onCheckBoxHolderClick,onHolderClick);
        }

        final View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_task, parent, false);
        return new TaskViewHolder(layoutView, onCheckBoxHolderClick,onHolderClick,true);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder genericHolder, int position) {

        if(getItemViewType(position) == DATE){
            final CompletedViewHolder holder = (CompletedViewHolder)genericHolder;
            final Task currentTask = this.tasks.get(position).first;
            final String listName = this.tasks.get(position).second;
            final LocalDate completionDate = currentTask.getCompletionTime().toLocalDate();
            holder.roundCheckBox.setColor(currentTask.getColor());
            holder.title.setText(currentTask.getTitle());
            holder.list.setText(listName);
            holder.completionDate.setText(completionDate.format(DateTimeFormatter.ISO_DATE));
            if (currentTask.getStart() != null) {
                holder.startDate.setText(currentTask.getStart().format(DateTimeFormatter.ofPattern("HH:mm")));
            }
            if (currentTask.getStart() != null) {
                if (currentTask.getEnd().isBefore(currentTask.getCompletionTime())) {
                    holder.startDate.setText(currentTask.getEnd().format(DateTimeFormatter.ofPattern("MM.dd  HH:mm")));
                } else{
                    holder.startDate.setText(currentTask.getStart().format(DateTimeFormatter.ofPattern("HH:mm")));
                }
            }else{
                if (currentTask.getEnd().isBefore(currentTask.getCompletionTime())) {
                    holder.startDate.setText(currentTask.getEnd().format(DateTimeFormatter.ofPattern("MM.dd")));
                }
            }
        }else{
            final TaskViewHolder holder = (TaskViewHolder)genericHolder;
            final Task currentTask = this.tasks.get(position).first;
            final String listName = this.tasks.get(position).second;
            holder.checkBox.setColor(currentTask.getColor());
            holder.taskTitle.setText(currentTask.getTitle());
            holder.listTitle.setText(listName);

            if (currentTask.getStart() != null) {
                if (currentTask.getEnd().isBefore(currentTask.getCompletionTime())) {
                    holder.date.setText(currentTask.getEnd().format(DateTimeFormatter.ofPattern("MM.dd  HH:mm")));
                } else{
                    holder.date.setText(currentTask.getStart().format(DateTimeFormatter.ofPattern("HH:mm")));
                }
            }else{
                if (currentTask.getEnd().isBefore(currentTask.getCompletionTime())) {
                    holder.date.setText(currentTask.getEnd().format(DateTimeFormatter.ofPattern("MM.dd")));
                }
            }
        }
    }

    @Override
    public int getItemViewType(int position) {
        if(position > 0){
            final LocalDate prevDate = this.tasks.get(position - 1).first.getCompletionTime().toLocalDate();
            final LocalDate completionDate = this.tasks.get(position).first.getCompletionTime().toLocalDate();
            if(prevDate != null && completionDate.isEqual(prevDate)){
                return NO_DATE;
            }
        }
        return DATE;
    }

    @Override
    public int getItemCount() {
        return this.tasks.size();
    }

    public void setData(@NonNull final List<Pair<Task,String>> tasks){
        this.tasks = tasks;
        notifyDataSetChanged();
    }
}
