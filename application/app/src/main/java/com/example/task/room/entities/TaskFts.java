package com.example.task.room.entities;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Fts4;
import androidx.room.PrimaryKey;

import org.threeten.bp.LocalDateTime;

@Fts4(contentEntity = Task.class)
@Entity(tableName = "task_fts")
public class TaskFts {
    @PrimaryKey
    int rowid;
    String title;
}
