package com.example.task.view.homeactivity.login;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;

import com.example.task.R;
import com.example.task.sharedpreferences.SettingKey;
import com.example.task.sharedpreferences.UserSettings;
import com.example.task.view.UtilityClass;
import com.example.task.view.UtilityTags;
import com.example.task.view.homeactivity.MainActivity;
import com.example.task.volley.RequestManager.SubmitSuccess;
import com.example.task.volley.RequestManager.SubmitFailure;
import com.example.task.volley.NetworkConnection;
import com.example.task.volley.RequestManager;
import com.google.android.material.textfield.TextInputLayout;

import org.json.JSONArray;
import org.json.JSONException;

public abstract class AbstractLogin extends Fragment {
    protected FragmentActivity activity;
    protected Context context;
    protected NetworkConnection networkConnection;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        this.activity = requireActivity();
        this.context = requireContext();
        this.networkConnection = new NetworkConnection(this.activity);
        this.networkConnection.registerNetworkCallback();

        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
        this.networkConnection.registerNetworkCallback();
    }

    @Override
    public void onPause() {
        super.onPause();
        this.networkConnection.unregisterCallback();
    }

    public boolean connectServer(@NonNull final JSONArray send, @NonNull final SubmitSuccess success, @NonNull final SubmitFailure failure) throws JSONException {
        if(this.networkConnection.isNetworkConnected()){
            final RequestManager requestManager = new RequestManager(this.activity);
            requestManager.sendData(send,success,failure);
            return true;
        }else {
            UtilityClass.noInternetConnectionToast(this.context);
            return false;
        }
    }
    String getText(@NonNull final TextInputLayout inputLayout){
        return inputLayout.getEditText().getText().toString();
    }

    void writeUserCredentials(@NonNull final String email,@NonNull final String password){
        final UserSettings userSettings = new UserSettings(this.context);
        userSettings.putString(SettingKey.USER_EMAIL,email).putString(SettingKey.ENCRYPTED_PASSWORD,password);
    }

    void startHomeActivity(){
        UtilityClass.startActivityNoBack(this.activity, MainActivity.class);
        UtilityClass.scheduleSync();
        UtilityClass.scheduleNextNotifications();
        UtilityClass.scheduleRepetitions();
    }

    public void tryCatch(VoidFunction givenFunction){
        try {
            givenFunction.executeAction();
        } catch (JSONException e) {
            this.jsonException();
        }
    }


    void replaceFragmentAnimationRight(@NonNull final Fragment fragment, @NonNull final String tag){
        this.networkConnection.unregisterCallback();
        UtilityClass.replaceFragmentAnimationNoBackRight(this.activity, R.id.fragmentContainer,fragment, tag);
    }

    void replaceFragmentAnimationLeft(@NonNull final Fragment fragment, @NonNull final String tag){
        this.networkConnection.unregisterCallback();
        UtilityClass.replaceFragmentAnimationNoBackLeft(this.activity, R.id.fragmentContainer,fragment, tag);
    }

    public abstract void jsonException();
    public abstract void connectionError();

    public interface VoidFunction{void executeAction() throws JSONException;}

}
