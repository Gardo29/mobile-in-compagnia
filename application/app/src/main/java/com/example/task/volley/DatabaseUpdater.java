package com.example.task.volley;

import android.accessibilityservice.FingerprintGestureController;
import android.content.Context;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;

import com.example.task.room.repositories.ListRepository;
import com.example.task.room.repositories.NotificationRepository;
import com.example.task.room.repositories.TaskRepository;
import com.example.task.view.UtilityClass;
import com.example.task.view.homeactivity.login.AbstractLogin.VoidFunction;
import com.example.task.room.entities.List;
import com.example.task.room.entities.Notification;
import com.example.task.room.entities.Task;
import com.example.task.room.databaseutility.DatabaseWriter;
import com.example.task.sharedpreferences.SettingKey;
import com.example.task.sharedpreferences.UserSettings;
import com.example.task.viewmodel.GetListsViewModel;
import com.example.task.viewmodel.GetNotificationsViewModel;
import com.example.task.viewmodel.GetTasksViewModel;

import org.json.JSONException;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

import java8.util.stream.StreamSupport;

import static com.example.task.volley.RequestManager.RequestFailure;
import static com.example.task.volley.RequestManager.SubmitFailure;

public class DatabaseUpdater {

    private final RequestManager requestManager;
    private final DataBuilder dataBuilder;
    private final DatabaseWriter databaseWriter;

    private final ListRepository listRepository;
    private final TaskRepository taskRepository;
    private final NotificationRepository notificationRepository;

    private final String userEmail;
    private final String password;

    public DatabaseUpdater(@NonNull final Context context){
        final UserSettings userSettings = new UserSettings(context);
        this.requestManager = new RequestManager(context);
        this.userEmail = userSettings.getString(SettingKey.USER_EMAIL);
        this.password = userSettings.getString(SettingKey.ENCRYPTED_PASSWORD);
        this.dataBuilder = new DataBuilder(context);
        this.databaseWriter = new DatabaseWriter(context);
        this.listRepository = new ListRepository(context);
        this.taskRepository = new TaskRepository(context);
        this.notificationRepository = new NotificationRepository(context);
    }

    public void syncData(@NonNull RequestFailure requestFailure,@NonNull SubmitFailure submitFailure,@NonNull JSONHandler jsonHandler){
        this.pullNewDataAndSend(requestFailure,submitFailure,jsonHandler);
    }
    private void sendDirtyLists(@NonNull final SubmitFailure submitFailure, @NonNull final JSONHandler jsonHandler) {
        final java.util.List<List> dirtyLists = this.listRepository.getDirtyLists();
        if(dirtyLists == null || dirtyLists.size() == 0 ){
            this.sendDirtyTasks(submitFailure,jsonHandler);
            return;
        }
        final AtomicInteger dataCounter = new AtomicInteger(dirtyLists.size());
        StreamSupport.stream(dirtyLists).forEach(list-> this.sendSingleList(list,submitFailure,jsonHandler,listId->{
            dataCounter.decrementAndGet();
            if(dataCounter.get() == 0){
                this.sendDirtyTasks(submitFailure,jsonHandler);
            }
        }));
    }
    public void sendSingleList(@NonNull final List list,@NonNull final SubmitFailure submitFailure,
                               @NonNull final JSONHandler jsonHandler,@NonNull final ReceiveDataCallback callback){
        try {
            this.requestManager.sendData(this.dataBuilder.createListSubmit(this.userEmail,this.password,list),submitSuccess->{
                try {
                    final Integer serverId = this.databaseWriter.updateList(list,submitSuccess);
                    callback.onReceivedData(serverId);
                } catch (JSONException e) {
                    jsonHandler.onJSONException(e);
                }
            },submitFailure);
        } catch (JSONException e) {
            jsonHandler.onJSONException(e);
        }
    }
    public void sendDirtyTasks(@NonNull final SubmitFailure submitFailure, @NonNull final JSONHandler jsonHandler){

        final java.util.List<Task> dirtyTasks = this.taskRepository.getDirtyTasks();
        if(dirtyTasks == null || dirtyTasks.size() == 0){
            this.sendDirtyNotifications(submitFailure,jsonHandler);
            return;
        }
        final AtomicInteger dataCounter = new AtomicInteger(dirtyTasks.size());
        StreamSupport.stream(dirtyTasks).forEach(task-> {
            if(task.getServerListId() == null){
                final int serverListId = this.listRepository.getListById(task.getListId()).getServerId();
                task.setServerListId(serverListId);
            }
            this.sendSingleTask(task,submitFailure,jsonHandler,taskId->{
                dataCounter.decrementAndGet();
                if(dataCounter.get() == 0){
                    this.sendDirtyNotifications(submitFailure,jsonHandler);
                }
            });
        });
    }



    public void sendSingleTask(@NonNull final Task task, @NonNull final SubmitFailure submitFailure,
                               @NonNull final JSONHandler jsonHandler,@NonNull final ReceiveDataCallback callback){
        try {
            this.requestManager.sendData(this.dataBuilder.createTaskSubmit(this.userEmail,this.password,task),submitSuccess->{
                try {
                    final Integer serverId = this.databaseWriter.updateTask(task,submitSuccess);
                    callback.onReceivedData(serverId);
                } catch (JSONException e) {
                    jsonHandler.onJSONException(e);
                }
            }, submitFailure);
        } catch (JSONException e) {
            jsonHandler.onJSONException(e);
        }
    }

    private void sendDirtyNotifications(@NonNull final SubmitFailure submitFailure, @NonNull final JSONHandler jsonHandler) {
        final java.util.List<Notification> dirtyNotifications = this.notificationRepository.getDirtyNotifications();

        if(dirtyNotifications != null){
            StreamSupport.stream(dirtyNotifications).forEach(notification-> {
                if(notification.getTaskServerId() == null){
                    final Task task = this.taskRepository.getTaskById(notification.getTaskId());
                    final int serverTaskId = task.getServerId();
                    notification.setTaskServerId(serverTaskId);
                }
                this.sendSingleNotification(notification,submitFailure,jsonHandler);
            });
        }
        this.databaseWriter.clearDeletedEntities();
    }
    public void sendSingleNotification(@NonNull final Notification notification,@NonNull final SubmitFailure submitFailure, @NonNull final JSONHandler jsonHandler){
        try {
            this.requestManager.sendData(this.dataBuilder.createNotificationSubmit(this.userEmail,this.password,notification),submitSuccess->{
                try {
                    if(notification.getTaskServerId() == null){
                        notification.setTaskServerId(taskRepository.getTaskById(notification.getTaskId()).getServerId());
                    }
                    this.databaseWriter.updateNotification(notification,submitSuccess);
                } catch (JSONException e) {
                    jsonHandler.onJSONException(e);
                }
            },submitFailure);
        } catch (JSONException e) {
            jsonHandler.onJSONException(e);
        }
    }
    public void pullNewDataAndSend(@NonNull final RequestFailure requestFailure,@NonNull final SubmitFailure submitFailure,@NonNull JSONHandler jsonHandler){
        try {
            this.requestManager.getData(this.dataBuilder.createDataRequest(this.userEmail,this.password),requestSuccess->{
                try {
                    this.databaseWriter.updateDatabase(requestSuccess);
                    this.sendDirtyLists(submitFailure,jsonHandler);
                } catch (JSONException e) {
                    jsonHandler.onJSONException(e);
                }
            },requestFailure);
        }catch (JSONException e){
            jsonHandler.onJSONException(e);
        }
    }

    public void cancelConnection(){
        this.requestManager.cancelConnection();
    }

    public interface JSONHandler{void onJSONException(JSONException e);}
    public interface ReceiveDataCallback { void onReceivedData(Integer id);}
}
