package com.example.task.room.entities;

import android.util.Log;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.Index;


import com.example.task.room.FieldsUtility;

import org.threeten.bp.LocalDateTime;
import org.threeten.bp.temporal.ChronoUnit;

import java.io.Serializable;

import static androidx.room.ForeignKey.CASCADE;

@Entity(tableName = "notifications",primaryKeys = {"time","task_id"},
        foreignKeys = {@ForeignKey(onDelete = CASCADE,entity = Task.class,parentColumns = "task_id",childColumns = "task_id")},
        indices = {@Index("task_id")})
public class Notification implements Serializable {
    @NonNull
    private LocalDateTime time;
    @ColumnInfo(name = "task_id")
    private int taskId;
    @ColumnInfo(name = "task_server_id")
    private Integer taskServerId;
    private String unit;
    private boolean deleted;
    private int ts;
    private boolean dirty;

    public Notification(@NonNull final LocalDateTime time,final int taskId){
        this.time = time;
        this.taskId = taskId;
    }

    @NonNull
    public LocalDateTime getTime() {
        return this.time;
    }

    public String getUnit() { return this.unit; }

    public Integer getTaskServerId() { return this.taskServerId; }

    public int getTaskId() {
        return this.taskId;
    }

    public int getTs() { return this.ts; }

    public boolean isDeleted() { return this.deleted; }

    public boolean isDirty() { return dirty; }

    public void setDeleted(final boolean deleted) { this.deleted = deleted; }

    public void setTs(final int ts) { this.ts = ts; }

    public void setTaskId(final int taskId) { this.taskId = taskId; }

    public void setTaskServerId(final Integer taskServerId) { this.taskServerId = taskServerId; }

    public void setDirty(final boolean dirty) { this.dirty = dirty; }

    public void setTime(@NonNull final LocalDateTime time) { this.time = time.truncatedTo(ChronoUnit.SECONDS); }

    public void setUnit(String unit) { this.unit = unit; }
}
