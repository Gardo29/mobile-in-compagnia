package com.example.task.view.customcomponents;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;

import com.example.task.R;

public class RoundCheckBox extends View {

    private final static int DEFAULT_COLOR = Color.WHITE;
    private final float border = 3;
    private final  float roundEdges = 20;
    private Paint paint;
    private boolean done;
    private int color;
    //private Path check;
    //private float length;
    //private ObjectAnimator objectAnimator;

    public RoundCheckBox(Context context) {
        super(context);
        init();
        setSaveEnabled(true);
    }

    public RoundCheckBox(Context context, AttributeSet attr) {
        super(context, attr);
        init();
        setSaveEnabled(true);
        setUpAttributes(context, attr);
    }

    private void init() {
        this.paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        //this.check = new Path();
        this.done = false;
        //this.setCheckPath();
    }

    private void setUpAttributes(Context context, AttributeSet attr) {
        final TypedArray ar = context.getTheme().obtainStyledAttributes(attr, R.styleable.RoundCheckBox, 0, 0);
        this.color = ar.getColor(R.styleable.RoundCheckBox_checkBoxColor, Color.alpha(DEFAULT_COLOR));
        ar.recycle();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        fillColor(canvas);
        addBorder(canvas);
        drawDone(canvas);
    }

    private void fillColor(Canvas canvas) {
        //draw the white rectangle
        this.paint.setColor(Color.WHITE);
        this.paint.setStyle(Paint.Style.FILL);
        canvas.drawRoundRect(delta()*0.2f, delta()*0.2f, delta()*8.5f, delta()*2f, roundEdges, roundEdges, this.paint);

        //draw the square colored checkbox
        this.paint.setColor(color);
        this.paint.setStyle(Paint.Style.FILL);
        canvas.drawRoundRect(delta()*0.2f, delta()*0.2f, delta()*2.5f, delta()*2f, roundEdges, roundEdges, this.paint);

        //fix checkbox
        this.paint.setColor(Color.WHITE);
        this.paint.setStyle(Paint.Style.FILL);
        canvas.drawRect(delta()*2f, delta()*0.2f, delta()*3f, delta()*2f, this.paint);
    }

    private void addBorder(Canvas canvas) {
        //fix checkbox border
        this.paint.setColor(Color.rgb(102, 102, 102));
        this.paint.setStyle(Paint.Style.STROKE);
        this.paint.setStrokeWidth(border);
        canvas.drawLine(delta()*2, delta()*0.2f, delta()*2f, delta()*2f, this.paint);

        //draw border
        this.paint.setColor(Color.rgb(102, 102, 102));
        this.paint.setStyle(Paint.Style.STROKE);
        this.paint.setStrokeWidth(border);
        canvas.drawRoundRect(delta()*0.2f, delta()*0.2f, delta()*8.5f, delta()*2, roundEdges, roundEdges, this.paint);

        //draw white circle
        this.paint.setColor(Color.rgb(102, 102, 102));
        this.paint.setStyle(Paint.Style.STROKE);
        this.paint.setStrokeWidth(border);
        canvas.drawCircle(delta()*1.13f, delta()*1.13f, delta()*0.4f, this.paint);
    }

    private void drawDone(Canvas canvas){

        if (!this.done) {
            this.paint.setColor(Color.WHITE);
            this.paint.setStyle(Paint.Style.FILL);
            canvas.drawCircle(delta()*1.13f, delta()*1.13f, delta()*0.4f, this.paint);
        } else {
            this.paint.setColor(Color.WHITE);
            this.paint.setStyle(Paint.Style.FILL);
            canvas.drawCircle(delta()*1.13f, delta()*1.13f, delta()*0.4f, this.paint);

            this.paint.setColor(Color.rgb(102, 102, 102));
            this.paint.setStyle(Paint.Style.STROKE);
            this.paint.setStrokeWidth(border*3);

            //this.objectAnimator.start();

            canvas.drawLine(delta()*1.03f, delta()*1.38f, delta()*1.43f, delta()*0.95f, this.paint);
            canvas.drawLine(delta()*1.075f, delta()*1.39f, delta()*0.8f, delta()*1.14f, this.paint);
        }
    }

    /*private void setCheckPath() {
        this.check.moveTo(delta()*0.8f, delta()*1.14f);
        this.check.lineTo(delta()*1.075f, delta()*1.39f);
        this.check.lineTo(delta()*1.43f, delta()*0.95f);

        // Measure the path
        PathMeasure measure = new PathMeasure(this.check, false);
        this.length = measure.getLength();

        this.objectAnimator = ObjectAnimator.ofFloat(RoundCheckBox.this, "phase", 1.0f, 0.0f);
        this.objectAnimator.setDuration(3000);
    }

    public void setPhase(float phase)
    {
        paint.setPathEffect(createPathEffect(length, phase, 0.0f));
    }

    private static PathEffect createPathEffect(float pathLength, float phase, float offset)
    {
        return new DashPathEffect(new float[] { pathLength, pathLength },
                Math.max(phase * pathLength, offset));
    }*/

    private float delta() {
        return getMeasuredWidth()/8.6f;
    }

    public void setDone(boolean doneState){
        this.done = doneState;
    }

    public void updateDoneState() {
        this.done = !done;
        invalidate();
    }

    public void setColor(int newColor) {
        this.color = newColor;
        invalidate();
    }

    public boolean isDone() {
        return this.done;
    }

}
