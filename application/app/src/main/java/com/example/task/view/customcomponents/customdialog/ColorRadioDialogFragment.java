package com.example.task.view.customcomponents.customdialog;

import android.app.Dialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.recyclerview.widget.RecyclerView;

import com.example.task.R;
import com.example.task.sharedpreferences.SettingKey;
import com.example.task.sharedpreferences.UserSettings;
import com.example.task.view.homeactivity.TasksColors;
import com.example.task.view.recyclerview.customdialog.ColorItem;
import com.example.task.view.recyclerview.customdialog.ColorRadioAdapter;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;

import java8.util.stream.Collectors;
import java8.util.stream.StreamSupport;

public class ColorRadioDialogFragment extends DialogFragment implements ColorRadioAdapter.OnItemClick {

    private int selectedItem;
    private TasksColors tasksColors;
    private final OnColorSelected onColorSelected;

    public ColorRadioDialogFragment(int colorSelected,@NonNull final OnColorSelected onColorSelected){
        this.onColorSelected = onColorSelected;
        this.selectedItem = colorSelected;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        final View view = LayoutInflater.from(requireContext()).inflate(R.layout.fragment_dialog_list,null);
        this.tasksColors = new TasksColors(requireContext());
        final java.util.List<ColorItem> colorList = StreamSupport.stream(this.tasksColors.colorsList())
                .map(e->new ColorItem(this.tasksColors.toColorName(e),e))
                .collect(Collectors.toList());
        final int colorPosition = colorList.indexOf(StreamSupport.stream(colorList).filter(e->e.getColor() == this.selectedItem).findFirst().get());
        final RecyclerView recyclerView = view.findViewById(R.id.dialogRecyclerView);
        recyclerView.setAdapter(new ColorRadioAdapter(colorList,this,colorPosition));

        return new MaterialAlertDialogBuilder(requireContext())
                .setView(view).create();
    }

    @Override
    public void onItemClick(int position) {
        if(this.selectedItem != position) {
            final int color = this.tasksColors.colorsList().get(position);
            final String name = this.tasksColors.toColorName(color);
            this.onColorSelected.onColorSelected(color, name);
            this.dismiss();
        }
    }

    public interface OnColorSelected{ void onColorSelected (int color, String name);}
}
