package com.example.task.view.recyclerview;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.task.R;
import com.example.task.room.entities.Task;
import com.example.task.view.recyclerview.interfaces.OnCheckBoxClick;
import com.example.task.view.recyclerview.interfaces.OnTaskClick;

import org.threeten.bp.LocalDate;
import org.threeten.bp.LocalDateTime;
import org.threeten.bp.format.DateTimeFormatter;

import java.util.ArrayList;
import java.util.List;

import java8.util.function.Supplier;

public class TaskAdapter extends RecyclerView.Adapter<TaskViewHolder> {

    private final OnCheckBoxClick checkBoxListener;
    private final OnTaskClick cardViewListener;
    private final boolean doneState;
    private final List<Pair<Task, String>> taskList = new ArrayList<>();

    public TaskAdapter(@NonNull OnCheckBoxClick checkBoxListener, @NonNull OnTaskClick cardViewListener, final boolean done){
        this.checkBoxListener = checkBoxListener;
        this.cardViewListener = cardViewListener;
        this.doneState = done;
    }

    @NonNull
    @Override
    public TaskViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        final View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_task,
                parent, false);
        return new TaskViewHolder(layoutView, (position,isChecked) -> this.checkBoxListener.onCheckBoxClick(taskList.get(position).first, isChecked,position),
                position -> this.cardViewListener.onTaskClick(this.taskList.get(position).first), this.doneState);
    }

    @Override
    public void onBindViewHolder(@NonNull TaskViewHolder holder, int position) {
        final Task currentTask = taskList.get(position).first;
        final String listName = taskList.get(position).second;
        holder.setIsRecyclable(false);
        holder.checkBox.setColor(currentTask.getColor());
        holder.taskTitle.setText(currentTask.getTitle());
        holder.listTitle.setText(listName);

        holder.date.setText("");
        this.setDate(() -> currentTask.isExpired() && currentTask.getStart() == null, holder.date, currentTask.getEnd(), "MM.dd");
        this.setDate(() -> currentTask.isExpired() && currentTask.getStart() != null, holder.date, currentTask.getStart(), "MM.dd HH:mm");
        this.setDate(() -> !currentTask.isExpired() && currentTask.getStart() != null && currentTask.getStart().toLocalDate().isEqual(LocalDate.now()), holder.date, currentTask.getStart(), "HH:mm");

        if (currentTask.isExpired()) {
            holder.taskTitle.setTextColor(Task.EXPIRED_TASK_COLOR);
            holder.date.setTextColor(Task.EXPIRED_TASK_COLOR);
        }
    }

    public void setData(@NonNull List<Pair<Task, String>> data) {
        this.taskList.clear();
        this.taskList.addAll(data);
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return taskList.size();
    }

    private void setDate(@NonNull final Supplier<Boolean> condition, @NonNull final TextView textView, @NonNull LocalDateTime time, String dateFormat) {
        if (condition.get()) {
            textView.setText(time.format(DateTimeFormatter.ofPattern(dateFormat)));
        }
    }
}
