<?php 
define("SITE", "sitolorenzogardini");

$db = new DatabaseHelper("localhost",SITE, "", "my_sitolorenzogardini");

class DatabaseHelper{

    public function __construct($servername, $username, $password, $dbname){
        $this->db = new mysqli($servername, $username, $password, $dbname);
    
        if ($this->db->connect_error) {
            die("Connection failed: " . $db->connect_error);
        } 
        $this->db->set_charset("utf8");       
    }
    /*      GETTERS      */
    public function tryLogin($email,$password){
        $user = $this->getUserByEmail($email);
        if($user != null){
            $encryptedPassword = $user["encrypted_password"];
            $salt = $user["salt"];
            if($encryptedPassword == hash("sha512",$password.$salt)){
                return true;
            }
        }
        return false;
    }
    public function getUserByEmail($email){
        $stmt = $this->db->prepare("SELECT * FROM USERS WHERE email = ?");
        $stmt->bind_param("s",$email);
        $stmt->execute();
        return $stmt->get_result()->fetch_all(MYSQLI_ASSOC)[0];
    }
    public function getLists($userId,$ts){
        $stmt = $this->db->prepare("SELECT * FROM LISTS WHERE user_id = ? AND ts > ? ORDER BY creation_date ");
        $stmt->bind_param("ii",$userId,$ts);
        $stmt->execute();
        return $stmt->get_result()->fetch_all(MYSQLI_ASSOC);
    }
    public function getTasks($userId,$ts){
        $stmt = $this->db->prepare("SELECT T.* FROM LISTS L,TASKS T WHERE user_id = ? AND L.list_id = T.list_id AND T.ts > ? ORDER BY start_date");
        $stmt->bind_param("ii",$userId,$ts);
        $stmt->execute();
        return $stmt->get_result()->fetch_all(MYSQLI_ASSOC);
    }
    public function getNotifications($userId,$ts){
        $stmt = $this->db->prepare("SELECT N.* FROM LISTS L,TASKS T,NOTIFICATIONS N WHERE user_id = ? AND L.list_id = T.list_id AND T.task_id = N.task_id AND N.ts > ? ORDER BY time");
        $stmt->bind_param("ii",$userId,$ts);
        $stmt->execute();
        return $stmt->get_result()->fetch_all(MYSQLI_ASSOC);
    }
    public function getListTs(){
        return $this->getManageTs($this->db->query("SELECT ts FROM LISTS ORDER BY ts DESC LIMIT 1")->fetch_all(MYSQLI_ASSOC));
    }
    public function getTaskTs(){
        return $this->getManageTs($this->db->query("SELECT ts FROM TASKS ORDER BY ts DESC LIMIT 1")->fetch_all(MYSQLI_ASSOC));
    }
    public function getNotificationTs(){
        return $this->getManageTs($this->db->query("SELECT ts FROM NOTIFICATIONS ORDER BY ts DESC LIMIT 1")->fetch_all(MYSQLI_ASSOC));
    }
    private function getManageTs($tuple){
        if(!empty($tuple)){
            return $tuple[0]["ts"] + 1;
        }
        return 0;
    }
    private function getNotificationById($time,$taskId){
        $stmt = $this->db->prepare("SELECT * FROM NOTIFICATIONS WHERE task_id = ? AND time = ?");
        $stmt->bind_param("is",$taskId,$time);
        $stmt->execute();
        return $stmt->get_result()->fetch_all(MYSQLI_ASSOC)[0];
    }
    /*      INSERTIONS      */
    public function createUser($email,$completeName,$encryptedPassword,$salt,$deleted = false){
        if($this->getUserByEmail($email) == null){
            $stmt = $this->db->prepare("INSERT INTO USERS (email,complete_name,encrypted_password,salt,deleted) VALUES (?,?,?,?,?)");
            $stmt->bind_param("ssssi",$email,$completeName,$encryptedPassword,$salt,$deleted);
            $stmt->execute();
            return array("result" => true);
        }
        return array("result" => false);
    }
    public function createList($userId,$name,$description,$creationDate,$display = true,$deleted = false,$enabledNotifications = true){
        $ts = $this->getListTs();
        $stmt = $this->db->prepare("INSERT INTO LISTS (display_home,ts,deleted,name,description,creation_date,user_id,enabled_notifications) VALUES (?,?,?,?,?,?,?,?)");
        $stmt->bind_param("iiisssii",$display,$ts,$deleted,$name,$description,$creationDate,$userId,$enabledNotifications);
        if($stmt->execute()){
            $listId = $stmt->insert_id;
            return array("list_id" => $listId,"ts" => $ts,"result" => true);
        }
        return array("result" => false);
    }
    public function createTask($listId,$title,$description,$start,$end,$color,$repeat,$creationDate,$repeatUnit,$completed = null,$deleted = null,$permanentlyDelete = false){
        $ts = $this->getTaskTs();
        $stmt = $this->db->prepare("INSERT INTO TASKS (list_id,ts,title,description,start_date,end_date,color,repeat_time,creation_date,completed,deleted,permanently_deleted,repeat_unit) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)");
        $stmt->bind_param("iissssiisssis",$listId,$ts,$title,$description,$start,$end,$color,$repeat,$creationDate,$completed,$deleted,$permanentlyDelete,$repeatUnit);


        if($stmt->execute()){

            $taskId = $stmt->insert_id;
            return array("task_id" => $taskId,"ts" => $ts,"result" => true);
        }
        return array("result" => false);
    }
    public function addDeleteNotification($time,$taskId,$unit,$deleted = false){
        $ts = $this->getNotificationTs();
        $stmt = $this->db->prepare("INSERT INTO NOTIFICATIONS (time,task_id,deleted,unit,ts) VALUES (?,?,?,?,?) ON DUPLICATE KEY UPDATE deleted = ?, ts = ?");
        $stmt->bind_param("siisiii",$time,$taskId,$deleted,$unit,$ts,      $deleted,$ts); // <-- for update
        $stmt->execute();
        if($stmt->execute()){
            return array("task_id" => $taskId,"time" => $time,"ts" => $ts,"result" => true);
        }
        return array("result" => false);
    }
    /*      UPDATES        */
    public function updateList($listId,$name,$description,$display = true,$deleted = false,$enabledNotifications = true){
        $ts = $this->getListTs();
        $stmt = $this->db->prepare("UPDATE LISTS SET display_home = ?, ts = ?, deleted = ?, name = ?, description = ?, enabled_notifications = ? WHERE list_id = ? ");
        $stmt->bind_param("iiissii",$display,$ts,$deleted,$name,$description,$enabledNotifications,$listId);
        if($stmt->execute() && $stmt->affected_rows > 0){
            return array("list_id" => $listId,"ts" => $ts,"result"=>true);
        }
        return array("list_id" => $listId,"result"=>false);
    }
    public function updateTask($taskId,$title,$description,$start,$end,$color,$repeat,$repeatUnit,$completed = false,$deleted = null,$permanentlyDelete = false){
        $ts = $this->getTaskTs();
        $stmt = $this->db->prepare("UPDATE TASKS SET ts = ?,title = ?,description = ?,start_date = ?,end_date = ?,color = ?,repeat_time = ?,repeat_unit = ?,completed = ?,deleted = ?,permanently_deleted = ? WHERE task_id = ?");
        $stmt->bind_param("issssiisssii",$ts,$title,$description,$start,$end,$color,$repeat,$repeatUnit,$completed,$deleted,$permanentlyDelete,$taskId);
        if($stmt->execute() && $stmt->affected_rows > 0){
            return array("task_id" => $taskId,"ts" => $ts,"result"=>true);
        }
        return array("task_id" => $taskId,"result"=>false);
    }

    public function updateNotification($time,$taskId,$newTime,$unit){
        if($this->getNotificationById($time,$taskId) != null){
            $remove = $this->addDeleteNotification($time,$taskId,null,true);
            if($remove["result"]){
                $addResult = $this->addDeleteNotification($newTime,$taskId,$unit,false);
                if($addResult["result"]){
                    return array("task_id" => $taskId,"time" => $newTime,"ts" => $addResult["ts"],"result" => true);
                }
            }
        }
        return array("result" => false);
    }
}

?>
