<?php 
define("TOKEN",hash("sha512","cats"));
require_once("database.php");

header("Access-Control-Allow-Origin: *");
header('Content-Type: application/json');


$json = file_get_contents('php://input');
$data = json_decode($json,true);
$data = $data[0];
$fail = json_encode(array(array("result" => false)));


if(!isset($data["token"]) || $data["token"] != TOKEN || 
   !isset($data["action"]) || !isset($data["user_email"]) || !isset($data["password"])){
    echo $fail;
    exit;
}

if($data["action"] == "login"){
    echo json_encode(array(array("result" => $db->tryLogin($data["user_email"],$data["password"]))));
    exit;
}

if($data["action"] == "registration" && isset($data["complete_name"]) && isset($data["password"])){
    $completePassword = createPassword($data["password"]);
    echo json_encode(array($db->createUser($data["user_email"],$data["complete_name"],$completePassword["crypted_password"],$completePassword["salt"])));
    exit;
}

// under you have to be logged to proceed
if(!$db->tryLogin($data["user_email"],$data["password"])){
    echo $fail;
    exit;
}
if($data["action"] == "modify_account"){ //TODO: BOOOOOOOOOOOOOOOOOOO
    if($db->tryLogin($data["user_email"],$data["password"])){
        echo json_encode($db->modifyUser($data["modify_user"]));
        exit;
    }
}

if($data["action"] == "pull" && isset($data["notification_ts"])){
    $listTs = $data["list_ts"];
    $taskTs = $data["task_ts"];
    $notificationTs = $data["notification_ts"];

    

    $userId = $db->getUserByEmail($data["user_email"])["user_id"];
    echo json_encode(array(array(
        "lists" => $db->getLists($userId,$listTs),
        "tasks" => $db->getTasks($userId,$taskTs),
        "notifications" => $db->getNotifications($userId,$notificationTs)
    )));
    exit;
}
if($data["action"] == "push"){
    $result = array();
    $user = $db->getUserByEmail($data["user_email"]);
    switch($data["entity"]){
        case "list":
            if(isset($data["list_id"])){
                $result = $db->updateList($data["list_id"],$data["name"],$data["description"],$data["display_home"],$data["deleted"],$data["enabled_notifications"]);
            }else{
                $result = $db->createList($user["user_id"],$data["name"],$data["description"],$data["creation_date"],$data["display_home"],$data["deleted"],$data["enabled_notifications"]);
            }
        break;
        case "task":
            if(isset($data["task_id"])){
                $result = $db->updateTask($data["task_id"],$data["title"],$data["description"],$data["start_date"],$data["end_date"],$data["color"],$data["repeat_time"],$data["repeat_unit"],$data["completed"],$data["deleted"],$data["permanently_deleted"]);
            }else{                                                                                                                                                                                                         
                $result = $db->createTask($data["list_id"],$data["title"],$data["description"],$data["start_date"],$data["end_date"],$data["color"],$data["repeat_time"],$data["creation_date"],$data["repeat_unit"],$data["completed"],$data["deleted"],$data["permanently_deleted"]);
            }
        break;
        case "notification":
                $result = $db->addDeleteNotification($data["time"],$data["task_id"],$data["unit"],$data["deleted"]);
        break;
    }
    echo json_encode(array($result));
    exit;
}
echo $fail;


function createPassword($password){
    // Crea una chiave casuale
    $random_salt = hash("sha512", uniqid(mt_rand(1, mt_getrandmax()), true));
    // Crea una password usando la chiave appena creata.
    $password = hash("sha512", $password.$random_salt);
    return array("crypted_password"=>$password,"salt"=>$random_salt);
}
?>
